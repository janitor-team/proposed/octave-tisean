# This test compares the results of TISEAN spectrum function
# $ spectrum amplitude.dat -f1 -o "amplitude_tisean.dat"
# $ spectrum square.dat -f1 -o "square_tisean.dat"
# $ spectrum ar.dat -f2 -w0.001 -o "ar_tisean.dat"


close all

dataset = {"amplitude", "square", "ar"};
ndata   = numel (dataset);

tisean_output = @(x) sprintf("%s_tisean.dat",x);

for i=1:ndata

  # Load tisean results
  data_tisean = load (tisean_output (dataset{i}));
  
  data = load ([dataset{i} ".dat"]);

  # Calculate with Octave 
  
  n   = length (data);
  if (strcmp (dataset{i}, "ar"))
    f = 2; 
    w = 0.001;
  else
    f = 1;
    w = 1/n;
  endif
  
  spec_tmp    = fft (data);
  
  half_n      = floor (n/2) + 1; 
  step        = 2*floor(n*w/(2*f)) + 1;
  idx         = 1:step:half_n;
  
  # Create spectrum unadjusted (the simple way)
  spec_unadj  = abs (spec_tmp(idx)); 

  # Create spectrum adjusted to fit the TISEAN data
  npoints     = columns (idx);
  spec_adj    = zeros (npoints,1);  
  spec_tmp    = abs (spec_tmp / n).^2;
  spec_adj(1) = spec_tmp(1);
  
  for j=2:npoints
    id = idx(j-1)+1:idx(j);       
    spec_adj(j) = sum (spec_tmp(id));
  endfor

  # Create the frequencies for the output
  freqs = (idx-1) / n * f;

  # Compare
  figure (2*i-1);
  plot (freqs, log10(spec_adj),'r.', ...
        data_tisean(:,1), log10(data_tisean(:,2)),'bo')
  legend ("Octave","Tisean");
  title ("Adjusted GNU Octave 'fft'");
  axis tight

  figure (2*i);
  plot (freqs, log10(spec_unadj),'r.', ...
        data_tisean(:,1), log10(data_tisean(:,2)),'bo')
  legend ("Octave","Tisean");
  title ("Unadjusted GNU Octave 'fft'");
  axis tight
 
  printf ("Difference on adjusted %s: %.3g\n", dataset{i}, ...
                   sqrt (mean (log10(spec_adj)-log10(data_tisean(:,2))).^2));
 
  fflush (stdout);

endfor
