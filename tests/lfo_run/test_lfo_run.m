# This compares the results from TISEAN and TISEAN package (tisean)
# octave:#> hen = henon (1000);
# octave:#+1> save hen1000.dat hen;
# $ lfo-run hen1000.dat -m2,4 -d6 -o lfo_run_hen.dat

load lfo_run_hen.dat

res = lfo_run (henon(1000), 'm',4,'d',6);

plot (1:1000, henon(2000)(1001:2000,1),'bo',1:1000,lfo_run_hen(:,1),'ro', 1:1000, res(:,1), 'g.');
h = legend ("Actual Henon", "Predicted with TISEAN", "Predicted with Matrix::Solve()");

set (h, 'fontsize', 15);
