# This test compares the results of TISEAN polynom function
# $ polynom amplitude.dat -m4 -d8 -p4 -L1000 -o amplitude_tisean.dat


close all

dataset = { "amplitude"};
ndata   = numel (dataset);

tisean_output = @(x) sprintf("%s_tisean.dat",x);

for i=1:ndata

  # Load tisean results
  data_tisean = load (tisean_output (dataset{i}));
  
  data = load ([dataset{i} ".dat"]);

  # Calculate with Octave 
  n = length (data);

  idx = transpose ([1:n]);
  [P,S,MU] =  polyfit (idx, data, 10);
  
  data_octave = S.yf;
  
  # Compare
  figure (2*i-1);
  plot (idx, data, 'g',...
        idx, data_octave,'r', ...
  # The Tisean data is put after the input data, because 'polynom' produces a prediction.  
        [n+1:n+length(data_tisean(:,1))],data_tisean(:,1),'b')

  legend ("Original", "Octave","Tisean");

  idx = [n-1000:n];  
  
  figure (2*i)
   plot (idx, data(idx), 'g',...
        [n+1:n+length(data_tisean(:,1))],data_tisean(:,1),'b')
  legend ("Original", "Tisean");
  printf ("Difference on %s: %.3g\n", dataset{i}, ...
                   sqrt (mean (data_octave-data).^2));
 
  fflush (stdout);

endfor
