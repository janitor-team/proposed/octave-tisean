/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber,
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lzo-test of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <cmath>

#include "routines_c/tsa.h"

/*number of boxes for the neighbor search algorithm*/
#define NMAX 512

void sort(const Matrix &series, octave_idx_type *found, 
          double *abstand, octave_idx_type embed, octave_idx_type DELAY, 
          octave_idx_type MINN, octave_idx_type nfound,
          double **hser)
{

  octave_idx_type hdim = (embed-1) * DELAY;
  octave_idx_type dim = series.columns ();

  for (octave_idx_type i=0;i<nfound;i++) {
    octave_idx_type hf=found[i];
    abstand[i]=0.0;
    for (octave_idx_type j=0;j<dim;j++) {
      for (octave_idx_type k=0;k<=hdim;k += DELAY) {
      double dx=fabs(series(hf-k,j)-hser[hdim-k][j]);
      if (dx > abstand[i]) abstand[i]=dx;
      }
    }
  }

  for (octave_idx_type  i=0;i<MINN;i++)
    for (octave_idx_type  j=i+1;j<nfound;j++)
      if (abstand[j]<abstand[i]) {
      double dswap=abstand[i];
      abstand[i]=abstand[j];
      abstand[j]=dswap;
      octave_idx_type iswap=found[i];
      found[i]=found[j];
      found[j]=iswap;
      }
}

void make_fit(const Matrix &series, octave_idx_type dim,
              octave_idx_type act, octave_idx_type number,
              octave_idx_type istep, octave_idx_type *found,
              Matrix &error_array, Matrix &diffs)
{
  double casted;
  const double *help;
  octave_idx_type h;
  h=istep-1;
  octave_idx_type len = series.rows ();
  for (octave_idx_type j=0;j<dim;j++) 
    {
      casted=0.0;
  //  old  help=series[j]+istep;
      help=series.fortran_vec()+j*len+istep;
      for (octave_idx_type i=0;i<number;i++)
        casted += help[found[i]];
      casted /= (double)number;
      diffs(act,j) = casted-help[act];
      error_array(h,j)  += sqr(casted-help[act]);
    }
}

DEFUN_DLD (__lzo_test__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if ((nargin != 13) || (nargout > 2))
  {
    print_usage ();
  }
  else
    {

      // Assign input
      Matrix input            = args(0).matrix_value ();
      octave_idx_type embed   = args(1).idx_type_value ();
      octave_idx_type DELAY   = args(2).idx_type_value ();
      octave_idx_type CLENGTH = args(3).idx_type_value ();
      bool clengthset         = args(4).bool_value ();
      octave_idx_type refstep = args(5).idx_type_value ();
      octave_idx_type MINN    = args(6).idx_type_value ();
      double EPS0             = args(7).double_value ();
      bool epsset             = args(8).bool_value ();
      double EPSF             = args(9).double_value ();
      octave_idx_type STEP    = args(10).idx_type_value ();
      octave_idx_type causal  = args(11).idx_type_value ();
      bool setsort            = args(12).bool_value ();

      octave_idx_type LENGTH  = input.rows ();
      octave_idx_type dim     = input.columns ();

      // Allocate memory and analyze input
      OCTAVE_LOCAL_BUFFER(double*, hser, dim);
      OCTAVE_LOCAL_BUFFER(double, av, dim);
      OCTAVE_LOCAL_BUFFER(double, rms, dim);
      OCTAVE_LOCAL_BUFFER(double, hinter, dim);

      double mind;
      double interval=0.0;

      for (octave_idx_type i=0;i<dim;i++) {
        rescale_data(input,i,LENGTH,&mind,&hinter[i]);
        variance(input.column(i),LENGTH,&av[i],&rms[i]);
        interval += hinter[i];
      }
      interval /= (double)dim;

      OCTAVE_LOCAL_BUFFER (long, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);
      OCTAVE_LOCAL_BUFFER (unsigned long, hfound, LENGTH);
      OCTAVE_LOCAL_BUFFER (bool, done, LENGTH);
      OCTAVE_LOCAL_BUFFER (double, abstand, LENGTH);

      Matrix error_array (STEP, dim);
      Matrix diffs (LENGTH, dim);
      error_array.fill (0.0);

      MArray<octave_idx_type> box (dim_vector(NMAX, NMAX));

      // Compute forecast error
      if ( ! error_state)
        {
          for (octave_idx_type i=0;i<LENGTH;i++)
            done[i]=0;

          bool alldone = false;
          if (epsset)
            EPS0 /= interval;

          double epsilon = EPS0 / EPSF;

          if (!clengthset)
            CLENGTH=LENGTH;
          octave_idx_type clength = ((CLENGTH*refstep+STEP) <= LENGTH) 
                                    ? CLENGTH : (LENGTH-STEP)/refstep;

          // Compute estimates
          octave_idx_type actfound;
          octave_idx_type hi;
          while (!alldone) {
            alldone=1;
            epsilon*=EPSF;
            make_multi_box(input,box,list,LENGTH-(long)STEP,NMAX,dim,
                           embed,DELAY,epsilon);
            for (octave_idx_type i=(embed-1)*DELAY;i<clength;i++)
              if (!done[i]) {
              hi=i*refstep;
              for (octave_idx_type j=0;j<dim;j++)
                {
//              old  hser[j]=series[j]+hi;
                  hser[j] = input.fortran_vec() + j * LENGTH + hi;
                }
              actfound=find_multi_neighbors(input,box,list,hser,NMAX,
                                            dim,embed,DELAY,epsilon,hfound);
              actfound=exclude_interval(actfound,hi-(long)causal+1,
                                  hi+causal+(embed-1)*DELAY-1,hfound,found);
              if (actfound >= MINN)
                {
                  if (setsort)
                    {
                      sort(input, found, abstand, embed, DELAY, MINN,
                           actfound, hser);
                      actfound=MINN;
                    }
                  for (octave_idx_type j=1;j<=STEP;j++) {
                    make_fit(input,dim,hi,actfound,j,found,error_array,diffs);
                  }
                  done[i]=1;
                }
              alldone &= done[i];
              }
          }

          // Create relative forecast error output
          Matrix rel_forecast_err (STEP, dim + 1);
          for (octave_idx_type i=0;i<STEP;i++) 
            {
              rel_forecast_err(i,0) = i + 1;
              for (octave_idx_type j=0;j<dim;j++) 
                {
//            old  fprintf(stdout,"%e ",
//                    sqrt(error[j][i]/(clength-(embed-1)*DELAY))/rms[j]);
                  rel_forecast_err(i,j+1) = sqrt(error_array(i,j)
                                          /(clength-(embed-1)*DELAY))/rms[j];
                }
            }

          // Create individual forecast error output
          Matrix ind_forecast_err (1,1);
          if (nargout > 1)
            {
              ind_forecast_err.resize(clength - (embed-1)*DELAY, dim);
              for (octave_idx_type i=(embed-1)*DELAY;i<clength;i++) 
                {
                  hi=i*refstep;
                  for (octave_idx_type j=0;j<dim;j++)
                    {
//                   old fprintf(stdout,"%e ",diffs[j][hi]*hinter[j]);
                      ind_forecast_err(i-(embed-1)*DELAY,j) = \
                      diffs(hi,j)*hinter[j];
                    }
                }
            }

          retval(0) = rel_forecast_err;
          retval(1) = ind_forecast_err;
        }
    }
  return retval;
}
