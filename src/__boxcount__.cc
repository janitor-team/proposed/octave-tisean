/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on boxcount of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <vector>
#include <list>
#include <octave/oct.h>
#include <octave/oct-map.h>
#include "routines_c/tsa.h"

void next_dim(const double **series, octave_idx_type **which_dims,
              std::vector <double> &histo, octave_idx_type maxembed,
              octave_idx_type dimension, octave_idx_type DELAY,
              octave_idx_type length, octave_idx_type epsi, double Q,
              int wd,int n, std::vector<octave_idx_type> &first)
{

  OCTAVE_LOCAL_BUFFER (std::vector<octave_idx_type>, act, epsi);
  OCTAVE_LOCAL_BUFFER_INIT (octave_idx_type, found, epsi, 0);

  for (octave_idx_type i=0;i<n;i++)
    {
      octave_idx_type comp  = which_dims[wd][0];
      octave_idx_type d1    = which_dims[wd][1]*DELAY;

      octave_idx_type which = (octave_idx_type)
                              (series[comp][first[i]+d1]*(double)epsi);
      octave_idx_type hf    = ++found[which];
      act[which].resize(hf);
      act[which][hf-1]      = first[i];
    }

  for (octave_idx_type i=0;i<epsi;i++)
    if (found[i]) {
      double p = (double)(found[i]) / (double)length;
      if (Q == 1.0)
        histo[wd] -= p*log(p);
      else
        histo[wd] += pow(p,Q);
    }

  if (wd<(maxembed*dimension-1))
    for (octave_idx_type i=0;i<epsi;i++)
      if (found[i])
        next_dim(series, which_dims, histo, maxembed, dimension, DELAY, length,
                 epsi, Q, wd+1,found[i],act[i]);

}

void start_box(const double **series, octave_idx_type **which_dims,
               std::vector <double> &histo, octave_idx_type maxembed,
               octave_idx_type dimension, octave_idx_type DELAY,
               octave_idx_type length, octave_idx_type epsi, double Q)
{

  OCTAVE_LOCAL_BUFFER (std::vector<octave_idx_type>, act, epsi);
  OCTAVE_LOCAL_BUFFER_INIT (octave_idx_type, found, epsi, 0);

  for (octave_idx_type i=0;i<length;i++) {
    octave_idx_type which=(octave_idx_type)(series[0][i]*(double)epsi);
    octave_idx_type hf= ++found[which];
    act[which].resize(hf);
    act[which][hf-1]=i;
  }

  for (octave_idx_type i=0;i<epsi;i++)
    if (found[i]) {
      double p = (double)(found[i]) / (double)length;
      if (Q == 1.0)
        histo[0] -= p*log(p);
      else
        histo[0] += pow(p,Q);
    }

  if (1<dimension*maxembed) {
    for (octave_idx_type i=0;i<epsi;i++) {
      if (found[i])
        next_dim(series, which_dims, histo, maxembed, dimension, DELAY, length,
                 epsi, Q, 1,found[i],act[i]);
    }
  }
  /*
  else {
    if (1<maxembed)
      for (octave_idx_type i=0;i<epsi;i++) {
        if (found[i])
          next_dim(series, which_dims, histo, maxembed, dimension, DELAY,
                   length, epsi, Q, 1,found[i],act[i]);
      }
  }
  */

}

DEFUN_DLD (__boxcount__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 9)
  {
    print_usage ();
  }
  else
    {
      // Assign input
      Matrix input             = args(0).matrix_value ();
      octave_idx_type maxembed = args(1).idx_type_value ();
      octave_idx_type DELAY    = args(2).idx_type_value ();
      double Q                 = args(3).double_value ();
      double EPSMIN            = args(4).double_value ();
      bool epsminset           = args(5).bool_value ();
      double EPSMAX            = args(6).double_value ();
      bool epsmaxset           = args(7).bool_value ();
      octave_idx_type EPSCOUNT = args(8).idx_type_value ();

      octave_idx_type LENGTH    = input.rows ();
      octave_idx_type dimension = input.columns ();

      // Analyze and rescale input
      double interval, min, maxinterval = 0.0;
      for (octave_idx_type i=0;i<dimension;i++) {
        rescale_data(input,i,LENGTH,&min,&interval);
        if (interval > maxinterval)
          maxinterval=interval;
      }
      if (epsminset)
        EPSMIN /= maxinterval;
      if (epsmaxset)
        EPSMAX /= maxinterval;
      for (octave_idx_type i=0;i<dimension;i++) {
        for (octave_idx_type j=0;j<LENGTH;j++)
          if (input(j,i) >= 1.0)
            input(j,i) -= EPSMIN/2.0;
      }

      // Series is a pointer to data stored in input
      // so input(i,j) == series[j][i]
      // This is done for optimization purposes
      OCTAVE_LOCAL_BUFFER (const double *, series, dimension);
      for (octave_idx_type j = 0; j < dimension; j++)
        {
          const double *ptr = input.fortran_vec ();
          series[j] = ptr + LENGTH * j;
        }

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (double, deps, EPSCOUNT);
      OCTAVE_LOCAL_BUFFER (octave_idx_type *, which_dims, maxembed *dimension);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, which_dims_data,
                           2 * maxembed * dimension);
      for (octave_idx_type i=0;i<maxembed*dimension;i++)
        which_dims[i] = which_dims_data + i * 2;
      for (octave_idx_type i=0;i<maxembed;i++)
        for (octave_idx_type j=0;j<dimension;j++) {
          which_dims[i*dimension+j][0]=j;
          which_dims[i*dimension+j][1]=i;
        }

      std::list<std::vector<double>> histo_list;

      // Assign support variables
      double EPSFAKTOR;
      if (EPSCOUNT >1)
        EPSFAKTOR=pow(EPSMAX/EPSMIN,1.0/(double)(EPSCOUNT-1));
      else
        EPSFAKTOR=1.0;

      octave_idx_type length=LENGTH-(maxembed-1)*DELAY;

      if ( ! error_state)
        {
          // Calculate output
          double heps              = EPSMAX*EPSFAKTOR;
          octave_idx_type epsi_old = 0;
          for (octave_idx_type k=0;k<EPSCOUNT;k++)
            {

              octave_idx_type epsi_test;
              do {
                heps /= EPSFAKTOR;
                epsi_test=(octave_idx_type)(1./heps);
              } while (epsi_test <= epsi_old);

              octave_idx_type epsi = epsi_test;
              epsi_old             = epsi;
              deps[k]              = heps;

              std::vector <double> histo (maxembed * dimension, 0.0);
              start_box(series, which_dims, histo, maxembed, dimension, DELAY,
                        length, epsi, Q);

              if (Q != 1.0)
                for (std::vector<double>::iterator it = histo.begin ();
                     it != histo.end (); it++)
                  *it = log(*it)/(1.0-Q);

              histo_list.push_back (histo);
            }

          // Create and assign output
          dim_vector dv (maxembed, dimension);
          string_vector keys;
          keys.append (std::string("dim"));
          keys.append (std::string("entropy"));
          octave_map output (dv, keys);

          for (octave_idx_type i=0;i<maxembed*dimension;i++)
            {
              octave_scalar_map tmp (keys);
              // old fprintf(fHq,"#component = %d embedding = %d\n",
              //             which_dims[i][0]+1, which_dims[i][1]+1);

              tmp.setfield ("dim", which_dims[i][1]+1);

              // Create entropy output 
              Matrix entropy_out (EPSCOUNT, 3);

              std::list<std::vector<double>>::const_iterator it_hist;
              it_hist = histo_list.cbegin ();
              for (octave_idx_type j=0;j<EPSCOUNT;j++)
                {
                  if (i == 0)
                    {
                      // old fprintf(fHq,"%e %e %e\n",deps[j]*maxinterval,
                      //             histo_el->hist[i],histo_el->hist[i]);
                      entropy_out(j,0) = deps[j]*maxinterval;
                      entropy_out(j,1) = (*it_hist)[i];
                      entropy_out(j,2) = (*it_hist)[i];
                    }
                  else
                    {
                      // old fprintf(fHq,"%e %e %e\n",deps[j]*maxinterval,
                      //             histo_el->hist[i],
                      //             histo_el->hist[i]-histo_el->hist[i-1]);
                      entropy_out(j,0) = deps[j]*maxinterval;
                      entropy_out(j,1) = (*it_hist)[i];
                      entropy_out(j,2) = (*it_hist)[i]
                                         - (*it_hist)[i-1];
                    }
                  it_hist++;
                }

              tmp.setfield ("entropy",entropy_out);

              output.assign (idx_vector(which_dims[i][1]),
                             idx_vector(which_dims[i][0]),
                             tmp);
            }


          retval(0) = output;
        }
    }
  return retval;
}
