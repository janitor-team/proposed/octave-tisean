/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on make_box of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#include <octave/oct.h>

void make_box(double *ser,octave_idx_type **box,octave_idx_type *list,
              octave_idx_type l, octave_idx_type bs, octave_idx_type dim,
              octave_idx_type del, double eps)
{

  for (octave_idx_type x=0;x<bs;x++)
    for (octave_idx_type y=0;y<bs;y++)
      box[x][y] = -1;
  
  for (octave_idx_type i=(dim-1)*del;i<l;i++) {
    octave_idx_type x=(octave_idx_type )(ser[i-(dim-1)*del]/eps)&(bs-1);
    octave_idx_type y=(octave_idx_type )(ser[i]/eps)&(bs-1);
    list[i]=box[x][y];
    box[x][y]=i;
  }
}

