/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger
 * Modified: Piotr Held <pjheld@gmail.com> (2015). 
 * This function is based on find_multi_neighbors of 
 * TISEAN 3.0.1 https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#include <octave/oct.h>
#include <cmath>

octave_idx_type find_multi_neighbors(const Matrix &s,
                                     const MArray <octave_idx_type> &box,
                                     long *list,double **x,
                                     octave_idx_type bs,octave_idx_type dim,
                                     octave_idx_type emb,octave_idx_type del,
                                     double eps, unsigned long *flist)
{
  double dx=0.0;

  octave_idx_type nf=0;
  octave_idx_type ib=bs-1;
  octave_idx_type element;
  octave_idx_type k1,i2;

  octave_idx_type i=(octave_idx_type)(x[0][0]/eps)&ib;
  octave_idx_type j=(octave_idx_type)(x[dim-1][0]/eps)&ib;
  
  for (octave_idx_type i1=i-1;i1<=i+1;i1++) 
    {
      i2=i1&ib;
      for (octave_idx_type j1=j-1;j1<=j+1;j1++) 
        {
          element=box(j1&ib,i2);
          while (element != -1) 
            {
              for (octave_idx_type k=0;k<emb;k++) 
                {
                  k1= -k*del;
                  for (octave_idx_type li=0;li<dim;li++) 
                    {
                      dx=std::fabs(x[li][k1]-s(element+k1,li));
                      if (dx > eps)
                        break;
                    }
                  if (dx > eps)
                    break;
                }
              if (dx <= eps)
                flist[nf++]=element;
              element=list[element];
            }
        }
    }
  return nf;
}

octave_idx_type find_multi_neighbors(const double ** s,
                                     octave_idx_type **box,
                                     long *list,const double **x,
                                     octave_idx_type bs,octave_idx_type dim,
                                     octave_idx_type emb,octave_idx_type del,
                                     double eps, unsigned long *flist)
{
  double dx=0.0;
  octave_idx_type nf=0;
  octave_idx_type ib=bs-1;
  octave_idx_type element;
  octave_idx_type k1,i2;

  octave_idx_type i=(octave_idx_type)(x[0][0]/eps)&ib;
  octave_idx_type j=(octave_idx_type)(x[dim-1][0]/eps)&ib;
  
  for (octave_idx_type i1=i-1;i1<=i+1;i1++) 
    {
      i2=i1&ib;
      for (octave_idx_type j1=j-1;j1<=j+1;j1++) 
        {
          element=box[i2][j1&ib];
          while (element != -1) 
            {
              for (octave_idx_type k=0;k<emb;k++) 
                {
                  k1= -k*del;
                  for (octave_idx_type li=0;li<dim;li++) 
                    {
                      dx=std::fabs(x[li][k1]-s[li][element+k1]);
                      if (dx > eps)
                        break;
                    }
                  if (dx > eps)
                    break;
                }
              if (dx <= eps)
                flist[nf++]=element;
              element=list[element];
            }
        }
    }
  return nf;
}
