/* -*- coding: utf-8 -*- */
/* Copyright (C) 1996-2015 Piotr Held <pjheld@gmail.com>
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */
/********************************************************************/
/********************************************************************/

#ifndef TISEAN_RAND_H
#define TISEAN_RAND_H

#include <octave/oct.h>

class TISEAN_rand
{
  private:
  unsigned long rnd_array [9689], nexti [9689];
  unsigned long rnd1279[1279], next1279[1279];
  unsigned long rndtime,rndtime1,rndtime2,rndtime3;
  unsigned long t1279,t1279_1,t1279_2,t1279_3;
  unsigned long factor, rnd69;

  double lo_limit;

  unsigned long gausscount;
  double y;

  public:
  TISEAN_rand (unsigned long iseed);

  unsigned long rnd_long (void);
  unsigned long rnd_1279 (void);
  unsigned long rnd69069 (void);
  double gaussian (double sigma);
};


#endif
