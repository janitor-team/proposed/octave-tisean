/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger Last modified: Aug 14th, 1998 */
#include <octave/oct.h>

void solvele(double **mat,double *vec,octave_idx_type n)
{

  for (octave_idx_type i=0;i<n-1;i++) {
    double max=fabs(mat[i][i]);
    octave_idx_type maxi=i;
    for (octave_idx_type j=i+1;j<n;j++)
      {
        double h=fabs(mat[j][i]);
        if (h > max) {
        max=h;
        maxi=j;
        }
      }
    if (maxi != i) {
      double *mswap=mat[i];
      mat[i]=mat[maxi];
      mat[maxi]=mswap;
      double vswap=vec[i];
      vec[i]=vec[maxi];
      vec[maxi]=vswap;
    }
    
    double *hvec=mat[i];
    double pivot=hvec[i];
    if (fabs(pivot) == 0.0) {
      error_with_id ("Octave:tisean", "solvele: singular matrix!\n");
    }

    for (octave_idx_type j=i+1;j<n;j++) {
      double q= -mat[j][i]/pivot;
      mat[j][i]=0.0;
      for (octave_idx_type k=i+1;k<n;k++)
      mat[j][k] += q*hvec[k];
      vec[j] += q*vec[i];
    }
  }
  vec[n-1] /= mat[n-1][n-1];
  for (octave_idx_type i=n-2;i>=0;i--) {
    double *hvec=mat[i];
    for (octave_idx_type j=n-1;j>i;j--)
      vec[i] -= hvec[j]*vec[j];
    vec[i] /= hvec[i];
  }
}
