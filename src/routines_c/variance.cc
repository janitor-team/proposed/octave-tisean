/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger.
 * Modified: Piotr Held <pjheld@gmail.com>. 
 * This function is based on variance of TISEAN 3.0.1 https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#include <octave/oct.h>
#include <cmath>

void variance(const NDArray &s,octave_idx_type l,double *av,double *var)
{
  double h;
  
  *av= *var=0.0;

  for (octave_idx_type i=0;i<l;i++) {
    h=s(i);
    *av += h;
    *var += h*h;
  }
  *av /= (double)l;

  if ((double)l-(*av)*(*av) != 0.0)
    *var=sqrt(fabs((*var)/(double)l-(*av)*(*av)));

  if (*var == 0.0) {
    error_with_id ("Octave:invalid-input-arg", "variance of the data is zero");
  }
}
