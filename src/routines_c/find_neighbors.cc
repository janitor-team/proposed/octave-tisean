/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on find_neighbors of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
#include <octave/oct.h>

octave_idx_type find_neighbors(double *s,octave_idx_type **box,
                             octave_idx_type *list, double *x,
                             octave_idx_type l,octave_idx_type bs,
                             octave_idx_type dim, octave_idx_type del,
                             double eps, octave_idx_type *flist)
{

  octave_idx_type nf = 0;

  octave_idx_type k=(octave_idx_type)((dim-1)*del);
  octave_idx_type i=(octave_idx_type)(x[-k]/eps)&(bs-1);
  octave_idx_type j=(octave_idx_type)(x[0]/eps)&(bs-1);

  for (octave_idx_type i1=i-1;i1<=i+1;i1++) {
    octave_idx_type i2=i1&(bs-1);
    for (octave_idx_type j1=j-1;j1<=j+1;j1++) {
      octave_idx_type element=box[i2][j1&(bs-1)];
      while (element != -1) {
        for (k=0;k<dim;k++) {
          octave_idx_type k1= -k*del;
          double dx=fabs(x[k1]-s[element+k1]);
          if (dx > eps)
            break;
        }
        if (k == dim)
          flist[nf++]=element;
        element=list[element];
      }
    }
  }
  return nf;
}
