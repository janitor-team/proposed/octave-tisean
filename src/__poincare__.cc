/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on poincare of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/
#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#define DIRECTION_FROM_BELOW false

#include <octave/oct.h>

octave_idx_type poincare (const double *series, octave_idx_type length,
                          octave_idx_type embdim, octave_idx_type delay,
                          octave_idx_type comp, double where, bool direction,
                          Matrix &output)
{
  octave_idx_type jd;
  octave_idx_type count = 0;
  octave_idx_type column = 0;
  double delta,xcut;
  double time=0.0,lasttime=0.0;

  if (direction == DIRECTION_FROM_BELOW)
    {
      for (octave_idx_type i=(comp-1)*delay;i<length-(embdim-comp)*delay-1;i++)
        {
          if ((series[i] < where) && (series[i+1] >= where)) 
            {
              delta=(series[i]-where)/(series[i]-series[i+1]);
              time=(double)i+delta;
              if (lasttime > 0.0)
                {
                  column = 0;
                  for (octave_idx_type j= -(comp-1);j<=embdim-comp;j++) 
                    {
                      if (j != 0)
                        {
                          jd=i+j*delay;
                          xcut=series[jd]+delta*(series[jd+1]-series[jd]);
                          // old fprintf(stdout,"%e ",xcut);
                          output(count, column) = xcut;
                          column += 1;
                        }
                    }
                  // old fprintf(stdout,"%e\n",time-lasttime);
                  output(count, column) = time - lasttime;
                  count += 1;
                }
            }
          lasttime=time;
        }
    }
  else // direction is from above
    {
      for (octave_idx_type i=(comp-1)*delay;i<length-(embdim-comp)*delay-1;i++)
        {
          if ((series[i] > where) && (series[i+1] <= where)) 
            {
              delta=(series[i]-where)/(series[i]-series[i+1]);
              time=(double)i+delta;
              if (lasttime > 0.0)
                {
                  column = 0;
                  for (octave_idx_type j= -(comp-1);j<=embdim-comp;j++)
                    {
                      if (j != 0) 
                        {
                          jd=i+j*delay;
                          xcut=series[jd]+delta*(series[jd+1]-series[jd]);
                        // old fprintf(stdout,"%e ",xcut);
                          output(count, column) = xcut;
                          column += 1;
                        }
                    }
                  // old  fprintf(stdout,"%e\n",time-lasttime);
                  output(count, column) = time - lasttime;
                  count += 1;
                }
              lasttime=time;
            }
        }
    }
  return count;
}

DEFUN_DLD (__poincare__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 7)
    {
      print_usage ();
    }
  else
    {
      // Load data into local variables
      NDArray input            = args(0).array_value ();
      octave_idx_type embdim   = args(1).idx_type_value ();
      octave_idx_type delay    = args(2).idx_type_value ();
      octave_idx_type comp     = args(3).idx_type_value ();
      double where             = args(4).double_value ();
      bool direction           = args(5).bool_value ();
      octave_idx_type out_size = args(6).idx_type_value ();



        if ( ! error_state)
          {
            Matrix output (out_size, embdim);

            octave_idx_type count = poincare (input.fortran_vec(), 
                                              input.numel(), embdim,
                                              delay, comp, where, direction,
                                              output);
            // Resize output to fit sections found
            output.resize (count, embdim);

            retval(0) = output;
          }
    }
  return retval;
}
