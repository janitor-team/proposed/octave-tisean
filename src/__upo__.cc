/* -*- coding: utf-8 -*- */
/* Copyright (C) 1996-2015 Piotr Held <pjheld@gmail.com>
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */
/* Author: Piotr Held <pjheld@gmail.com>.
 * This function is based on upo of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/******************************************************************************/
/******************************************************************************/

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <octave/f77-fcn.h>

#if defined (OCTAVE_HAVE_F77_INT_TYPE)
#  define TO_F77_INT(x) octave::to_f77_int (x)
#else
typedef octave_idx_type F77_INT;
#  define TO_F77_INT(x) (x)
#endif

extern "C"
{
  F77_RET_T
  F77_FUNC (ts_upo, TS_UPO)
            (const F77_INT& m, const double& eps,
             const double& frac, const double& teq,
             const double& tdis, const double& h,
             const double& tacc, const F77_INT& iper,
             const F77_INT& icen, const F77_INT& lines_read,
             double* in_out1, double* olens,
             double* orbit_data, const F77_INT& sizedat,
             double* accuracy, double* stability);
}


DEFUN_DLD (__upo__, args, nargout, HELPTEXT)
{
  octave_value_list retval;
  int nargin = args.length ();


  if ((nargin != 10) || (nargout != 4))
    {
      print_usage ();
    }
  else
    {
    // Assigning inputs
      NDArray in_out1 = args(0).array_value();
      F77_INT m       = TO_F77_INT (args(1).int_value());
      double eps      = args(2).double_value();
      double frac     = args(3).double_value();
      double teq      = args(4).double_value();
      double tdis     = args(5).double_value();
      double h        = args(6).double_value();
      double tacc     = args(7).double_value();
      F77_INT iper    = TO_F77_INT (args(8).int_value());
      F77_INT icen    = TO_F77_INT (args(9).int_value());



      if (! error_state)
        {

          F77_INT lines_read = TO_F77_INT (in_out1.numel());
          // Generating output vectors with estimated lengths
          // The extra length (+1) is to store the actual lengths
          NDArray olens (dim_vector (icen+1,1));
          NDArray orbit_data (dim_vector ( (icen*20<lines_read?\
                                                    icen*20:lines_read)+1, 1));
          NDArray acc (dim_vector (icen+1,1));
          NDArray stability (dim_vector (icen+1,1));

          F77_XFCN (ts_upo, TS_UPO,
                    (m, eps, frac,teq, tdis, h, tacc, iper,icen, lines_read,
                     in_out1.fortran_vec(), olens.fortran_vec(), 
                     orbit_data.fortran_vec(), orbit_data.numel(), 
                     acc.fortran_vec(), stability.fortran_vec()));


          retval(0) = olens;
          retval(1) = orbit_data;
          retval(2) = acc;
          retval(3) = stability;
        }
    }
  return retval;
}
