/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on polynom of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"

double polynom(double *series, octave_idx_type DELAY, octave_idx_type N,
               octave_idx_type act,octave_idx_type dim,long cur,long fac)
{
  double ret=1.0;

  octave_idx_type n  = cur/fac;
  octave_idx_type hi = act-(dim-1)*DELAY;
  for (octave_idx_type j=1;j<=n;j++)
    ret *= series[hi];
  if (dim > 1) 
    ret *= polynom(series, DELAY, N,act,dim-1,cur-n*fac,fac/(N+1));

  return ret;
}

octave_idx_type number_pars(octave_idx_type DIM, octave_idx_type ord,
                            octave_idx_type start)
{

  octave_idx_type ret=0;

  if (ord == 1)
    for (octave_idx_type i=start;i<=DIM;i++)
      ret += 1;
  else
    for (octave_idx_type i=start;i<=DIM;i++)
      ret += number_pars(DIM, ord-1,i);

  return ret;
}

void make_coding(std::vector <octave_idx_type> &coding_vec, octave_idx_type N,
                 octave_idx_type ord, octave_idx_type d,
                 octave_idx_type fac, octave_idx_type cur)
{
  if (d == -1)
    coding_vec.push_back (cur);
  else
    for (octave_idx_type j=0;j<=ord;j++)
      make_coding(coding_vec, N, ord-j,d-1,fac*(N+1),cur+j*fac);
}

void make_fit(double *series, octave_idx_type *coding, double *results,
              octave_idx_type INSAMPLE, octave_idx_type N,
              octave_idx_type DIM, octave_idx_type DELAY,
              long maxencode, octave_idx_type pars)
{

  Matrix mat_b (pars,1);
  double *b = mat_b.fortran_vec ();

  Matrix mat (pars,pars);
  OCTAVE_LOCAL_BUFFER (double *, mat_arr, pars);

  for (octave_idx_type i=0;i<pars;i++)
    mat_arr[i]=mat.fortran_vec () + i * pars;

  for (octave_idx_type i=0;i<pars;i++) {
    b[i]=0.0;
    for (octave_idx_type j=0;j<pars;j++)
      mat_arr[i][j]=0.0;
  }

  for (octave_idx_type i=0;i<pars;i++)
    for (octave_idx_type j=i;j<pars;j++)
      for (octave_idx_type k=(DIM-1)*DELAY;k<INSAMPLE-1;k++)
        mat_arr[i][j] += polynom(series,DELAY,N,k,DIM,coding[i],maxencode)*
          polynom(series,DELAY,N,k,DIM,coding[j],maxencode);
  for (octave_idx_type i=0;i<pars;i++)
    for (octave_idx_type j=i;j<pars;j++)
      mat_arr[j][i]=(mat_arr[i][j] /= (INSAMPLE-1-(DIM-1)*DELAY));

  for (octave_idx_type i=0;i<pars;i++) {
    for (octave_idx_type j=(DIM-1)*DELAY;j<INSAMPLE-1;j++)
      b[i] += series[j+1]*polynom(series,DELAY,N,j,DIM,coding[i],maxencode);
    b[i] /= (INSAMPLE-1-(DIM-1)*DELAY);
  }

// old  solvele(mat_arr,b,pars);
  Matrix solved_vec = mat.solve (mat_b);
  double *solved_ptr = solved_vec.fortran_vec ();

  for (octave_idx_type i=0;i<pars;i++)
    results[i]=solved_ptr[i];

}

void decode(octave_idx_type N,octave_idx_type *out,int dim,long cur,long fac)
{

  octave_idx_type n=cur/fac;
  out[dim]=n;
  if (dim > 0) 
    decode(N,out,dim-1,cur-(long)n*fac,fac/(N+1));
}

double make_error(double *series, octave_idx_type *coding, double *results,
                  octave_idx_type N, octave_idx_type DIM,
                  octave_idx_type DELAY, long maxencode, octave_idx_type pars,
                  octave_idx_type i0, octave_idx_type i1)
{

  double err=0.0;
  for (octave_idx_type j=i0+(DIM-1)*DELAY;j<(long)i1-1;j++) {
    double h=0.0;
    for (octave_idx_type k=0;k<pars;k++) 
      h += results[k]*polynom(series,DELAY,N,j,DIM,coding[k],maxencode);
    err += (series[j+1]-h)*(series[j+1]-h);
  }
  return err /= (double)(i1-i0-(DIM-1)*DELAY);
}

void make_cast (NDArray &forecast, double *series, octave_idx_type *coding,
                double * results, octave_idx_type LENGTH,
                octave_idx_type CLENGTH, octave_idx_type N,
                octave_idx_type DIM, octave_idx_type DELAY, long maxencode,
                octave_idx_type pars,
                double std_dev)
{

  for (octave_idx_type i=0;i<=(DIM-1)*DELAY;i++)
    series[i]=series[LENGTH-(DIM-1)*DELAY-1+i];

  octave_idx_type hi=(DIM-1)*DELAY;
  forecast.resize (dim_vector (CLENGTH,1));
  for (octave_idx_type i=1;i<=CLENGTH;i++) 
    {
      double casted=0.0;
      for (octave_idx_type k=0;k<pars;k++)
        casted += results[k]*polynom(series,DELAY,N,(DIM-1)*DELAY,DIM,
                                     coding[k],maxencode);

    // old fprintf(fcast,"%e\n",casted*std_dev);
      forecast(i-1) = casted*std_dev;
      for (octave_idx_type j=0;j<(DIM-1)*DELAY;j++)
        series[j]=series[j+1];
      series[hi]=casted;
    }
}

DEFUN_DLD (__polynom__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 6 || nargout != 5)
  {
    print_usage ();
  }
  else
    {

      // Assign input
      NDArray input = args(0).array_value ();
      octave_idx_type DIM      = args(1).idx_type_value ();
      octave_idx_type DELAY    = args(2).idx_type_value ();
      octave_idx_type N        = args(3).idx_type_value ();
      octave_idx_type INSAMPLE = args(4).idx_type_value ();
      octave_idx_type CLENGTH  = args(5).idx_type_value ();

      octave_idx_type LENGTH = input.numel ();
      double *series = input.fortran_vec ();

      // Analyze inputs
      double std_dev, av;
      variance(input,LENGTH,&av,&std_dev);

      // Rescale input
      for (octave_idx_type i=0;i<LENGTH;i++)
        series[i] /= std_dev;

      // Create help values for the fit
      long maxencode=1;
      for (octave_idx_type i=1;i<DIM;i++)
        maxencode *= (N+1);

      octave_idx_type pars = 1;
      for (octave_idx_type i=1;i<=N;i++) {
        pars += number_pars(DIM, i,1);
      }

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (double, results, pars);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, opar, DIM);

      // Create coding
      std::vector <octave_idx_type> coding_vec;
      coding_vec.reserve (pars);
      make_coding(coding_vec,N,N,DIM-1,1,0);
      octave_idx_type *coding = coding_vec.data();

      if (! error_state)
        {
          // Promote warnings connected with singular matrixes to errors
          set_warning_state ("Octave:nearly-singular-matrix","error");
          set_warning_state ("Octave:singular-matrix","error");
 
          // Make the fit
          make_fit (series, coding, results, INSAMPLE, N, DIM, DELAY,
                    maxencode, pars);

          // If error encountered during the fit there is no sense to continue
          if (error_state)
            {
              return retval;
            }

          // Create outputs

          // Create output that contains the number of free parameters
          // old fprintf(file,"#number of free parameters= %d\n\n",pars);
          octave_idx_type free_par = pars;

          // Create output that contains the norm used for the fit
          // old fprintf(file,"#used norm for the fit= %e\n",std_dev);
          double fit_norm = std_dev;

          // Create coefficients output
          Matrix coeffs (pars, DIM + 1);
          for (octave_idx_type j=0;j<pars;j++)
            {
              decode(N,opar,DIM-1,coding[j],maxencode);
              octave_idx_type sumpar=0;
              for (octave_idx_type k=0;k<DIM;k++)
                {
                  sumpar += opar[k];
                //  old fprintf(file,"%d ",opar[k]);
                  coeffs(j, k) = opar[k];
                }
          //  old fprintf(file,"%e\n",results[j]
          //                          /pow(std_dev,(double)(sumpar-1)));
              coeffs(j,DIM) = results[j]/pow(std_dev,(double)(sumpar-1));

            }

          // Create sample error
          // 1st element of sample_error is the insample error
          // 2nd element of sample_error is the out of sample error (if exists)
          NDArray sample_err (dim_vector (1,1));

    // old in_error = make_error((unsigned long)0,INSAMPLE)
    //     fprintf(file,"#average insample error= %e\n",
    //             sqrt(in_error)*std_dev);
          sample_err(0) = sqrt(make_error(series, coding, results, N, DIM,
                                          DELAY, maxencode, pars,
                                          0,INSAMPLE))
                          * std_dev;

          if (INSAMPLE < LENGTH) 
            {
            // old out_error=make_error(INSAMPLE,LENGTH);
            //     fprintf(file,"#average out of sample error= %e\n",
            //             sqrt(out_error)*std_dev);
              sample_err.resize (dim_vector (2,1));
              sample_err(1) = sqrt (make_error (series, coding, results, N,
                                                DIM, DELAY, maxencode, pars,
                                                INSAMPLE, LENGTH)) 
                              * std_dev;
            }

          // Create forecast
          NDArray forecast (dim_vector (0,0));
          if (CLENGTH > 0)
            make_cast(forecast, series, coding, results, LENGTH, CLENGTH, N,
                      DIM, DELAY, maxencode, pars, std_dev);

          // Assign outputs
          retval(0) = free_par;
          retval(1) = fit_norm;
          retval(2) = coeffs;
          retval(3) = sample_err;
          retval(4) = forecast;
        }
    }
  return retval;
}
