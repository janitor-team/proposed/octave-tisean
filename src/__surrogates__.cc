/* -*- coding: utf-8 -*- */
/* Copyright (C) 1996-2015 Piotr Held <pjheld@gmail.com>
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */
/* Author: Piotr Held <pjheld@gmail.com>.
 * This function is based on surrogates of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/******************************************************************************/
/******************************************************************************/

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <octave/f77-fcn.h>
#include <octave/Cell.h>

#if defined (OCTAVE_HAVE_F77_INT_TYPE)
#  define TO_F77_INT(x) octave::to_f77_int (x)
#else
typedef octave_idx_type F77_INT;
#  define TO_F77_INT(x) (x)
#endif

extern "C"
{
  F77_RET_T
  F77_FUNC (ts_surrogates, TS_SURROGATES)
            (const double *xx, const F77_INT& nmaxp,
             const F77_INT& mcmax, const F77_INT& imax,
             const F77_INT& ispec, const double& seed, double *output,
             F77_INT& iterations, double& rel_discrepency);
}


DEFUN_DLD (__surrogates__, args, nargout, HELPTEXT)
{
  octave_value_list retval;
  int nargin = args.length ();

  if (nargin != 5)
    {
      print_usage ();
    }
  else
    {
    // Assigning inputs
      Matrix input          = args(0).matrix_value ();
      octave_idx_type nsur  = args(1).idx_type_value ();
      F77_INT imax          = TO_F77_INT (args(2).idx_type_value ());
      F77_INT ispec         = TO_F77_INT (args(3).idx_type_value ());
      double seed           = args(4).double_value ();

      if (! error_state)
        {

          F77_INT nmaxp = TO_F77_INT (input.rows ());
          F77_INT mcmax = TO_F77_INT (input.columns ());

          Cell surro_data (dim_vector (nsur,1));
          Matrix surro_tmp (input.dims ());
          Matrix pars (nsur, 2);

          for (octave_idx_type i = 0; i < nsur; i++)
            {
              F77_INT it_tmp;
              double rel_discrepency_tmp;

              F77_XFCN (ts_surrogates, TS_SURROGATES,
                        (input.fortran_vec (), nmaxp, mcmax, imax, ispec,
                         seed, surro_tmp.fortran_vec (), it_tmp,
                         rel_discrepency_tmp));

              surro_data(i) = surro_tmp;
              pars(i,0)     = it_tmp;
              pars(i,1)     = rel_discrepency_tmp;
            }

          retval(0) = surro_data;
          retval(1) = pars;
        }

    }
  return retval;
}
