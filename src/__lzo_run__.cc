/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lzo-run of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include "routines_c/tsa.h"
#include <octave/oct.h>

#define NMAX 128

octave_idx_type embed, DELAY, MINN;

void sort(const Matrix &series, octave_idx_type *found, 
          double *abstand, double **cast, octave_idx_type nfound,
          octave_idx_type hdim)
{
  octave_idx_type dim = series.columns ();

  for (octave_idx_type i=0;i<nfound;i++) {
    octave_idx_type hf=found[i];
    abstand[i]=0.0;
    for (octave_idx_type j=0;j<dim;j++) {
      for (octave_idx_type k=0;k<=hdim;k += DELAY) {
      double dx=fabs(series(hf-k,j)-cast[hdim-k][j]);
      if (dx > abstand[i]) abstand[i]=dx;
      }
    }
  }

  for (octave_idx_type  i=0;i<MINN;i++)
    for (octave_idx_type  j=i+1;j<nfound;j++)
      if (abstand[j]<abstand[i]) {
      double dswap=abstand[i];
      abstand[i]=abstand[j];
      abstand[j]=dswap;
      octave_idx_type iswap=found[i];
      found[i]=found[j];
      found[j]=iswap;
      }
}

void put_in_boxes(const Matrix &series, MArray<octave_idx_type> &box,
                  long *list, double epsilon, octave_idx_type hdim)
{

  double epsinv=1.0/epsilon;
  for (octave_idx_type i=0;i<NMAX;i++)
    for (octave_idx_type j=0;j<NMAX;j++)
      box(i,j)= -1;

  octave_idx_type LENGTH = series.rows ();
  octave_idx_type dim    = series.columns ();

  for (octave_idx_type n=hdim;n<LENGTH-1;n++) {
    octave_idx_type i=(octave_idx_type)(series(n,0)*epsinv)&(NMAX-1);
    octave_idx_type j=(octave_idx_type)(series(n-hdim,dim-1)*epsinv)&(NMAX-1);
    list[n]=box(i,j);
    box(i,j)=n;
  }
}

unsigned int hfind_neighbors(const Matrix &series,
                             const MArray<octave_idx_type> indexes,
                             const MArray<octave_idx_type> box,
                             const long *list,
                             double **cast,
                             octave_idx_type *found, double epsilon,
                             octave_idx_type hdim)
{

  octave_idx_type nfound=0;

  octave_idx_type dim = series.columns ();

  double epsinv=1.0/epsilon;
  octave_idx_type i=(octave_idx_type)(cast[hdim][0]*epsinv)&(NMAX-1);
  octave_idx_type j=(octave_idx_type)(cast[0][dim-1]*epsinv)&(NMAX-1);

  for (octave_idx_type i1=i-1;i1<=i+1;i1++) {
    octave_idx_type i2=i1&(NMAX-1);
    for (octave_idx_type j1=j-1;j1<=j+1;j1++) {
      octave_idx_type element=box(i2,j1&(NMAX-1));
      while (element != -1) {
        double max=0.0;
        bool toolarge = false;
        for (octave_idx_type l=0;(l<dim*embed) && (toolarge == false);l++) {
          octave_idx_type hc=indexes(l,0);
          octave_idx_type hd=indexes(l,1);
          double dx=fabs(series(element-hd,hc)-cast[hdim-hd][hc]);
          max=(dx>max) ? dx : max;
          if (max > epsilon) {
            toolarge=true;
          }
        }
        if (max <= epsilon)
          found[nfound++]=element;
        element=list[element];
      }
    }
  }
  return nfound;
}

void make_zeroth(const Matrix &series, TISEAN_rand &generator,
                 bool setnoise, const double *var, double Q,
                 const octave_idx_type *found,
                 int number,double *newcast)
{
  octave_idx_type len = series.rows ();
  octave_idx_type dim = series.columns ();

  for (octave_idx_type d=0;d<dim;d++) {
    newcast[d]=0.0;
//  old   sd=series[d]+1;
    const double *sd = series.fortran_vec() + d*len + 1;
    for (octave_idx_type i=0;i<number;i++)
      newcast[d] += sd[found[i]];
    newcast[d] /= (double)number;
  }

  if (setnoise) {
    for (octave_idx_type d=0;d<dim;d++)
      newcast[d] += generator.gaussian(var[d]*Q);
  }
}

DEFUN_DLD (__lzo_run__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if ((nargin != 12))
  {
    print_usage ();
  }
  else
    {

      Matrix input            = args(0).matrix_value ();
      embed                   = args(1).idx_type_value ();
      DELAY                   = args(2).idx_type_value ();
      octave_idx_type FLENGTH = args(3).idx_type_value ();
      MINN                    = args(4).idx_type_value ();
      bool setsort            = args(5).bool_value ();
      unsigned long seed      = args(6).ulong_value ();
      double EPS0             = args(7).double_value ();
      bool epsset             = args(8).bool_value ();
      double EPSF             = args(9).double_value ();
      double Q                = args(10).double_value ();
      bool setnoise           = args(11).bool_value ();

      octave_idx_type LENGTH = input.rows();
      octave_idx_type dim = input.columns();
      octave_idx_type hdim=(embed-1)*DELAY+1;

      OCTAVE_LOCAL_BUFFER (double, min, dim);
      OCTAVE_LOCAL_BUFFER (double, interval, dim);
      OCTAVE_LOCAL_BUFFER (double, var, dim);

      double maxinterval=0.0;

      for (octave_idx_type i=0;i<dim;i++) {
        rescale_data(input,i,LENGTH,&min[i],&interval[i]);
        double dummy;
        variance(input,LENGTH,&dummy,&var[i]);
        if (interval[i] > maxinterval)
          maxinterval=interval[i];
      }

      if (epsset)
        EPS0 /= maxinterval;

      Matrix cast_mat (dim, hdim);
      OCTAVE_LOCAL_BUFFER (double *, cast, hdim);

      for (octave_idx_type i=0;i<hdim;i++)
        cast[i] = cast_mat.fortran_vec () + dim * i;


      OCTAVE_LOCAL_BUFFER (double, newcast, dim);
      OCTAVE_LOCAL_BUFFER (long, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);
      OCTAVE_LOCAL_BUFFER (double, abstand, LENGTH);

      MArray<octave_idx_type> box (dim_vector(NMAX,NMAX));

      if ( ! error_state)
        {
          for (octave_idx_type j=0;j<dim;j++)
            for (octave_idx_type i=0;i<hdim;i++)
              cast[i][j]=input(LENGTH-hdim+i,j);

        // old  indexes=make_multi_index(dim,embed,DELAY);

          octave_idx_type alldim=dim * embed;

          MArray<octave_idx_type> indexes (dim_vector (alldim, 2));
          for (octave_idx_type i=0;i<alldim;i++) 
            {
              indexes(i,0)=i%dim;
              indexes(i,1)=(i/dim)*DELAY;
            }

         // end old index = make_multi_index();

      //   old rnd_init(seed);
          TISEAN_rand generator (seed);

          double epsilon0=EPS0/EPSF;

          if (setnoise) 
            Q /= 100.0;

          Matrix output (FLENGTH, dim);
          octave_idx_type row_count = 0;
          octave_idx_type count = 1;
          for (octave_idx_type i=0;i<FLENGTH;i++)
            {
              bool done=0;
              double epsilon;
              if (setsort)
                epsilon= epsilon0/((double)count*EPSF);
              else
                epsilon=epsilon0;
              while (!done) {
                epsilon*=EPSF;
                put_in_boxes(input, box, list, epsilon,(embed-1)*DELAY);
                octave_idx_type actfound=hfind_neighbors(input, indexes, box,
                                                         list, cast, found,
                                                         epsilon,
                                                         (embed-1) * DELAY);
                if (actfound >= MINN) {
                  if (setsort) {
                    epsilon0 += epsilon;
                    count++;
                    sort(input, found, abstand, cast, actfound, (embed-1)*DELAY);
                    actfound=MINN;
                  }
                  make_zeroth(input, generator, setnoise, var, Q, found,
                              actfound, newcast);

                  for (octave_idx_type j=0;j<dim-1;j++)
                  {
                    // old printf("%e ",newcast[j]*interval[j]+min[j]);
                    output(row_count, j) = newcast[j]*interval[j]+min[j];
                  }
                  // old printf("%e\n",newcast[dim-1]*interval[dim-1]+min[dim-1]);
                  output(row_count, dim-1) = newcast[dim-1]*interval[dim-1]
                                             + min[dim-1];
                  row_count += 1;

                  done=1;
                  double *swap=cast[0];
                  for (octave_idx_type j=0;j<hdim-1;j++)
                    cast[j]=cast[j+1];
                  cast[hdim-1]=swap;
                  for (octave_idx_type j=0;j<dim;j++)
                    cast[hdim-1][j]=newcast[j];
                }
              }
            }

            if (row_count != 0)
            {
              output.resize (row_count, dim);
              retval(0) = output;
            }
            else 
              retval(0) = Matrix (0,0);
        }
    }
  return retval;
}
