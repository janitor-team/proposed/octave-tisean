/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lfo-ar of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"

/*number of boxes for the neighbor search algorithm*/
#define NMAX 256

void multiply_matrix(double **mat,double *vec, octave_idx_type dim,
                     octave_idx_type embed)
{

  OCTAVE_LOCAL_BUFFER (double, hvec, dim*embed);

  for (octave_idx_type i=0;i<dim*embed;i++) {
    hvec[i]=0.0;
    for (octave_idx_type j=0;j<dim*embed;j++)
      hvec[i] += mat[i][j]*vec[j];
  }
  for (octave_idx_type i=0;i<dim*embed;i++)
    vec[i]=hvec[i];
}

void make_fit(const double **series,
              octave_idx_type* found,
              double *error_array, long act,
              octave_idx_type dim, octave_idx_type embed,
              octave_idx_type delay, octave_idx_type STEP,
              octave_idx_type number)
{

  OCTAVE_LOCAL_BUFFER (double, foreav, dim);
  OCTAVE_LOCAL_BUFFER (double, localav, embed * dim);
  
  for (octave_idx_type i=0;i<embed*dim;i++)
    localav[i]=0;
  for (octave_idx_type i=0;i<dim;i++)
    foreav[i]=0.0;
  
  for (octave_idx_type n=0;n<number;n++) {
    octave_idx_type which=found[n];
    for (octave_idx_type j=0;j<dim;j++) {
      //old sj=series[j];
      const double *sj = series[j];
      foreav[j] += sj[which+STEP];
      for (octave_idx_type j1=0;j1<embed;j1++) {
      octave_idx_type hj=j*embed+j1;
      localav[hj] += sj[which-j1*delay];
      }
    }
  }

  for (octave_idx_type i=0;i<dim*embed;i++)
    localav[i] /= number;
  for (octave_idx_type i=0;i<dim;i++)
    foreav[i] /= number;


  Matrix mat (embed * dim, embed * dim);

  // mat_arr points to the same data as mat
  // so mat(i,j) == mat_arr[j][i]
  OCTAVE_LOCAL_BUFFER (double *, mat_arr, embed * dim);
  for (octave_idx_type j = 0; j < embed * dim; j++)
    {
      double *ptr = mat.fortran_vec ();
      mat_arr[j] = ptr + embed * dim * j;
    }

  for (octave_idx_type i=0;i<dim;i++) {
    //old si=series[i];
    const double *si = series[i];
    for (octave_idx_type i1=0;i1<embed;i1++) {
      octave_idx_type hi=i*embed+i1;
      double lavi=localav[hi];
      octave_idx_type hi1=i1*delay;
      for (octave_idx_type j=0;j<dim;j++) {
     //old sj=series[j];
      const double *sj = series[j];
      for (octave_idx_type j1=0;j1<embed;j1++) {
        octave_idx_type hj=j*embed+j1;
        double lavj=localav[hj];
        octave_idx_type hj1=j1*delay;
        mat_arr[hi][hj]=0.0;
        if (hj >= hi) {
          for (octave_idx_type n=0;n<number;n++) {
            octave_idx_type which=found[n];
            mat_arr[hi][hj] += (si[which-hi1]-lavi)*(sj[which-hj1]-lavj);
          }
        }
      }
      }
    }
  }

  for (octave_idx_type i=0;i<dim*embed;i++)
    for (octave_idx_type j=i;j<dim*embed;j++) {
      mat_arr[i][j] /= number;
      mat_arr[j][i] = mat_arr[i][j];
    }

  Matrix vec (embed * dim, 1);
  double *vec_arr = vec.fortran_vec ();

  for (octave_idx_type i=0;i<dim;i++) {
    //old si=series[i];
    const double *si = series[i];
    double fav=foreav[i];
    for (octave_idx_type j=0;j<dim;j++) {
      //old sj=series[j];
      const double *sj = series[j];
      for (octave_idx_type j1=0;j1<embed;j1++) {
      octave_idx_type hj=j*embed+j1;
      double lavj=localav[hj];
      octave_idx_type hj1=j1*delay;
      vec_arr[hj] =0.0;
      for (octave_idx_type n=0;n<number;n++) {
        octave_idx_type which=found[n];
        vec_arr[hj] += (si[which+STEP]-fav)*(sj[which-hj1]-lavj);
      }
      vec_arr[hj] /= number;
      }
    }

//    Version that uses TISEAN solvele

//    OCTAVE_LOCAL_BUFFER (double, imat_data, sqr(dim*embed));
//    OCTAVE_LOCAL_BUFFER (double *, imat, dim*embed);
//    for (octave_idx_type j=0;j<dim*embed;j++)
//      {
//        imat[j] = imat_data + dim*embed *j;
//      }
//    invert_matrix(mat_arr,imat,dim*embed);
//    multiply_matrix(imat,vec_arr,dim,embed);
//    double *solved_vec_arr = vec_arr;

// Below is version that uses Octave's Matrix::solve()
// It is slower, than generating an inverse matrix, but gives warnings 
// (which are treated as errors) when a near singular matrix is encountered.

    Matrix solved_vec = mat.solve(vec);
    double *solved_vec_arr = solved_vec.fortran_vec ();

    // If errors were raised, there is no sense in countinueing
    if (error_state)
      {
        return ;
      }

    double cast=foreav[i];
    for (octave_idx_type j=0;j<dim;j++) {

      const double *sj = series[j];
      for (octave_idx_type j1=0;j1<embed;j1++) {
      octave_idx_type hj=j*embed+j1;
      cast += solved_vec_arr[hj]*(sj[act-j1*delay]-localav[hj]);
      }
    }
    error_array[i] += sqr(cast-series[i][act+STEP]);
  }
}

DEFUN_DLD (__lfo_ar__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 12)
  {
    print_usage ();
  }
  else
    {

      // Assign input
      Matrix input            = args(0).matrix_value ();
      octave_idx_type embed   = args(1).idx_type_value ();
      octave_idx_type delay   = args(2).idx_type_value ();
      octave_idx_type CLENGTH = args(3).idx_type_value ();
      double EPS0             = args(4).double_value ();
      bool eps0set            = args(5).bool_value ();
      double EPS1             = args(6).double_value ();
      bool eps1set            = args(7).bool_value ();
      double EPSF             = args(8).double_value ();
      octave_idx_type STEP    = args(9).idx_type_value ();
      unsigned long causal    = args(10).ulong_value ();
      bool verbose            = args(11).bool_value ();

      octave_idx_type dim    = input.columns ();
      octave_idx_type LENGTH = input.rows ();

      // Analyze data
      double interval, min;
      double maxinterval=0.0;
      for (octave_idx_type i=0;i<dim;i++) {
        rescale_data(input,i,LENGTH,&min,&interval);
        if (interval > maxinterval)
          maxinterval=interval;
      }
      interval=maxinterval;

      // Allocate memory

      // series is a pointer to that data in input
      // this is done to for code optimization
      OCTAVE_LOCAL_BUFFER (const double *, series, dim);
      for (octave_idx_type j = 0; j < dim; j++)
        {
          const double *ptr = input.fortran_vec ();
          series[j] = ptr + LENGTH * j;
        }

      OCTAVE_LOCAL_BUFFER (long, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);
      OCTAVE_LOCAL_BUFFER (unsigned long, hfound, LENGTH);

      MArray<octave_idx_type> box_mat (dim_vector(NMAX, NMAX));
      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, NMAX);
      for (octave_idx_type j = 0; j < NMAX; j++)
        {
          octave_idx_type *ptr = box_mat.fortran_vec ();
          box[j] = ptr + NMAX * j;
        }

      OCTAVE_LOCAL_BUFFER (double, error_array, dim);
      OCTAVE_LOCAL_BUFFER (double, hrms, dim);
      OCTAVE_LOCAL_BUFFER (double, hav, dim);
      OCTAVE_LOCAL_BUFFER (const double*, hser, dim);

      if (eps0set)
        EPS0 /= interval;
      if (eps1set)
        EPS1 /= interval;

      octave_idx_type clength=(CLENGTH <= LENGTH) ? CLENGTH-STEP : LENGTH-STEP;

      if ( ! error_state)
        {

          // Promote warnings connected with singular matrixes to errors
          set_warning_state ("Octave:nearly-singular-matrix","error");
          set_warning_state ("Octave:singular-matrix","error");

          // Estimate maximum possible output size
          octave_idx_type output_rows = (octave_idx_type)
                                        ((log(EPS1) - log(EPS0)) / log (EPSF));
          output_rows += 2;

          // Create output
          Matrix output (output_rows,dim+4);
          octave_idx_type count = 0;

          if (verbose)
            printf("\nStarting new dataset\n\n");

          for (double epsilon=EPS0;epsilon<EPS1*EPSF;epsilon*=EPSF) 
            {
              if (verbose)
                {
                  printf ("For epsilon = %e, the count = %lu\n",
                          epsilon, (unsigned long)count);
                  fflush (stdout);
                }

              long pfound=0;
              for (octave_idx_type i=0;i<dim;i++)
                error_array[i]=hrms[i]=hav[i]=0.0;
              double avfound=0.0;
              make_multi_box(series,box,list,LENGTH-STEP,NMAX,dim,
                             embed,delay,epsilon);
              for (octave_idx_type i=(embed-1)*delay;i<clength;i++) 
                {
                  for (octave_idx_type j=0;j<dim;j++)
                    hser[j] = series[j] + i;

                  octave_idx_type actfound;
                  actfound=find_multi_neighbors(series,box,list,hser,NMAX,
                                                dim,embed,delay, epsilon,
                                                hfound);
                  actfound=exclude_interval(actfound,i-causal+1,
                                            i+causal+(embed-1) *delay-1,
                                            hfound,found);
                  if (actfound > 2*(dim*embed+1))
                    {
                      make_fit (series, found, error_array,
                                i,dim, embed, delay, STEP, actfound);
                      // Checking if the fit was correct
                      // If any errors were raised: end function
                      if (error_state)
                        {
                          return retval;
                        }
                      pfound++;
                      avfound += (double)(actfound-1);
                      for (octave_idx_type j=0;j<dim;j++) 
                        {
                          hrms[j] += series[j][i+STEP] * series[j][i+STEP];
                          hav[j] += series[j][i+STEP];
                        }
                    }
                }
              if (pfound > 1) 
                {
                  double sumerror=0.0;
                  for (octave_idx_type j=0;j<dim;j++)
                    {
                      hav[j] /= pfound;
                      hrms[j]=sqrt(fabs(hrms[j]/(pfound-1)-hav[j]*hav[j]*pfound
                                        /(pfound-1)));
                      error_array[j]=sqrt(error_array[j]/pfound)/hrms[j];
                      sumerror += error_array[j];
                    }
             
                
              // old fprintf(stdout,"%e %e ",epsilon*interval,
              //                          sumerror/(double)dim);
                  output(count, 0) = epsilon*interval;
                  output(count, 1) = sumerror/(double)dim;
                  for (octave_idx_type j=0;j<dim;j++)
                    {
                    //old fprintf(stdout,"%e ",error_array[j]);
                      output(count, 2 + j) = error_array[j];
                    }
              // old  fprintf(stdout,"%e %e\n",(double)pfound
              //          /(clength-(embed-1)*delay),
              //          avfound/pfound);
                  output(count, 2 + dim) = (double)pfound
                                           /(clength-(embed-1)*delay);
                  output(count, 2 + dim + 1) = avfound/pfound;

                  count += 1;
                }
            }
          // Resize output to fit actual results instead of
          // an educated guess
          // if count == 0 then the output will be an 0x4+dim matrix
          output.resize (count, dim + 4);

          retval(0) = output;
        }
    }
  return retval;
}
