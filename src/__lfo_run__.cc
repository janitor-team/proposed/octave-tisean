/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lfo-run of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"

#define NMAX 128

void put_in_boxes(const double **series, octave_idx_type LENGTH,
                  octave_idx_type *list, octave_idx_type **box, double epsilon,
                  octave_idx_type dim,octave_idx_type embed,
                  octave_idx_type DELAY)
{

  octave_idx_type hdim=(embed-1)*DELAY;
  double epsinv=1.0/epsilon;
  for (octave_idx_type i=0;i<NMAX;i++)
    for (octave_idx_type j=0;j<NMAX;j++)
      box[i][j]= -1;

  for (octave_idx_type n=hdim;n<LENGTH-1;n++) {
    octave_idx_type i=(octave_idx_type)(series[0][n]*epsinv)&(NMAX-1);
    octave_idx_type j=(octave_idx_type)(series[dim-1][n-hdim]*epsinv)
                      &(NMAX-1);
    list[n]=box[i][j];
    box[i][j]=n;
  }
}

octave_idx_type hfind_neighbors(const double **series, double **cast,
                                octave_idx_type *found, octave_idx_type *list,
                                octave_idx_type **box, double epsilon,
                                octave_idx_type dim, octave_idx_type embed,
                                octave_idx_type DELAY)
{

  octave_idx_type nfound=0;
  octave_idx_type hdim=(embed-1)*DELAY;

  double epsinv=1.0/epsilon;

  octave_idx_type i=(octave_idx_type)(cast[hdim][0]*epsinv)&(NMAX-1);
  octave_idx_type j=(octave_idx_type)(cast[0][dim-1]*epsinv)&(NMAX-1);
  
  for (octave_idx_type i1=i-1;i1<=i+1;i1++) {
    octave_idx_type i2=i1&(NMAX-1);
    for (octave_idx_type j1=j-1;j1<=j+1;j1++) {
      octave_idx_type element=box[i2][j1&(NMAX-1)];
      while (element != -1) {
      double max=0.0;
      bool toolarge=0;
      for (octave_idx_type l=0;l<dim;l++) {
        for (octave_idx_type k=0;k<=hdim;k += DELAY) {
          double dx=fabs(series[l][element-k]-cast[hdim-k][l]);
          max=(dx>max) ? dx : max;
          if (max > epsilon) {
            toolarge=1;
            break;
          }
        }
        if (toolarge)
          break;
      }
      if (max <= epsilon)
        found[nfound++]=element;
      element=list[element];
      }
    }
  }
  return nfound;
}

void multiply_matrix(double **mat,double *vec, octave_idx_type DIM)
{

  OCTAVE_LOCAL_BUFFER (double, hvec, DIM);
  for (octave_idx_type i=0;i<DIM;i++) {
    hvec[i]=0.0;
    for (octave_idx_type j=0;j<DIM;j++)
      hvec[i] += mat[i][j]*vec[j];
  }
  for (octave_idx_type i=0;i<DIM;i++)
    vec[i]=hvec[i];

}

void make_fit(const double **series, double **cast, octave_idx_type *found,
              octave_idx_type dim, octave_idx_type embed,
              octave_idx_type DELAY,
              int number,double *newcast)
{

  octave_idx_type hdim=(embed-1)*DELAY;

  OCTAVE_LOCAL_BUFFER (double, localav, dim * embed);
  OCTAVE_LOCAL_BUFFER (double, foreav, dim);

  for (octave_idx_type i=0;i<dim*embed;i++)
    localav[i]=0.0;
  for (octave_idx_type i=0;i<dim;i++)
    foreav[i]=0.0;

  for (octave_idx_type n=0;n<number;n++) {
    octave_idx_type which=found[n];
    for (octave_idx_type j=0;j<dim;j++) {
      const double *sj=series[j];
      foreav[j] += sj[which+1];
      for (octave_idx_type j1=0;j1<embed;j1++) {
      octave_idx_type hj=j*embed+j1;
      localav[hj] += sj[which-j1*DELAY];
      }
    }
  }

  for (octave_idx_type i=0;i<dim*embed;i++)
    localav[i] /= number;
  for (octave_idx_type i=0;i<dim;i++)
    foreav[i] /= number;

  Matrix mat (dim * embed, dim * embed);
  // mat_arr points to the same data as mat
  // so mat(i,j) == mat_arr[j][i]
  OCTAVE_LOCAL_BUFFER (double *, mat_arr, dim * embed);
  for (octave_idx_type j = 0; j < dim * embed; j++)
    {
      double *ptr = mat.fortran_vec ();
      mat_arr[j] = ptr + dim * embed * j;
    }

  for (octave_idx_type i=0;i<dim;i++) {
    const double *si=series[i];
    for (octave_idx_type i1=0;i1<embed;i1++) {
      octave_idx_type hi=i*embed+i1;
      double lavi=localav[hi];
      octave_idx_type hi1=i1*DELAY;
      for (octave_idx_type j=0;j<dim;j++) {
      const double *sj=series[j];
      for (octave_idx_type j1=0;j1<embed;j1++) {
        octave_idx_type hj=j*embed+j1;
        double lavj=localav[hj];
        octave_idx_type hj1=j1*DELAY;
        mat_arr[hi][hj]=0.0;
        if (hj >= hi) {
          for (octave_idx_type n=0;n<number;n++) {
            octave_idx_type which=found[n];
            mat_arr[hi][hj] += (si[which-hi1]-lavi)*(sj[which-hj1]-lavj);
          }
        }
      }
      }
    }
  }
  
  for (octave_idx_type i=0;i<dim*embed;i++)
    for (octave_idx_type j=i;j<dim*embed;j++) {
      mat_arr[i][j] /= number;
      mat_arr[j][i]=mat_arr[i][j];
    }



  Matrix vec (dim * embed,1);
  double *vec_arr = vec.fortran_vec ();

  for (octave_idx_type i=0;i<dim;i++)
    {
      const double *si=series[i];
      double fav=foreav[i];
      for (octave_idx_type j=0;j<dim;j++) {
        const double *sj=series[j];
        for (octave_idx_type j1=0;j1<embed;j1++) {
        octave_idx_type hj=j*embed+j1;
        double lavj=localav[hj];
        octave_idx_type hj1=j1*DELAY;
        vec_arr[hj]=0.0;
        for (octave_idx_type n=0;n<number;n++) {
          octave_idx_type which=found[n];
          vec_arr[hj] += (si[which+1]-fav)*(sj[which-hj1]-lavj);
        }
        vec_arr[hj] /= number;
        }
      }

// The commented version is faster, but does not account for 
// singular or near singular matrixes. It is left here for reference.
//      OCTAVE_LOCAL_BUFFER (double, imat_data, sqr(dim * embed));
//      OCTAVE_LOCAL_BUFFER (double *, imat, dim * embed);
//      for (octave_idx_type j=0;j<dim * embed;j++)
//        {
//          imat[j] = imat_data + dim * embed *j;
//        }
//      invert_matrix(mat_arr,imat,dim * embed);
//      multiply_matrix(imat,vec_arr, dim * embed);
//      double *solved_vec_arr = vec_arr;

// Below is version that uses Octave's Matrix::solve().
// It is slower, than generating an inverse matrix, but gives warnings 
// (which are treated as errors) when a near singular matrix is encountered.

      Matrix solved_vec      = mat.solve (vec);
      double *solved_vec_arr = solved_vec.fortran_vec ();

    // If errors were raised (a singular matrix was encountered), 
    // there is no sense in countinuing
    if (error_state)
      {
        return ;
      }

      newcast[i]=foreav[i];
      for (octave_idx_type j=0;j<dim;j++) {
        for (octave_idx_type j1=0;j1<embed;j1++) {
        octave_idx_type hj=j*embed+j1;
        newcast[i] += solved_vec_arr[hj]*(cast[hdim-j1*DELAY][j]-localav[hj]);
        }
      }
    }
}

void make_zeroth(const double **series, octave_idx_type *found,
                 octave_idx_type dim,
                 int number,double *newcast)
{
  
  for (octave_idx_type d=0;d<dim;d++) {
    newcast[d]=0.0;
    const double *sj=series[d]+1;
    for (octave_idx_type i=0;i<number;i++)
      newcast[d] += sj[found[i]];
    newcast[d] /= number;
  }
}

DEFUN_DLD (__lfo_run__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 9)
  {
    print_usage ();
  }
  else
    {

      // Assign inputs
      Matrix input            = args(0).matrix_value ();
      octave_idx_type embed   = args(1).idx_type_value ();
      octave_idx_type DELAY   = args(2).idx_type_value ();
      octave_idx_type FLENGTH = args(3).idx_type_value ();
      octave_idx_type MINN    = args(4).idx_type_value ();
      double EPS0             = args(5).double_value ();
      bool epsset             = args(6).bool_value ();
      double EPSF             = args(7).double_value ();
      bool do_zeroth          = args(8).bool_value ();

      octave_idx_type LENGTH = input.rows ();
      octave_idx_type dim    = input.columns ();

      // Assign help values
      octave_idx_type hdim=(embed-1)*DELAY+1;

      // Series is a pointer to data stored in input
      // so input(i,j) == series[j][i]
      // This is done for optimization purposes
      OCTAVE_LOCAL_BUFFER (const double *, series, dim);
      for (octave_idx_type j = 0; j < dim; j++)
        {
          const double *ptr = input.fortran_vec ();
          series[j] = ptr + LENGTH * j;
        }

      // Analyze input
      OCTAVE_LOCAL_BUFFER (double, min_array, dim);
      OCTAVE_LOCAL_BUFFER (double, interval, dim);

      double maxinterval=0.0;
      for (octave_idx_type i=0;i<dim;i++) {
        rescale_data(input,i,LENGTH,&min_array[i],&interval[i]);
        if (interval[i] > maxinterval)
          maxinterval=interval[i];
      }

      if (epsset)
        EPS0 /= maxinterval;

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (double *, cast, hdim);
      OCTAVE_LOCAL_BUFFER (double, cast_data, hdim * dim);
      for (octave_idx_type j=0;j<hdim;j++)
        cast[j]=cast_data + j * dim;

      OCTAVE_LOCAL_BUFFER (double, newcast, dim);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);

      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, NMAX);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, box_data, NMAX * NMAX);
      for (octave_idx_type j=0;j<NMAX;j++)
        box[j]=box_data + j * NMAX;

      for (octave_idx_type j=0;j<dim;j++)
        for (octave_idx_type i=0;i<hdim;i++)
          cast[i][j]=series[j][LENGTH-hdim+i];

      if ( ! error_state)
        {

          // Promote warnings connected with singular matrixes to errors
          set_warning_state ("Octave:nearly-singular-matrix","error");
          set_warning_state ("Octave:singular-matrix","error");

          // Calculate the maximum epsilon that makes sense
          // On the basis of 'i' and 'j' from put_in_boxes ()
          NDArray input_max = input.max ();
          double maximum_epsilon = (input_max(0) > input_max(dim-1))
                                   ? input_max(0) : input_max(dim-1);
          maximum_epsilon *= EPSF;

          // Create output
          Matrix output (FLENGTH, dim);
          for (octave_idx_type i=0;i<FLENGTH;i++)
            {
              bool done=0;
              double epsilon=EPS0/EPSF;
              while (!done) 
                {
                  // If epsilon became too large 
                  // there is no sense in continuing
                  if (epsilon > maximum_epsilon)
                    {
                      error_with_id ("Octave:tisean", "The neighbourhood size"
                                     " became too large during search,"
                                     " no sense continuing");
                      return retval;
                    }

                  epsilon*=EPSF;
                  put_in_boxes(series, LENGTH, list, box, epsilon, dim, embed,
                               DELAY);
                  octave_idx_type actfound;
                  actfound=hfind_neighbors (series, cast, found, list, box,
                                            epsilon, dim, embed, DELAY);
                  if (actfound >= MINN)
                    {
                      if (!do_zeroth)
                        make_fit(series, cast, found, dim, embed, DELAY,
                                 actfound, newcast);
                      else
                        make_zeroth(series, found, dim, actfound,newcast);

                      // Checking if the fit was correct
                      // If any errors were raised: end function
                      if (error_state)
                        {
                          return retval;
                        }

                      for (octave_idx_type j=0;j<dim;j++)
                        {
                      // old printf("%e ",newcast[j]*interval[j]+min_array[j]);
                          output(i,j) = newcast[j]*interval[j]+min_array[j];
                        }

                      done=1;
                      for (octave_idx_type j=0;j<dim;j++) 
                        {
                          // If this occurs there is no sense to continue
                          if ((newcast[j] > 2.0) || (newcast[j] < -1.0)) 
                            {
                              error_with_id("Octave:tisean","forecast failed, "
                                            "escaping data region");
                              return retval;
                            }
                        }
                      double *swap=cast[0];
                      for (octave_idx_type j=0;j<hdim-1;j++)
                        cast[j]=cast[j+1];
                      cast[hdim-1]=swap;
                      for (octave_idx_type j=0;j<dim;j++)
                        cast[hdim-1][j]=newcast[j];
                    }
                }
            }
          retval(0) = output;
        }
    }
  return retval;
}
