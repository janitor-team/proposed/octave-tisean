/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lyap_spec of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <octave/oct-map.h>
#include "routines_c/tsa.h"
#include <limits>

#define OUT 10

#define BOX 512
#define EPSMAX 1.0
// the delay vector is calculated in the associated m-file
// according to authors of TISEAN the DELAY > 1 did not work for some reason
#define DELAY 1

double sort(const double **series, unsigned long *found,
            octave_idx_type **indexes, double epsmin, bool epsset,
            octave_idx_type alldim, octave_idx_type MINNEIGHBORS,
            octave_idx_type LENGTH, long act,
            octave_idx_type *nfound, bool *enough)
{
  double maxeps=0.0,dx,dswap,maxdx;
  long self=0,i,j,del,hf,iswap,n1;
  octave_idx_type imax = *nfound;

  *enough=0;

  OCTAVE_LOCAL_BUFFER (double, abstand, LENGTH);

  for (i=0;i<imax;i++) {
    hf=found[i];
    if (hf != act) {
      maxdx=fabs(series[0][act]-series[0][hf]);
      for (j=1;j<alldim;j++) {
        n1=indexes[0][j];
        del=indexes[1][j];
        dx=fabs(series[n1][act-del]-series[n1][hf-del]);
        if (dx > maxdx) maxdx=dx;
      }
      abstand[i]=maxdx;
    }
    else {
      self=i;
    }
  }

  if (self != (imax-1)) {
    abstand[self]=abstand[imax-1];
    found[self]=found[imax-1];
  }

  for (i=0;i<MINNEIGHBORS;i++) {
    for (j=i+1;j<imax-1;j++) {
      if (abstand[j]<abstand[i]) {
        dswap=abstand[i];
        abstand[i]=abstand[j];
        abstand[j]=dswap;
        iswap=found[i];
        found[i]=found[j];
        found[j]=iswap;
      }
    }
  }

  if (!epsset || (abstand[MINNEIGHBORS-1] >= epsmin)) {
    *nfound=MINNEIGHBORS;
    *enough=1;
    maxeps=abstand[MINNEIGHBORS-1];

    return maxeps;
  }

  for (i=MINNEIGHBORS;i<imax-2;i++) {
    for (j=i+1;j<imax-1;j++) {
      if (abstand[j]<abstand[i]) {
        dswap=abstand[i];
        abstand[i]=abstand[j];
        abstand[j]=dswap;
        iswap=found[i];
        found[i]=found[j];
        found[j]=iswap;
      }
    }
    if (abstand[i] > epsmin) {
      (*nfound)=i+1;
      *enough=1;
      maxeps=abstand[i];

      return maxeps;
    }
  }

  maxeps=abstand[imax-2];

  return maxeps;
}

void make_dynamics(const double **series, octave_idx_type **box,
                   octave_idx_type **indexes, double &epsmin, bool epsset,
                   double EPSSTEP, octave_idx_type EMBED,
                   octave_idx_type MINNEIGHBORS, octave_idx_type LENGTH,
                   octave_idx_type DIMENSION,
                   octave_idx_type count, double &avneig, double &aveps,
                   double **dynamics, double *averr, octave_idx_type act)
{

  OCTAVE_LOCAL_BUFFER (unsigned long, found, LENGTH);

  OCTAVE_LOCAL_BUFFER (const double*, hser, DIMENSION);
  for (octave_idx_type i=0;i<DIMENSION;i++)
    hser[i]=series[i]+act;

  octave_idx_type alldim = DIMENSION * EMBED;

  double epsilon=epsmin/EPSSTEP;
  double foundeps = 0.0;
  octave_idx_type nfound=0;
  do {
    epsilon *= EPSSTEP;
    if (epsilon > EPSMAX)
      epsilon=EPSMAX;
    OCTAVE_LOCAL_BUFFER (long, list, LENGTH);
    make_multi_box(series,box,list,LENGTH-DELAY,BOX,DIMENSION,EMBED,
                   DELAY,epsilon);
    nfound=find_multi_neighbors(series,box,list,hser,BOX,
                                DIMENSION,EMBED,DELAY,epsilon,found);
    if (nfound > MINNEIGHBORS) {
      bool got_enough;
      foundeps=sort(series, found, indexes, epsmin, epsset, alldim,
                    MINNEIGHBORS, LENGTH, act, &nfound, &got_enough);
      if (got_enough)
        break;
    }
  } while (epsilon < EPSMAX);


  avneig += nfound;
  aveps += foundeps;
  if (!epsset)
    epsmin=aveps/count;

  // If less neighbors found than the minimum number
  // No sense continuing
  if (nfound < MINNEIGHBORS)
    {
      error_with_id ("Octave:tisean","Not enough neighbors found");
      return ;
    }

  // *_arr signifies pointer to data in *
  // This is done for optimization
  Matrix vec (alldim + 1,1);
  vec.fill (0.0);
  double *vec_arr = vec.fortran_vec ();

  Matrix mat (alldim+1, alldim+1);
  mat.fill (0.0);
  OCTAVE_LOCAL_BUFFER (double *, mat_arr, alldim + 1);
  for (octave_idx_type i = 0; i < alldim + 1; i++)
    mat_arr[i] = mat.fortran_vec () + (alldim+1) * i;
  
  for (octave_idx_type i=0;i<nfound;i++) {
    octave_idx_type act=found[i];
    mat_arr[0][0] += 1.0;
    for (octave_idx_type j=0;j<alldim;j++)
      mat_arr[0][j+1] += series[indexes[0][j]][act-indexes[1][j]];
    for (octave_idx_type j=0;j<alldim;j++) {
      double hv1=series[indexes[0][j]][act-indexes[1][j]];
      octave_idx_type hj=j+1;
      for (octave_idx_type k=j;k<alldim;k++)
        mat_arr[hj][k+1] += series[indexes[0][k]][act-indexes[1][k]]*hv1;
    }
  }

  for (octave_idx_type i=0;i<=alldim;i++)
    for (octave_idx_type j=i;j<=alldim;j++)
      mat_arr[j][i]=(mat_arr[i][j]/=(double)nfound);

  for (octave_idx_type d=0;d<DIMENSION;d++)
    {
      for (octave_idx_type i=0;i<=alldim;i++)
        vec_arr[i]=0.0;
      for (octave_idx_type i=0;i<nfound;i++) {
        octave_idx_type act=found[i];
        double hv=series[d][act+DELAY];
        vec_arr[0] += hv;
        for (octave_idx_type j=0;j<alldim;j++)
          vec_arr[j+1] += hv*series[indexes[0][j]][act-indexes[1][j]];
      }
      for (octave_idx_type i=0;i<=alldim;i++)
        vec_arr[i] /= (double)nfound;

      Matrix solved      = mat.solve (vec);
      double *solved_arr = solved.fortran_vec ();

    // If errors were raised (a singular matrix was encountered), 
    // there is no sense in countinuing
    if (error_state)
      {
        return ;
      }

      double new_vec = solved_arr[0];
      for (octave_idx_type i=1;i<=alldim;i++)
        dynamics[d][i-1] = solved_arr[i];

      for (octave_idx_type i=0;i<alldim;i++)
        new_vec += dynamics[d][i]*series[indexes[0][i]][act-indexes[1][i]];
      averr[d]  += (new_vec-series[d][act+DELAY])
                   *(new_vec-series[d][act+DELAY]);
    }

}

void gram_schmidt(octave_idx_type alldim, double **delta,
                  double *stretch)
{

  OCTAVE_LOCAL_BUFFER (double, diff, alldim);
  OCTAVE_LOCAL_BUFFER (double *, dnew, alldim);
  OCTAVE_LOCAL_BUFFER (double, dnew_data, alldim * alldim);
  for (octave_idx_type i=0;i<alldim;i++)
    dnew[i]=dnew_data + alldim * i;

  for (octave_idx_type i=0;i<alldim;i++) {
    for (octave_idx_type j=0;j<alldim;j++) 
      diff[j]=0.0;
    for (octave_idx_type j=0;j<i;j++) {
      double norm=0.0;
      for (octave_idx_type k=0;k<alldim;k++)
        norm += delta[i][k]*dnew[j][k];
      for (octave_idx_type k=0;k<alldim;k++)
        diff[k] -= norm*dnew[j][k];
    }
    double norm=0.0;
    for (octave_idx_type j=0;j<alldim;j++)
      norm += sqr(delta[i][j]+diff[j]);
    stretch[i]=(norm=sqrt(norm));
    for (octave_idx_type j=0;j<alldim;j++)
      dnew[i][j]=(delta[i][j]+diff[j])/norm;
  }
  for (octave_idx_type i=0;i<alldim;i++)
    for (octave_idx_type j=0;j<alldim;j++)
      delta[i][j]=dnew[i][j];

}

void make_iteration(octave_idx_type DIMENSION, octave_idx_type alldim,
                    double **dynamics, double **delta)
{

  OCTAVE_LOCAL_BUFFER (double *, dnew, alldim);
  OCTAVE_LOCAL_BUFFER (double, dnew_data, alldim * alldim);
  for (octave_idx_type i=0;i<alldim;i++)
    dnew[i]=dnew_data + alldim * i;

  for (octave_idx_type i=0;i<alldim;i++) {
    for (octave_idx_type j=0;j<DIMENSION;j++) {
      dnew[i][j]=dynamics[j][0]*delta[i][0];
      for (octave_idx_type k=1;k<alldim;k++)
        dnew[i][j] += dynamics[j][k]*delta[i][k];
    }
    for (octave_idx_type j=DIMENSION;j<alldim;j++)
      dnew[i][j]=delta[i][j-1];
  }

  for (octave_idx_type i=0;i<alldim;i++)
    for (octave_idx_type j=0;j<alldim;j++)
      delta[i][j]=dnew[i][j];

}

DEFUN_DLD (__lyap_spec__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if ((nargin != 8 && nargin != 13) || nargout != 2)
  {
    print_usage ();
  }
  else
    {

      // Assign input
      Matrix input                 = args(0).matrix_value ();
      octave_idx_type EMBED        = args(1).idx_type_value ();
      octave_idx_type ITERATIONS   = args(2).idx_type_value ();
      double epsmin                = args(3).double_value ();
      bool epsset                  = args(4).bool_value ();
      double EPSSTEP               = args(5).double_value ();
      octave_idx_type MINNEIGHBORS = args(6).idx_type_value ();
      octave_idx_type it_pause     = args(7).idx_type_value ();

      octave_idx_type count = 0;
      if (nargin == 13)
        {
          count        = args(8).idx_type_value ();
          if (count > (ITERATIONS - (EMBED-1)*DELAY))
            error_with_id ("Octave:tisean", "The variable 'count' grew too "
                                            "large");
          if (count < 0)
            error_with_id ("Octave:tisean", "The variable 'count' is too "
                                            "small");
        }

      octave_idx_type LENGTH    = input.rows ();
      octave_idx_type DIMENSION = input.columns ();
      octave_idx_type alldim    = DIMENSION*EMBED;

      // The following Matrix need to passed when
      // performing an interrupted execution
      Matrix averr_matrix;
      if (count == 0)
        averr_matrix.resize (DIMENSION,1);
      else if (nargin == 13)
        averr_matrix = args(9).matrix_value ();
      else
        error_with_id ("Octave:tisean", "Wrong number of arguments (%d) "
                                        "for current count = %lu",
                       nargin, (unsigned long)count);
      double *averr = averr_matrix.fortran_vec ();

      Matrix delta_matrix;
      if (count == 0)
        delta_matrix.resize (alldim, alldim);
      else if (nargin == 13)
        delta_matrix = args(10).matrix_value ();
      else
        error_with_id ("Octave:tisean", "Wrong number of arguments (%d) "
                                        "for current count = %lu",
                       nargin, (unsigned long)count);
      OCTAVE_LOCAL_BUFFER (double *, delta, alldim);
      for (octave_idx_type i=0;i<alldim;i++)
        delta[i] = delta_matrix.fortran_vec () + alldim * i;

      double avneig;
      if (nargin == 13)
        avneig = args(11).double_value ();
      double aveps;
      if (nargin == 13)
        aveps  = args(12).double_value ();

      // Analyze and rescale input
      OCTAVE_LOCAL_BUFFER (double, interval, DIMENSION);
      OCTAVE_LOCAL_BUFFER (double, var, DIMENSION);
      double maxinterval=0.0;
      for (octave_idx_type i=0;i<DIMENSION;i++) {
        double min_val;
        rescale_data(input,i,LENGTH,&min_val,&interval[i]);
        if (interval[i] > maxinterval) 
          maxinterval=interval[i];
        double av;
        variance(input.column(i),LENGTH,&av,&var[i]);
      }
      
      // Series is a pointer to data stored in input
      // so input(i,j) == series[j][i]
      // This is done for optimization purposes
      OCTAVE_LOCAL_BUFFER (const double *, series, DIMENSION);
      for (octave_idx_type j = 0; j < DIMENSION; j++)
        {
          const double *ptr = input.fortran_vec ();
          series[j] = ptr + LENGTH * j;
        }

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, BOX);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, box_data, BOX * BOX);
      for (octave_idx_type i=0;i<BOX;i++)
        box[i]=box_data + BOX * i;

      OCTAVE_LOCAL_BUFFER (double, factor, alldim);
      OCTAVE_LOCAL_BUFFER (double, lfactor, alldim);

      OCTAVE_LOCAL_BUFFER (double *, dynamics, DIMENSION);
      OCTAVE_LOCAL_BUFFER (double, dynamics_data, DIMENSION * alldim);
      for (octave_idx_type i=0;i<DIMENSION;i++)
        dynamics[i]=dynamics_data + alldim * i;

    // old  indexes=make_multi_index(dim,embed,DELAY);
      OCTAVE_LOCAL_BUFFER (octave_idx_type *, indexes, 2);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, indexes_data, 2*DIMENSION * EMBED);
      indexes[0] = indexes_data;
      indexes[1] = indexes_data + (DIMENSION * EMBED);

      for (octave_idx_type i=0;i<DIMENSION * EMBED;i++) 
        {
          indexes[0][i]=i%DIMENSION;
          indexes[1][i]=(i/DIMENSION)*DELAY;
        }
     // end old indexes = make_multi_index();

      if (!error_state)
        {

          // Promote warnings connected with singular matrixes to errors
          set_warning_state ("Octave:nearly-singular-matrix","error");
          set_warning_state ("Octave:singular-matrix","error");

          // Prepare data for running first time
          if (count == 0)
            {
              averr_matrix.fill (0.0);

              if (epsset)
                epsmin /= maxinterval;

              // old rnd_init(0x098342L);
              TISEAN_rand generator (0x098342L);
              for (octave_idx_type i=0;i<10000;i++)
                generator.rnd_long();
              for (octave_idx_type i=0;i<alldim;i++) {
                factor[i]=0.0;
                for (octave_idx_type j=0;j<alldim;j++)
                  delta[i][j] = (double)generator.rnd_long()
                                / (double)std::numeric_limits<octave_idx_type>
                                          ::max ();
              }
              gram_schmidt(alldim, delta,lfactor);

              avneig = 0.0;
              aveps  = 0.0;
            }

          // Create output
          Matrix lyap_exp (1, 1 + alldim);
          time_t lasttime;
          time(&lasttime);
          bool pause_calc = false;
          for (octave_idx_type i = count + (EMBED-1) * DELAY;
               i < ITERATIONS && !pause_calc; i++)
            {
              count++;
              make_dynamics(series, box, indexes, epsmin, epsset, EPSSTEP,
                            EMBED, MINNEIGHBORS, LENGTH, DIMENSION, count,
                            avneig, aveps, dynamics, averr, i);
              // If there was an error
              // (matrix singularity or not enough neighbors)
              // No sense continuing
              if (error_state)
                {
                  return retval;
                }
              make_iteration(DIMENSION, alldim, dynamics, delta);
              gram_schmidt(alldim, delta,lfactor);
              for (octave_idx_type j=0;j<alldim;j++) {
                factor[j] += log(lfactor[j])/(double)DELAY;
              }

              if (((time(NULL)-lasttime) > OUT) || (i == (ITERATIONS-1))
                  || (count % it_pause == 0) )
                {
                  time(&lasttime);

                  // Create spectrum output
                  // old fprintf(stdout,"%ld ",count);
                  lyap_exp(0,0) = count;
                  for (octave_idx_type j=0;j<alldim;j++)
                    {
                      // old fprintf(stdout,"%e ",factor[j]/count);
                      lyap_exp(0, 1+j) = factor[j]/count;
                    }

                  pause_calc = true;
                }
            }

          // Create pause output
          if (count < (ITERATIONS - (EMBED-1)*DELAY))
            {
              octave_scalar_map pause_vars;
              pause_vars.setfield ("averr", averr_matrix);
              pause_vars.setfield ("delta", delta_matrix);
              pause_vars.setfield ("count", count);
              pause_vars.setfield ("avneig", avneig);
              pause_vars.setfield ("aveps", aveps);
              pause_vars.setfield ("epsmin", epsmin);
              retval(0) = lyap_exp; 
              retval(1) = pause_vars;
            }

          // Create final output
          if (count == (ITERATIONS - (EMBED-1)*DELAY))
            {
              double dim=0.0;
              octave_idx_type i;
              for (i=0;i<alldim;i++) {
                dim += factor[i];
                if (dim < 0.0)
                  break;
              }
              if (i < alldim)
                dim=i+(dim-factor[i])/fabs(factor[i]);
              else
                dim=alldim;

              // Create output pars
              octave_scalar_map pars;

              // Create rel_err
              Matrix rel_err (1, DIMENSION);
              // old fprintf(stdout,"#Average relative forecast errors:= ");
              for (octave_idx_type i=0;i<DIMENSION;i++)
                {
                  // old fprintf(stdout,"%e ",sqrt(averr[i]/count)/var[i]);
                  rel_err(0,i) = sqrt(averr[i]/count)/var[i];
                }
              pars.setfield ("rel_err", rel_err);

              // Create abs_err
              Matrix abs_err (1, DIMENSION);
              // old fprintf(stdout,"#Average absolute forecast errors:= ");
              for (octave_idx_type i=0;i<DIMENSION;i++)
                {
                  // old fprintf(stdout,"%e ",sqrt(averr[i]/count)
                  //                          *interval[i]);
                  abs_err(0,i) = sqrt(averr[i]/count)*interval[i];
                }
              pars.setfield ("abs_err", abs_err);

              // old fprintf(stdout,"#Average Neighborhood Size= %e\n",
              //             aveps*maxinterval/count);
              pars.setfield ("nsize", aveps*maxinterval/count);

              // old fprintf(stdout,"#Average num. of neighbors= %e\n",
              //             avneig/count);
              pars.setfield ("nno", avneig/count);

              // old fprintf(stdout,"#estimated KY-Dimension= %f\n",dim);
              pars.setfield ("ky_dim", dim);

              // Assign output
              retval(0) = lyap_exp;
              retval(1) = pars;
            }
        }
    }
  return retval;
}
