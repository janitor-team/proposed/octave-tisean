/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber,
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lzo-gm of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <cmath>
#include "routines_c/tsa.h"

/*number of boxes for the neighbor search algorithm*/
#define NMAX 256

void make_fit(const Matrix& series, octave_idx_type dim,
              octave_idx_type act, octave_idx_type number,
              octave_idx_type STEP, octave_idx_type *found,
              double *error_array)
{
  octave_idx_type LENGTH = series.rows ();
  for (octave_idx_type i=0;i<dim;i++) 
    {
      const double *si = series.fortran_vec() + LENGTH * i;
      double cast=si[found[0]+STEP];
      for (octave_idx_type j=1;j<number;j++)
        cast += si[found[j]+STEP];
      cast /= (double)number;
      error_array[i] += sqr(cast-series(act+STEP,i));
    }
}

DEFUN_DLD (__lzo_gm__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 11)
  {
    print_usage ();
  }
  else
    {

      // Assign input
      Matrix input            = args(0).matrix_value ();
      octave_idx_type embed   = args(1).idx_type_value ();
      octave_idx_type delay   = args(2).idx_type_value ();
      octave_idx_type CLENGTH = args(3).idx_type_value ();
      double EPS0             = args(4).double_value ();
      bool eps0set            = args(5).bool_value ();
      double EPS1             = args(6).double_value ();
      bool eps1set            = args(7).bool_value ();
      double EPSF             = args(8).double_value ();
      octave_idx_type STEP    = args(9).idx_type_value ();
      unsigned long causal    = args(10).ulong_value ();

      octave_idx_type LENGTH = input.rows ();
      octave_idx_type dim    = input.columns ();

      octave_idx_type clength = (CLENGTH <= LENGTH) 
                                ? CLENGTH-STEP : LENGTH-STEP;

      // Analyze data and rescale input
      double interval,min;
      double maxinterval = 0.0;
      for (octave_idx_type i=0;i<dim;i++) 
        {
          rescale_data(input,i,LENGTH,&min,&interval);
          if (interval > maxinterval)
            maxinterval=interval;
        }
      interval=maxinterval;

      if (eps0set)
        EPS0 /= interval;
      if (eps1set)
        EPS1 /= interval; 

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (long, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);
      OCTAVE_LOCAL_BUFFER (unsigned long, hfound, LENGTH);
      OCTAVE_LOCAL_BUFFER (double, error_array, dim);
      OCTAVE_LOCAL_BUFFER (double, hrms, dim);
      OCTAVE_LOCAL_BUFFER (double, hav, dim);
      OCTAVE_LOCAL_BUFFER (double *, hser, dim);

      MArray<octave_idx_type> box (dim_vector(NMAX,NMAX));

      if ( ! error_state)
        {
          // Estimate maximum possible output size
          octave_idx_type output_rows = (octave_idx_type)
                                        ((log(EPS1) - log(EPS0)) / log (EPSF));
          output_rows += 2;

          // Create output
          Matrix output (output_rows,dim+4);
          octave_idx_type count = 0;

          for (double epsilon=EPS0;epsilon<EPS1*EPSF;epsilon*=EPSF) 
            {
              long pfound=0;
              for (octave_idx_type i=0;i<dim;i++)
                error_array[i]=hrms[i]=hav[i]=0.0;
              double avfound=0.0;

              make_multi_box(input,box,list,LENGTH-STEP,NMAX,dim,
                             embed,delay,epsilon);
              for (octave_idx_type i=(embed-1)*delay;i<clength;i++) 
                {
                  for (octave_idx_type j=0;j<dim;j++)
                    {
                      // old hser[j]=series[j]+i;
                      hser[j] = input.fortran_vec() + j * LENGTH + i;
                    }
                  octave_idx_type actfound;
                  actfound = find_multi_neighbors(input,box,list,hser,
                                                  NMAX,dim,embed,delay,
                                                  epsilon,hfound);
                  actfound = exclude_interval(actfound,i-causal+1,
                                             i+causal+(embed-1)*delay-1,
                                             hfound,found);
                  if (actfound > 2*(dim*embed+1)) 
                    {
                      make_fit (input, dim, i, actfound, STEP, found,
                               error_array);
                      pfound++;
                      avfound += (double)(actfound-1);
                      for (octave_idx_type j=0;j<dim;j++) {
                        hrms[j] += input(i+STEP,j) * input(i+STEP,j);
                        hav[j] += input(i+STEP,j);
                      }
                    }
                }
              if (pfound > 1) 
                {
                  double sumerror=0.0;
                  for (octave_idx_type j=0;j<dim;j++) 
                    {
                      hav[j] /= pfound;
                      hrms[j]=sqrt(fabs(hrms[j]/(pfound-1)-hav[j]*hav[j]
                                   * pfound/(pfound-1)));
                      error_array[j]=sqrt(error_array[j]/pfound)/hrms[j];
                      sumerror += error_array[j];
                    }

                  // Write output
        // old fprintf(stdout,"%e %e ",epsilon*interval,sumerror/(double)dim);
                  output(count, 0) = epsilon * interval;
                  output(count, 1) = sumerror / (double) dim;
                for (octave_idx_type j=0;j<dim;j++)
                  {
                    // old fprintf(stdout,"%e ",error_array[j]);
                    output(count, 2 + j) = error_array[j];
                  }
            
  //    old fprintf(stdout,"%e %e\n",(double)pfound/(clength-(embed-1)*delay),
  //                                avfound/pfound);
                output(count,2 + dim) = (double)pfound / 
                                        (clength-(embed-1)*delay);
                output(count,2 + dim + 1) = avfound/pfound;

                count += 1;
                }
            }

          // Resize output to fit actual results instead of
          // an educated guess
          // if count == 0 then the output will be an 0x4+dim matrix
          output.resize (count, dim + 4);

          retval(0) = output;

        }
    }
  return retval;
}
