/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger.
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on mutual of TISEAN 3.0.1
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#define HELPTEXT "\
-*- texinfo -*-\n\
@deftypefn{Function File} {@var{delay} =} mutual (@var{X})\n\
@deftypefnx{Function File} {[@var{delay}, @var{mutual_inf}] =} mutual (@var{X})\n\
@deftypefnx{Function File} {[@var{delay}, @var{mutual_inf}, @var{mutual_inf_log}] =} mutual (@var{X})\n\
@deftypefnx{Function File} {@dots{} =} mutual (@var{X}, @var{bd})\n\
@deftypefnx{Function File} {@dots{} =} mutual (@var{X}, @var{bd}, @var{d})\n\
\n\
Estimates the time delayed mutual information of the data. \
It is the simplest possible realization. It uses a fixed mesh of boxes. \
No finite sample corrections are implemented so far.\n\
\n\
@strong {Inputs}\n\
\n\
@table @var\n\
@item X\n\
Must be realvector. If it is a row vector then the output will be row vectors as well.\n\
@item bd\n\
If all inputs are present (nargin == 3) then @var{bd} is equal to the number of boxes for the\
 partition. If @var{d} is omitted (nargin == 2) then if @var{bd} > 0 then it is still equal to the \
number of boxes for the partition, but if @var{bd} < 0 then it is equal to the maximum time delay \
(@var{D}) and the number of boxes is its default value. @var{bd} cannot be equal 0 [default = 16].\n\
@item d\n\
The maximum time delay [default = 20].\n\
@end table\n\
\n\
@strong {Output}\n\
\n\
@table @var\n\
@item delay\n\
The delay.\n\
@item mutual_inf\n\
The calculated mutual information for the input vector @var{X}.\n\
@end table\n\
\n\
@strong{Algorithm}@*\n\
Based on TISEAN package mutual\n\
@end deftypefn"
/******************************************************************************/
/******************************************************************************/
#include <octave/oct.h>

#include "routines_c/tsa.h"

#define DEFAULT_NO_BOXES 16
#define DEFAULT_MAX_DELAY 20


double make_cond_entropy(octave_idx_type t, int32NDArray &h1,
                         int32NDArray &h11, int32NDArray &h2,
                         const long *array, octave_idx_type length,
                         octave_idx_type partitions)
{
  long hi,hii,count=0;
  double hpi,hpj,pij,cond_ent=0.0,norm;

  for (octave_idx_type i=0;i<partitions;i++) 
    {
      h1(i)  = 0;
      h11(i) = 0;
      for (octave_idx_type j=0;j<partitions;j++)
        h2(i,j)=0;
    }

  for (octave_idx_type i=0;i<length;i++)
    if (i >= t) {
      hii         = array[i];
      hi          = array[i-t];
      h1(hi)     += 1;
      h11(hii)   += 1;
      h2(hi,hii) += 1;
      count++;
    }

  norm=1.0/(double)count;
  cond_ent=0.0;

  for (octave_idx_type i=0;i<partitions;i++) {
    hpi=(double)(h1(i))*norm;
    if (hpi > 0.0) {
      for (octave_idx_type j=0;j<partitions;j++) {
      hpj=(double)(h11(j))*norm;
      if (hpj > 0.0) {
        pij=(double)(h2(i,j))*norm;
        if (pij > 0.0)
          cond_ent += pij*log(pij/hpj/hpi);
      }
      }
    }
  }

  return cond_ent;
}

DEFUN_DLD (mutual, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if ((nargin > 3) || (nargin < 1))
    {
      print_usage();
    }
  else if (nargout > 2)
    {
      error_with_id ("Octave:invalid-fun-call", \
                     "Can only output 2 arguments");
    }
  else
    {
      // Load input along with default values
      Matrix input               = args(0).matrix_value();
      octave_idx_type partitions = DEFAULT_NO_BOXES;
      octave_idx_type corrlength = DEFAULT_MAX_DELAY;

      if (nargin == 2)
        {
          int bD = args(1).int_value();
          if (bD < 0)
            {
              corrlength = -bD;
            }
          else
            {
              partitions = bD;
            }
        }

      if (nargin == 3)
        {
          partitions = args(1).int_value();
          corrlength = args(2).int_value();
        }


      // Data validation
      octave_idx_type rows = input.rows();
      octave_idx_type cols = input.columns();

      if (((rows != 1) && (cols != 1)) || (cols == 0) || (rows == 0))
        error_with_id ("Octave:invalid-input-arg", \
                       "input X must be a vector");

      if ((partitions < 1) || (corrlength < 1))
        error_with_id ("Octave:invalid-input-arg", \
                       "Neither the number of boxes, "
                       "nor the maximal delay time can be non-positive.");


      // Correct for row vectors;
      bool transposed = 0;
      if ((rows == 1) && (cols > 1))
        {
          transposed = 1;
          input = input.transpose();
        }

      // Dim == 1 always as we only use single column vectors.
      octave_idx_type dim    = input.columns();
      octave_idx_type length = input.rows();

      // Allocate memory
      int32NDArray h1 (dim_vector(partitions, 1));
      int32NDArray h11 (dim_vector(partitions, 1));
      int32NDArray h2 (dim_vector(partitions, partitions));
      OCTAVE_LOCAL_BUFFER (long, array, length);

      if (! error_state)
        {
          // Load array

          // Rescale data and load array
          // NOTE: currently supports only vectors so (dim == 1) always
          if (dim == 1){
          double mint, interval;
          rescale_data(input,0,length,&mint,&interval);

          for (octave_idx_type i=0;i<length;i++)
            if (input(i,0) < 1.0)
              array[i]=(long)(input(i,0)*(double)partitions);
            else
              array[i]=partitions-1;
          }

          double shannon = make_cond_entropy (0, h1, h11, h2, array, length,
                                              partitions);
          if (corrlength >= (long)length)
            corrlength=length-1;

          // Construct the output
          Matrix delay (corrlength+1,1);
          // To save memory
          int minf_len = 1;

          if (nargout > 1)
            minf_len = corrlength+1;
          Matrix mutual_inf (minf_len,1);

          // Assign output
          delay(0,0)      = 0;
          mutual_inf(0,0) = shannon;
          for (octave_idx_type tau=1;tau<=corrlength;tau++) {

  //          fprintf(stdout,"%ld %e %e\n",tau,condent,condent/log((double)partitions));
            delay(tau,0) = tau;
            if (nargout > 1)
              {
                mutual_inf(tau,0) = make_cond_entropy(tau, h1, h11, h2, array,
                                                      length, partitions);
              }
          }

          if (transposed)
            {
              delay          = delay.transpose();
              if (nargout > 1)
                mutual_inf     = mutual_inf.transpose();
            }
          retval(0) = delay;
          retval(1) = mutual_inf;
        }
    }
  return retval;
}

/*
%!fail("mutual((1:10),0)");

%!fail("mutual((1:10),-4,5)");

%!fail("mutual([(1:10);(1:10)])");

%!fail("[a,b,c,d] = mutual ((1:10),1)");

%!test
%! "'res' was created using 'mutual hen.dat' where from the TISEAN package.";
%! "'hen.dat' was created using 'henon(10000)' from Tisean package in GNU Octave";
%! res = [0, 2.684467e+00; 1, 1.362672e+00; 2, 1.054827e+00; 3, 8.300913e-01; 4, 6.421677e-01; 5, 4.650269e-01; 6, 3.409740e-01; 7, 2.485991e-01; 8, 1.621105e-01; 9, 1.144275e-01; 10, 9.430688e-02; 11, 6.796321e-02; 12, 6.181312e-02; 13, 4.369772e-02; 14, 3.566099e-02; 15, 2.923612e-02; 16, 2.354500e-02; 17, 1.924072e-02; 18, 1.999171e-02; 19, 1.887493e-02; 20, 1.464115e-02];
%! hen = henon(10000);
%! [a,b] = mutual (hen(:,1),-20);
%! assert ([a,b], res, 1e-6);
*/
