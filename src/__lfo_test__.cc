/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lfo-test of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"


/*number of boxes for the neighbor search algorithm*/
#define NMAX 512

void put_in_boxes(const double **series, octave_idx_type LENGTH,
                  octave_idx_type STEP, octave_idx_type COMP,
                  octave_idx_type hdim, double epsilon,
                  octave_idx_type *list, octave_idx_type **box)
{

  double epsinv=1.0/epsilon;
  for (octave_idx_type i=0;i<NMAX;i++)
    for (octave_idx_type j=0;j<NMAX;j++)
      box[i][j]= -1;

  for (octave_idx_type n=hdim;n<LENGTH-STEP;n++) {
    octave_idx_type i=(octave_idx_type)(series[0][n]*epsinv)&(NMAX-1);
    octave_idx_type j=(octave_idx_type)(series[COMP-1][n-hdim]*epsinv)
                                       &(NMAX-1);
    list[n]=box[i][j];
    box[i][j]=n;
  }
}

octave_idx_type hfind_neighbors(const double **series,
                             octave_idx_type **indexes, octave_idx_type COMP,
                             octave_idx_type hdim, octave_idx_type DIM,
                             double epsilon, unsigned long *hfound,
                             octave_idx_type *list, octave_idx_type **box,
                             octave_idx_type act)
{

  octave_idx_type nfound=0;
  double epsinv=1.0/epsilon;

  octave_idx_type i=(octave_idx_type)(series[0][act]*epsinv)&(NMAX-1);
  octave_idx_type j=(octave_idx_type)(series[COMP-1][act-hdim]*epsinv)
                                      &(NMAX-1);
  
  for (octave_idx_type i1=i-1;i1<=i+1;i1++) {
    octave_idx_type i2=i1&(NMAX-1);
    for (octave_idx_type j1=j-1;j1<=j+1;j1++) {
      octave_idx_type element=box[i2][j1&(NMAX-1)];
      while (element != -1) {
        double max=0.0;
        bool toolarge=0;
        for (octave_idx_type k=0;k<DIM;k += 1) {
          octave_idx_type hcomp=indexes[0][k];
          octave_idx_type hdel=indexes[1][k];
          double dx=fabs(series[hcomp][element-hdel]-series[hcomp][act-hdel]);
          max=(dx>max) ? dx : max;
          if (max > epsilon) {
            toolarge=1;
            break;
          }
          if (toolarge)
            break;
        }
        if (max <= epsilon)
          hfound[nfound++]=element;
        element=list[element];
      }
    }
  }
  return nfound;
}

void multiply_matrix(double **mat,double *vec, octave_idx_type DIM)
{

  OCTAVE_LOCAL_BUFFER (double, hvec, DIM);

  for (octave_idx_type i=0;i<DIM;i++) {
    hvec[i]=0.0;
    for (octave_idx_type j=0;j<DIM;j++)
      hvec[i] += mat[i][j]*vec[j];
  }
  for (octave_idx_type i=0;i<DIM;i++)
    vec[i]=hvec[i];

}

void make_fit(const double **series, octave_idx_type **indexes,
              const octave_idx_type* found, octave_idx_type STEP,
              octave_idx_type DIM, octave_idx_type COMP,
              octave_idx_type number,
              unsigned long act,double *newcast)
{

  OCTAVE_LOCAL_BUFFER (double, localav, DIM);
  OCTAVE_LOCAL_BUFFER (double, foreav, COMP);

  for (octave_idx_type i=0;i<DIM;i++)
    localav[i]=0.0;
  for (octave_idx_type i=0;i<COMP;i++)
    foreav[i]=0.0;

  for (octave_idx_type n=0;n<number;n++) {
    octave_idx_type which=found[n];
    for (octave_idx_type j=0;j<COMP;j++)
      foreav[j] += series[j][which+STEP];
    for (octave_idx_type j=0;j<DIM;j++) {
      octave_idx_type hcj=indexes[0][j];
      octave_idx_type hdj=indexes[1][j];
      localav[j] += series[hcj][which-hdj];
    }
  }

  for (octave_idx_type i=0;i<DIM;i++)
    localav[i] /= number;
  for (octave_idx_type i=0;i<COMP;i++)
    foreav[i] /= number;

  Matrix mat (DIM, DIM);
  // mat_arr points to the same data as mat
  // so mat(i,j) == mat_arr[j][i]
  OCTAVE_LOCAL_BUFFER (double *, mat_arr, DIM);
  for (octave_idx_type j = 0; j < DIM; j++)
    {
      double *ptr = mat.fortran_vec ();
      mat_arr[j] = ptr + DIM * j;
    }

  for (octave_idx_type i=0;i<DIM;i++) {
    octave_idx_type hci=indexes[0][i];
    octave_idx_type hdi=indexes[1][i];
    double lavi=localav[i];
    const double *si=series[hci];
    for (octave_idx_type j=i;j<DIM;j++) {
      octave_idx_type hcj=indexes[0][j];
      octave_idx_type hdj=indexes[1][j];
      double lavj=localav[j];
      const double *sj=series[hcj];
      mat_arr[i][j]=0.0;
      for (octave_idx_type n=0;n<number;n++) {
      octave_idx_type which=found[n];
      mat_arr[i][j] += (si[which-hdi]-lavi)*(sj[which-hdj]-lavj);
      }
      mat_arr[i][j] /= number;
      mat_arr[j][i] = mat_arr[i][j];
    }
  }


  Matrix vec (DIM,1);
  double *vec_arr = vec.fortran_vec ();

  for (octave_idx_type i=0;i<COMP;i++)
    {
      const double *si=series[i];
      double fav=foreav[i];
      for (octave_idx_type j=0;j<DIM;j++) {
        octave_idx_type hcj=indexes[0][j];
        octave_idx_type hdj=indexes[1][j];
        vec_arr[j]=0.0;
        const double *sj=series[hcj];
        for (octave_idx_type n=0;n<number;n++) {
        octave_idx_type which=found[n];
        vec_arr[j] += (si[which+STEP]-fav)*(sj[which-hdj]);
        }
        vec_arr[j] /= number;
      }

  // The commented version is faster, but does not account for 
  // singular or near singular matrixes. It is left here for reference.

  //  OCTAVE_LOCAL_BUFFER (double, imat_data, sqr(DIM));
  //  OCTAVE_LOCAL_BUFFER (double *, imat, DIM);
  //  for (octave_idx_type j=0;j<DIM;j++)
  //    {
  //      imat[j] = imat_data + DIM *j;
  //    }
  //  invert_matrix(mat_arr,imat,DIM);
  //  multiply_matrix(imat,vec_arr,DIM);

// Below is version that uses Octave's Matrix::solve().
// It is slower, than generating an inverse matrix, but gives warnings 
// (which are treated as errors) when a near singular matrix is encountered.

      Matrix solved_vec      = mat.solve (vec);
      double *solved_vec_arr = solved_vec.fortran_vec ();

      // If errors were raised (a singular matrix was encountered), 
      // there is no sense in countinueing
      if (error_state)
      {
        return ;
      }

      newcast[i]=foreav[i];
      for (octave_idx_type j=0;j<DIM;j++) {
        octave_idx_type hcj=indexes[0][j];
        octave_idx_type hdj=indexes[1][j];
        newcast[i] += solved_vec_arr[j]*(series[hcj][act-hdj]-localav[j]);
      }
    }
  
}

DEFUN_DLD (__lfo_test__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 10 || nargout > 2)
  {
    print_usage ();
  }
  else
    {
      // Assign input
      Matrix input            = args(0).matrix_value ();
      octave_idx_type EMBED   = args(1).idx_type_value ();
      octave_idx_type DELAY   = args(2).idx_type_value ();
      octave_idx_type CLENGTH = args(3).idx_type_value ();
      octave_idx_type MINN    = args(4).idx_type_value ();
      double EPS0             = args(5).double_value ();
      bool epsset             = args(6).bool_value ();
      double EPSF             = args(7).double_value ();
      octave_idx_type STEP    = args(8).idx_type_value ();
      unsigned long causal    = args(9).ulong_value ();

      octave_idx_type LENGTH = input.rows ();
      octave_idx_type COMP   = input.columns ();

      // Assign help values
      octave_idx_type DIM=EMBED*COMP;
      octave_idx_type hdim=(EMBED-1)*DELAY;

      // Series is a pointer to data stored in input
      // so input(i,j) == series[j][i]
      // This is done for optimization purposes
      OCTAVE_LOCAL_BUFFER (const double *, series, COMP);
      for (octave_idx_type j = 0; j < COMP; j++)
        {
          const double *ptr = input.fortran_vec ();
          series[j] = ptr + LENGTH * j;
        }

      // Analyze input
      OCTAVE_LOCAL_BUFFER (double, min_array, COMP);
      OCTAVE_LOCAL_BUFFER (double, interval, COMP);
      OCTAVE_LOCAL_BUFFER (double, av, COMP);
      OCTAVE_LOCAL_BUFFER (double, rms, COMP);

      double maxinterval=0.0;
      for (octave_idx_type i=0;i<COMP;i++) {
        rescale_data(input,i,LENGTH,&min_array[i],&interval[i]);
        maxinterval=(maxinterval<interval[i])?interval[i]:maxinterval;
        variance(input.column (i),LENGTH,&av[i],&rms[i]);
      }

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (double *, individual, COMP);
      OCTAVE_LOCAL_BUFFER (double, individual_data, COMP * LENGTH);
      for (octave_idx_type j=0;j<COMP;j++) {
        individual[j]=individual_data + LENGTH * j;
        for (octave_idx_type i=0;i<LENGTH;i++)
          individual[j][i]=0.0;
      }

      OCTAVE_LOCAL_BUFFER (octave_idx_type, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);
      OCTAVE_LOCAL_BUFFER (unsigned long, hfound, LENGTH);
      OCTAVE_LOCAL_BUFFER (bool, done, LENGTH);

      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, NMAX);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, box_data, NMAX * NMAX);
      for (octave_idx_type j=0;j<NMAX;j++)
        box[j]=box_data + NMAX * j;
        
      for (octave_idx_type i=0;i<LENGTH;i++)
        done[i]=false;

      if (epsset)
        EPS0 /= maxinterval;

      double epsilon=EPS0/EPSF;
      octave_idx_type clength=(CLENGTH <= LENGTH) ? CLENGTH-STEP : LENGTH-STEP;

      // old indexes=make_multi_index(COMP,EMBED,DELAY);

      octave_idx_type alldim = COMP * EMBED;

      OCTAVE_LOCAL_BUFFER (octave_idx_type*, indexes, 2);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, indexes_data, alldim * 2);
      indexes[0] = indexes_data;
      indexes[1] = indexes_data + alldim;
      for (octave_idx_type i=0;i<alldim;i++) 
        {
          indexes[0][i]=i%COMP;
          indexes[1][i]=(i/COMP)*DELAY;
        }

       // end old index = make_multi_index();
      OCTAVE_LOCAL_BUFFER (double, newcast, COMP);

      OCTAVE_LOCAL_BUFFER (double, error_array, COMP);
      for (octave_idx_type i=0;i<COMP;i++)
        error_array[i]=0.0;

      if ( ! error_state)
        {

          // Promote warnings connected with singular matrixes to errors
          set_warning_state ("Octave:nearly-singular-matrix","error");
          set_warning_state ("Octave:singular-matrix","error");

          // Calculate the maximum epsilon that makes sense
          // On the basis of 'i' and 'j' from put_in_boxes ()
          NDArray input_max = input.max ();
          double maximum_epsilon = (input_max(0) > input_max(COMP-1))
                                   ? input_max(0) : input_max(COMP-1);
          maximum_epsilon *= EPSF;

          // Calculate output
          bool alldone=0;
          while (!alldone) {
            alldone=1;

            // If epsilon became too large there is no sense in continuing
            if (epsilon > maximum_epsilon)
              {
                error_with_id ("Octave:tisean", "The neighbourhood size became"
                               " too large during search, no sense"
                               " continuing");
                return retval;
              }
            epsilon*=EPSF;
            put_in_boxes(series, LENGTH, STEP, COMP, hdim, epsilon, list, box);
            for (octave_idx_type i=(EMBED-1)*DELAY;i<clength;i++)
              if (!done[i]) 
                {

                  octave_idx_type actfound;
                  actfound=hfind_neighbors(series, indexes, COMP, hdim, DIM,
                                           epsilon, hfound, list, box, i);
                  actfound=exclude_interval(actfound,i-causal+1,
                                      i+causal+(EMBED-1)*DELAY-1,hfound,found);
                  if (actfound > MINN) 
                    {
                      make_fit(series, indexes, found, STEP, DIM, COMP,
                               actfound,i,newcast);
                      // Checking if the fit was correct
                      // If any errors were raised: end function
                      if (error_state)
                        {
                          return retval;
                        }
                      for (octave_idx_type j=0;j<COMP;j++)
                        error_array[j] += sqr(newcast[j]-series[j][i+STEP]);

                      for (octave_idx_type j=0;j<COMP;j++)
                        individual[j][i]=(newcast[j]-series[j][i+STEP])
                                          *interval[j];
                      done[i]=1;
                    }
                  alldone &= done[i];
                }
          }

          double norm=((double)clength-(double)((EMBED-1)*DELAY));

          // Create relative forecast error output
          Matrix rel (COMP, 1);
          for (octave_idx_type i=0;i<COMP;i++) 
            {
             // old fprintf(stdout,"# %e\n",sqrt(error_array[i]/norm)/rms[i]);
              rel(i,0) = sqrt(error_array[i]/norm)/rms[i];
            }

          // Create individual forecast error output
          Matrix ind (clength - (EMBED-1)*DELAY,COMP);
          for (octave_idx_type i=(EMBED-1)*DELAY;i<clength;i++)
            {
              for (octave_idx_type j=0;j<COMP;j++)
                {
                 // old fprintf(stdout,"%e ",individual[j][i]);
                  ind(i-(EMBED-1)*DELAY, j) = individual[j][i];
                }
            }

          retval(0) = rel;
          retval(1) = ind;
        }
    }
  return retval;
}
