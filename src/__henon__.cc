/* Copyright (C) 1996-2015 Piotr Held
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */

/* Author: Piotr Held <pjheld@gmail.com>. 
 * This function is based on henon of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/
#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>

DEFUN_DLD (__henon__, args, nargout, HELPTEXT)
{
    int nargin = args.length ();
    octave_value_list retval;

    if (nargin != 6)
      {
        print_usage ();
      }
    else
      {
        double yn, xn;

        int nmax  = args(0).int_value();
        double a  = args(1).scalar_value();
        double b  = args(2).scalar_value();
        double x0 = args(3).scalar_value();
        double y0 = args(4).scalar_value();
        int ntran = args(5).int_value();

        Matrix ret (nmax,2);

        for (int i = 0; i < ntran; i++)
          {
            xn = 1 - a*x0*x0 + b*y0;
            yn = x0;
            x0 = xn;
            y0 = yn;
          }        

        for (int i = 0; i < nmax; i++)
          {
            xn = 1 - a*x0*x0 + b*y0;
            yn = x0;
            x0 = xn;
            y0 = yn;

            ret(i,0) = xn;
            ret(i,1) = yn;
          }

        retval(0) = ret;

      }

    return retval;
}
