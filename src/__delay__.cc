/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber,
 *                           Piotr Held, Juan Pablo Carbajal
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger.
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on delay of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"


DEFUN_DLD (__delay__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 5)
    {
      print_usage ();
    }
  else
    {
      // Load the data into appropriate variables
      Matrix data            = args(0).matrix_value ();
      octave_idx_type length = args(1).idx_type_value ();
      octave_idx_type indim  = args(2).idx_type_value ();
      Array<octave_idx_type> formatdelay  = args(3).array_value ();
      Array<octave_idx_type> delaylist = args(4).array_value ();


      octave_idx_type alldim = 0;
      for (octave_idx_type i = 0; i < indim; i++)
        alldim += formatdelay(i);

      OCTAVE_LOCAL_BUFFER (octave_idx_type, inddelay, alldim);

      if (! error_state)
        {
          octave_idx_type rundel=0;
          octave_idx_type runmdel=0;

          unsigned int delsum;
          for (octave_idx_type i = 0; i < indim; i++)
            {
              delsum             = 0;
              inddelay[rundel++] = delsum;

              for (octave_idx_type j = 1; j < formatdelay(i); j++)
                {
                  delsum            += delaylist(runmdel++);
                  inddelay[rundel++] = delsum;
                }
            }

          octave_idx_type maxemb = 0;
          for (octave_idx_type i = 0; i < alldim; i++)
            maxemb = (maxemb < inddelay[i])? inddelay[i] : maxemb;

          octave_idx_type outdim = 0;
          for (octave_idx_type i = 0; i < indim; i++)
             outdim += formatdelay(i);

          octave_idx_type out_rows = (length > maxemb) ? length - maxemb : 0;

          Matrix series (out_rows, outdim);
          unsigned int embsum;
          for (octave_idx_type i = maxemb; i < length; i++)
            {
              rundel = 0;
              embsum = 0;

              for (octave_idx_type j = 0; j < indim; j++)
                {
                  octave_idx_type emb = formatdelay(j);

                  for (octave_idx_type k = 0; k < emb; k++)
                    series(i-maxemb, embsum+k) = data(i-inddelay[rundel++], j);

                    // previously fprintf(stdout,"%e ",series[j][i-inddelay[rundel++]]);
                  embsum += emb;
                }
              // previously fprintf(stdout,"\n");
            }
          retval(0) = series;
        }

    }
  return retval;
}
