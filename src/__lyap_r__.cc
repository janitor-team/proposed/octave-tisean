/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lyap_r of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"

#define NMAX 256

void put_in_boxes(double *series, octave_idx_type **box, octave_idx_type *list,
                  double eps, octave_idx_type length, octave_idx_type dim,
                  octave_idx_type delay, octave_idx_type steps)
{

  double epsinv=1.0/eps;

  for (octave_idx_type i=0;i<NMAX;i++)
    for (octave_idx_type j=0;j<NMAX;j++)
      box[i][j]= -1;

  octave_idx_type del=delay*(dim-1);
  for (octave_idx_type i=0;i<length-del-steps;i++) {
    octave_idx_type x=(octave_idx_type)(series[i]*epsinv)&(NMAX-1);
    octave_idx_type y=(octave_idx_type)(series[i+del]*epsinv)&(NMAX-1);
    list[i]=box[x][y];
    box[x][y]=i;
  }
}

bool make_iterate(double *series, octave_idx_type **box, octave_idx_type *list,
                  long *found, double *lyap, double eps,
                  octave_idx_type length, octave_idx_type dim,
                  octave_idx_type delay, octave_idx_type steps,
                  octave_idx_type mindist, octave_idx_type act)
{
  bool ok=0;
  octave_idx_type minelement= -1;
  double mindx=1.0;
  double epsinv = 1.0/eps;

  octave_idx_type k;
  octave_idx_type x = (octave_idx_type)(series[act]*epsinv)&(NMAX-1);
  octave_idx_type y = (octave_idx_type)(series[act+delay*(dim-1)]*epsinv)
                      &(NMAX-1);
  for (octave_idx_type i=x-1;i<=x+1;i++) {
    octave_idx_type i1=i&(NMAX-1);
    for (octave_idx_type j=y-1;j<=y+1;j++) {
      octave_idx_type element=box[i1][j&(NMAX-1)];
      while (element != -1) {
         if (labs(act-element) > mindist) {
           double dx=0.0;
           for (k=0;k<dim*delay;k+=delay) {
             dx += (series[act+k]-series[element+k])*
               (series[act+k]-series[element+k]);
             if (dx > eps*eps)
               break;
           }
           if (k==dim*delay) {
             if (dx < mindx) {
               ok=1;
               if (dx > 0.0) {
                  mindx=dx;
                  minelement=element;
               }
             }
           }
         }
         element=list[element];
      }
    }
  }
  if ((minelement != -1) ) {
    act--;
    minelement--;
    for (octave_idx_type i=0;i<=steps;i++) {
      act++;
      minelement++;
      double dx=0.0;
      for (octave_idx_type j=0;j<dim*delay;j+=delay) {
         dx += (series[act+j]-series[minelement+j])*
           (series[act+j]-series[minelement+j]);
      }
      if (dx > 0.0) {
         found[i]++;
         lyap[i] += log(dx);
      }
    }
  }
  return ok;
}

DEFUN_DLD (__lyap_r__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 8)
  {
    print_usage ();
  }
  else
    {
      // Assign input
      NDArray input           = args(0).array_value ();
      octave_idx_type dim     = args(1).idx_type_value ();
      octave_idx_type delay   = args(2).idx_type_value ();
      octave_idx_type mindist = args(3).idx_type_value ();
      double eps0             = args(4).double_value ();
      bool epsset             = args(5).bool_value ();
      octave_idx_type steps   = args(6).idx_type_value ();
      bool verbose            = args(7).bool_value ();

      octave_idx_type length = input.numel ();
      double *series = input.fortran_vec ();

      // Reascale data and adjust input
      double min_val,max_val;
      rescale_data(input,length,&min_val,&max_val);

      if (epsset)
        eps0 /= max_val;

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, NMAX);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, box_data, NMAX * NMAX);
      for (octave_idx_type i=0;i<NMAX;i++)
        box[i]=box_data + NMAX * i;

      OCTAVE_LOCAL_BUFFER (octave_idx_type, list, length);
      OCTAVE_LOCAL_BUFFER (double, lyap, steps + 1);
      OCTAVE_LOCAL_BUFFER (long, found, steps+1);
      OCTAVE_LOCAL_BUFFER (bool, done, length);

      for (octave_idx_type i=0;i<=steps;i++) {
        lyap[i]=0.0;
        found[i]=0;
      }
      for (octave_idx_type i=0;i<length;i++)
        done[i]=0;

      octave_idx_type maxlength=length-delay*(dim-1)-steps-1-mindist;
      bool alldone=0;

      if (! error_state)
        {

          // Calculate the maximum epsilon that makes sense
          // On the basis of 'i' and 'j' from put_in_boxes ()
          NDArray input_max = input.max ();
          double maximum_epsilon = (input_max(0) > input_max(dim-1))
                                   ? input_max(0) : input_max(dim-1);
          maximum_epsilon *= 1.1;

          // Calculate lyapunov exponents
          for (double eps=eps0;!alldone;eps*=1.1)
            {

              // If epsilon became too large 
              // there is no sense in continuing
              if (eps > maximum_epsilon)
                {
                  error_with_id ("Octave:tisean", "The neighbourhood size"
                                 " became too large during search,"
                                 " no sense continuing");
                  return retval;
                }

              put_in_boxes(series, box, list, eps, length, dim, delay, steps);
              alldone=1;
              for (octave_idx_type n=0;n<=maxlength;n++)
                {
                  if (!done[n])
                     done[n]=make_iterate(series, box, list, found, lyap, eps,
                                          length, dim, delay, steps, mindist,
                                          n);
                  alldone &= done[n];
                }
              if (verbose)
                printf("epsilon: %e already found: %ld\n",eps*max_val,
                       found[0]);
            }

          // Create output
          Matrix output (steps+1, 2);
          octave_idx_type count = 0;
          for (octave_idx_type i=0;i<=steps;i++)
            if (found[i])
              {
                // old fprintf(file,"%d %e\n",i,lyap[i]/found[i]/2.0);
                output(count,0) = i;
                output(count,1) = lyap[i]/found[i]/2.0;
                count          += 1;
              }

          // Resize output to match number of found points
          if (count < output.numel ())
            output.resize (count, 2);

          // Assign output
          retval(0) = output;
        }
    }
  return retval;
}
