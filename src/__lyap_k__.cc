/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on lyap_k of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <octave/oct-map.h>
#include <octave/str-vec.h>
#include "routines_c/tsa.h"

#define BOX 128

void put_in_boxes(double *series, octave_idx_type *liste,
                  octave_idx_type **box, octave_idx_type length,
                  octave_idx_type maxdim, octave_idx_type delay,
                  octave_idx_type maxiter, double eps)
{

  octave_idx_type blength=length-(maxdim-1)*delay-maxiter;

  for (octave_idx_type i=0;i<BOX;i++)
    for (octave_idx_type j=0;j<BOX;j++)
      box[i][j]= -1;

  for (octave_idx_type i=0;i<blength;i++) {
    octave_idx_type j=(octave_idx_type)(series[i]/eps)&(BOX-1);
    octave_idx_type k=(octave_idx_type)(series[i+delay]/eps)&(BOX-1);
    liste[i]=box[j][k];
    box[j][k]=i;
  }
}

void lfind_neighbors(double *series, octave_idx_type **lfound,
                     octave_idx_type *found, octave_idx_type *liste,
                     octave_idx_type **box, octave_idx_type window,
                     octave_idx_type maxdim, octave_idx_type delay,
                     long act,double eps)
{

  for (octave_idx_type hi=0;hi<maxdim-1;hi++)
    found[hi]=0;
  octave_idx_type i=(octave_idx_type)(series[act]/eps)&(BOX-1);
  octave_idx_type j=(octave_idx_type)(series[act+delay]/eps)&(BOX-1);
  for (octave_idx_type i1=i-1;i1<=i+1;i1++) {
    octave_idx_type i2=i1&(BOX-1);
    for (octave_idx_type j1=j-1;j1<=j+1;j1++) {
      octave_idx_type element=box[i2][j1&(BOX-1)];
      while (element != -1) {
         if ((element < (act-window)) || (element > (act+window))) {
           double dx=sqr(series[act]-series[element]);
           if (dx <= eps*eps) {
             for (octave_idx_type k=1;k<maxdim;k++) {
               octave_idx_type k1=k*delay;
               dx += sqr(series[act+k1]-series[element+k1]);
               if (dx <= eps*eps) {
                  k1=k-1;
                  lfound[k1][found[k1]]=element;
                  found[k1]++;
               }
               else
                  break;
             }
           }
         }
         element=liste[element];
      }
    }
  }
}

void iterate_points(double *series, double **lyap, long **count,
                    octave_idx_type **lfound, octave_idx_type *found,
                    octave_idx_type maxdim, octave_idx_type mindim,
                    octave_idx_type delay, octave_idx_type maxiter,
                    long act)
{

  OCTAVE_LOCAL_BUFFER (double *, lfactor, maxdim - 1);
  OCTAVE_LOCAL_BUFFER (double ,lfactor_data, (maxdim-1) * (maxiter+1));
  OCTAVE_LOCAL_BUFFER (long *, lcount, maxdim - 1);
  OCTAVE_LOCAL_BUFFER (long, lcount_data, (maxdim-1) * (maxiter + 1));
  for (octave_idx_type i=0;i<maxdim-1;i++) {
    lfactor[i] = lfactor_data + (maxiter+1) * i;
    lcount[i]  = lcount_data + (maxiter+1) * i;
  }
  OCTAVE_LOCAL_BUFFER (double, dx, maxiter+1);

  for (octave_idx_type i=0;i<=maxiter;i++)
    for (octave_idx_type j=0;j<maxdim-1;j++) {
      lfactor[j][i]=0.0;
      lcount[j][i]=0;
    }
  
  for (octave_idx_type j=mindim-2;j<maxdim-1;j++) {
    for (octave_idx_type k=0;k<found[j];k++) {
      octave_idx_type element=lfound[j][k];
      for (octave_idx_type i=0;i<=maxiter;i++)
         dx[i]=sqr(series[act+i]-series[element+i]);
      for (octave_idx_type l=1;l<j+2;l++) {
         octave_idx_type l1=l*delay;
         for (octave_idx_type i=0;i<=maxiter;i++)
           dx[i] += sqr(series[act+i+l1]-series[element+l1+i]);
      }
      for (octave_idx_type i=0;i<=maxiter;i++)
         if (dx[i] > 0.0){
           lcount[j][i]++;
           lfactor[j][i] += dx[i];
         }
    }
  }
  for (octave_idx_type i=mindim-2;i<maxdim-1;i++)
    for (octave_idx_type j=0;j<=maxiter;j++)
      if (lcount[i][j]) {
         count[i][j]++;
         lyap[i][j] += log(lfactor[i][j]/lcount[i][j])/2.0;
      }
}

DEFUN_DLD (__lyap_k__, args, , HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 13)
  {
    print_usage ();
  }
  else
    {
      // Assign input
      NDArray input             = args(0).array_value ();
      octave_idx_type maxdim    = args(1).idx_type_value ();
      octave_idx_type mindim    = args(2).idx_type_value ();
      octave_idx_type delay     = args(3).idx_type_value ();
      double epsmin             = args(4).double_value ();
      bool eps0set              = args(5).bool_value ();
      double epsmax             = args(6).double_value ();
      bool eps1set              = args(7).bool_value ();
      octave_idx_type epscount  = args(8).idx_type_value ();
      octave_idx_type reference = args(9).idx_type_value ();
      octave_idx_type maxiter   = args(10).idx_type_value ();
      octave_idx_type window    = args(11).idx_type_value ();
      bool verbose              = args(12).bool_value ();

      octave_idx_type length = input.numel ();
      double *series = input.fortran_vec ();
      double min_val, max_val;

      // Analyze input and adjust
      rescale_data(input,length,&min_val,&max_val);

      if (eps0set)
        epsmin /= max_val;
      if (eps1set)
        epsmax /= max_val;

      // Allocate memory

      OCTAVE_LOCAL_BUFFER (octave_idx_type, liste, length);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, maxdim-1);

      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, BOX);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, box_data, BOX * BOX);
      for (octave_idx_type i=0; i<BOX; i++)
        box[i] = box_data + BOX * i;

      OCTAVE_LOCAL_BUFFER (octave_idx_type *, lfound, maxdim -1);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, lfound_data, (maxdim-1) * length);
      for (octave_idx_type i=0;i<maxdim-1;i++)
        lfound[i]=lfound_data + length * i;

      OCTAVE_LOCAL_BUFFER (long *, count, maxdim - 1);
      OCTAVE_LOCAL_BUFFER (long, count_data, (maxdim-1) * (maxiter+1));
      for (octave_idx_type i=0;i<maxdim-1;i++)
        count[i]=count_data + (maxiter+1) * i;

      OCTAVE_LOCAL_BUFFER (double *, lyap, maxdim -1);
      OCTAVE_LOCAL_BUFFER (double, lyap_data, (maxdim-1) * (maxiter + 1));
      for (octave_idx_type i=0;i<maxdim-1;i++)
        lyap[i]=lyap_data + (maxiter+1) * i;

      double eps_fak;
      if (epscount == 1)
        eps_fak=1.0;
      else
        eps_fak=pow(epsmax/epsmin,1.0/(double)(epscount-1));


      if (! error_state)
        {
          // Calculate exponents
          dim_vector dv (epscount ,((int)maxdim-(int)mindim + 1));
          string_vector keys;
          keys.append (std::string("eps"));
          keys.append (std::string("dim"));
          keys.append (std::string("exp"));
          octave_map output (dv,keys);

          for (octave_idx_type l=0;l<epscount;l++)
            {
              double epsilon=epsmin*pow(eps_fak,(double)l);
              for (octave_idx_type i=0;i<maxdim-1;i++)
                for (octave_idx_type j=0;j<=maxiter;j++) {
                   count[i][j]=0;
                   lyap[i][j]=0.0;
                }
              put_in_boxes(series, liste, box, length, maxdim, delay, maxiter, 
                           epsilon);
              for (octave_idx_type i=0;i<reference;i++)
                {
                  lfind_neighbors(series, lfound, found, liste, box, window,
                                  maxdim, delay, i,epsilon);
                  iterate_points(series, lyap, count, lfound, found,
                                 maxdim, mindim, delay, maxiter, i);
                }

              if (verbose)
                printf("epsilon= %e\n",epsilon*max_val);
              // Assign output
              for (octave_idx_type i=mindim-2;i<maxdim-1;i++)
                {

                  // old fprintf(fout,"#epsilon= %e  dim= %d\n", 
                  //             epsilon*max_val,i+2);
                  octave_scalar_map tmp (keys);
                  tmp.setfield ("eps", epsilon*max_val);
                  tmp.setfield ("dim", i+2);

                  // Create matrix for the exponent data
                  Matrix lyap_exp (maxiter + 1,3);
                  octave_idx_type counter = 0;
                  for (octave_idx_type j=0;j<=maxiter;j++)
                    if (count[i][j])
                      {
                        // old fprintf(fout,"%d %e %ld\n",j,
                        //             lyap[i][j]/count[i][j],count[i][j]);

                          lyap_exp(counter, 0) = j;
                          lyap_exp(counter, 1) = lyap[i][j]/count[i][j];
                          lyap_exp(counter, 2) = count[i][j];
                          counter             += 1;
                       }
                  // Resize output to fit actual number of found exponents
                  lyap_exp.resize(counter, 3);
                  tmp.setfield ("exp", lyap_exp);
                  output.assign(idx_vector(l),idx_vector(i-(int)(mindim-2)),
                                tmp);
                }
            }
          // Assign output
          retval(0) = output;
        }
    }
  return retval;
}
