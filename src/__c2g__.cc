/* Copyright (C) 1996-2015 Piotr Held
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */

/* Author: Piotr Held <pjheld@gmail.com>. 
 * This function is based on c2g of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/********************************************************************/
/********************************************************************/
#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "Quad.h"

DEFUN_DLD (__c2g__, args, , HELPTEXT)
{
    int nargin = args.length ();
    octave_value_list retval;

    if (nargin != 6)
      {
        print_usage ();
      }
    else
      {
        // Assing input
        Matrix h_mat = args(0).matrix_value();
        Matrix f_mat = args(1).matrix_value();
        Matrix d_mat = args(2).matrix_value();
        Matrix a_mat = args(3).matrix_value();
        Matrix b_mat = args(4).matrix_value();
        bool gd      = args(5).bool_value();

        // A proper input will have f_mat, d_mat, a_mat, b_mat
        // as column vectors and h_mat as a row vector
        octave_idx_type columns = h_mat.columns ();
        octave_idx_type rows    = f_mat.rows ();

        // Get pointers to arrays
        double *h = h_mat.fortran_vec ();
        double *f = f_mat.fortran_vec ();
        double *d = d_mat.fortran_vec ();
        double *a = a_mat.fortran_vec ();
        double *b = b_mat.fortran_vec ();

        // Create output matrix
        Matrix ret_mat (rows, columns);

        for (octave_idx_type j = 0; j < columns; j++)
            {
              // ret is used for code optimization
              // it's a pointer to data stored in ret_mat
              double *ret = ret_mat.fortran_vec () + j * rows;

              // Assign parameter of function quad_fcn
              static double h_g; // static so lambda does not capture anything
              h_g = h[j];

              for (octave_idx_type i = 0; i < rows; i++)
                {

                  // Assign parameters of function quad_fcn
                  static double f_g, d_g; // static so lambda does not capture
                  f_g = f[i];
                  d_g = d[i];

                  // Create quad_fcn for integration.
                  integrand_fcn quad_fcn;
                  if (gd)
                    {
                    quad_fcn = [](double u) -> double
                                 {
                                   return f_g * exp ((4 + d_g)*u - exp (2*u)
                                                     / (2*h_g*h_g));
                                 };
                    }
                  else
                    quad_fcn = [](double u) -> double
                                 {
                                   return f_g * exp ((2 + d_g)*u - exp (2*u)
                                                     / (2*h_g*h_g));
                                 };

                  // Perform integration
                  // If a[i] or b[i] is infinite, then (almost always) both
                  // vectors have an infinite element. But when b[i] has
                  // an infinite element the integral calculated with that
                  // boundary will be -Inf, thus the results of c2g likely to
                  // be NaN. Therefore there is no reason to perform special
                  // computation (e.g. using IndefQuad), because the final 
                  // result will be a NaN anyway.
                  DefQuad dq (quad_fcn, a[i], b[i]);
                  ret[i] = dq.integrate ();
                }
            }
          retval(0) = ret_mat;
      }

    return retval;
}
