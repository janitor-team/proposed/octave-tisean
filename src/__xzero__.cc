/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on xzero of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"

/*number of boxes for the neighbor search algorithm*/
#define NMAX 128

double make_fit(double *series1, double *series2, octave_idx_type *found,
                octave_idx_type act, octave_idx_type number,
                octave_idx_type istep)
{
  double casted=0.0;

  for (octave_idx_type i=0;i<number;i++)
    casted += series1[found[i]+istep];
  casted /= number;

  return (casted-series2[act+istep])*(casted-series2[act+istep]);
}

DEFUN_DLD (__xzero__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 10)
  {
    print_usage ();
  }
  else
    {

      // Assign input
      NDArray input1          = args(0).array_value ();
      NDArray input2          = args(1).array_value ();
      octave_idx_type DIM     = args(2).idx_type_value ();
      octave_idx_type DELAY   = args(3).idx_type_value ();
      octave_idx_type CLENGTH = args(4).idx_type_value ();
      octave_idx_type MINN    = args(5).idx_type_value ();
      double EPS0             = args(6).double_value ();
      bool epsset             = args(7).bool_value ();
      double EPSF             = args(8).double_value ();
      octave_idx_type STEP    = args(9).idx_type_value ();

      octave_idx_type LENGTH = input1.numel ();
      double *series1        = input1.fortran_vec ();
      double *series2        = input2.fortran_vec ();

      // Analyze input
      double min_val,hinter;
      rescale_data(input1,LENGTH,&min_val,&hinter);
      double interval=hinter;
      rescale_data(input2,LENGTH,&min_val,&hinter);
      interval=(interval+hinter)/2.0;

      double av2, rms2;
      variance(input2,LENGTH,&av2,&rms2);

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (octave_idx_type, list, LENGTH);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, found, LENGTH);
      OCTAVE_LOCAL_BUFFER (bool, done, LENGTH);
      OCTAVE_LOCAL_BUFFER (double, error_array, STEP);

      for (octave_idx_type i=0;i<STEP;i++)
        error_array[i]=0.0;

      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, NMAX);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, box_data, NMAX * NMAX);
      for (octave_idx_type i=0;i<NMAX;i++)
        box[i]=box_data + NMAX * i;


      // Setup variables
      for (octave_idx_type i=0;i<LENGTH;i++)
        done[i]=0;

      bool alldone=0;
      if (epsset)
        EPS0 /= interval;

      double epsilon=EPS0/EPSF;
      octave_idx_type clength=(CLENGTH <= LENGTH) ? CLENGTH-STEP : LENGTH-STEP;

      if (! error_state)
        {
          // Calculate fit
          while (!alldone) {
            alldone=1;
            epsilon*=EPSF;
            make_box(series1,box,list,LENGTH-STEP,NMAX,DIM,DELAY,epsilon);
            for (octave_idx_type i=(DIM-1)*DELAY;i<clength;i++)
              if (!done[i]) {
                octave_idx_type actfound;
                actfound=find_neighbors(series1,box,list,series2+i,LENGTH,NMAX,
                                        DIM,DELAY,epsilon,found);
                if (actfound >= MINN) {
                  for (octave_idx_type j=1;j<=STEP;j++)
                    error_array[j-1] += make_fit (series1, series2, found,
                                                  i,actfound,j);
                  done[i]=1;
                }
                alldone &= done[i];
              }
          }

          // Create output
          Matrix output (STEP,1);
          for (octave_idx_type i=0;i<STEP;i++)
            {
//            old fprintf(stdout,"%lu %e\n",i+1,
//                      sqrt(error_array[i]/(clength-(DIM-1)*DELAY))/rms2);
              output(i,0) = sqrt(error_array[i]/(clength-(DIM-1)*DELAY))/rms2;
            }
          retval(0) = output;
        }

    }
  return retval;
}
