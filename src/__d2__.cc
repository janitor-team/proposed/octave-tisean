/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on d2 of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <limits>
#include <octave/oct.h>
#include <octave/oct-map.h>
#include "routines_c/tsa.h"

// the nargin of the function call from a paused state
#define PAUSE_NARGIN 21

/* output is written every WHEN seconds */
#define WHEN 120
/* Size of the field for box assisted neighbour searching 
   (has to be a power of 2)*/
#define NMAX 256
/* Size of the box for the scramble routine */
#define SCBOX 4096



void scramble(octave_idx_type length, octave_idx_type EMBED,
              octave_idx_type DELAY, 
              octave_idx_type *scr)
{

  unsigned long rnd,rndf;

  if (sizeof(long) == 8) {
    rndf=13*13*13*13;
    rndf=rndf*rndf*rndf*13;
    rnd=0x849178L;
  }
  else {
    rndf=69069;
    rnd=0x234571L;
  }
  for (octave_idx_type i=0;i<1000;i++)
    rnd=rnd*rndf+1;

  octave_idx_type hlength=length-(EMBED-1)*DELAY;

  OCTAVE_LOCAL_BUFFER (double, rz, hlength);
  OCTAVE_LOCAL_BUFFER (octave_idx_type, scfound, hlength);
  OCTAVE_LOCAL_BUFFER (octave_idx_type, scnhelp, hlength);
  OCTAVE_LOCAL_BUFFER (double, schelp, hlength);

  for (octave_idx_type i=0;i<hlength;i++)
    rz[i]=(double)(rnd=rnd*rndf+1)/std::numeric_limits<unsigned long>::max ();

  octave_idx_type scbox[SCBOX];
  for (octave_idx_type i=0;i<SCBOX;i++)
    scbox[i]= -1;
  for (octave_idx_type i=0;i<hlength;i++) {
    octave_idx_type m=(int)(rz[i]*((double)SCBOX-0.001))&(SCBOX-1);
    scfound[i]=scbox[m];
    scbox[m]=i;
  }

  octave_idx_type allscr = 0;
  for (octave_idx_type i=0;i<SCBOX;i++)
    {
      octave_idx_type scnfound=0;
      octave_idx_type element=scbox[i];
      while(element != -1) {
        scnhelp[scnfound]=element;
        schelp[scnfound++]=rz[element];
        element=scfound[element];
      }

      for (octave_idx_type j=0;j<scnfound-1;j++)
        for (octave_idx_type k=j+1;k<scnfound;k++)
          if (schelp[k] < schelp[j]) {
            double swap=schelp[k];
            schelp[k]=schelp[j];
            schelp[j]=swap;
            octave_idx_type lswap=scnhelp[k];
            scnhelp[k]=scnhelp[j];
            scnhelp[j]=lswap;
          }
      for (octave_idx_type j=0;j<scnfound;j++)
        scr[allscr+j]=scnhelp[j];
      allscr += scnfound;
    }
}

void make_c2_dim(const double **series, double **found, octave_idx_type *list,
                 octave_idx_type **box, octave_idx_type DIM,
                 octave_idx_type imin, double EPSMAX1, double EPSMAX,
                 double EPSMIN, octave_idx_type EMBED, octave_idx_type DELAY,
                 octave_idx_type MINDIST, octave_idx_type HOWOFTEN,
                 octave_idx_type n1)
{

  OCTAVE_LOCAL_BUFFER (double, hs, EMBED * DIM);

  octave_idx_type count = 0;
  for (octave_idx_type i1=0;i1<EMBED;i1++) {
    octave_idx_type i2=i1*DELAY;
    for (octave_idx_type j=0;j<DIM;j++)
      hs[count++]=series[j][n1+i2];
  }

  double epsinv     = 1.0 / EPSMAX;
  octave_idx_type x = (octave_idx_type)(hs[0]*epsinv)&(NMAX-1);
  octave_idx_type y = (octave_idx_type)(hs[1]*epsinv)&(NMAX-1);

  for (octave_idx_type i1=x-1;i1<=x+1;i1++) {
    octave_idx_type i2=i1&(NMAX-1);
    for (octave_idx_type j1=y-1;j1<=y+1;j1++) {
      octave_idx_type element=box[i2][j1&(NMAX-1)];
      while (element != -1) {
        if (labs((long)(element-n1)) > MINDIST) {
          octave_idx_type count = 0;
          double max            = 0.0;
          octave_idx_type maxi  = HOWOFTEN - 1;
          bool small            = 0;
          for (octave_idx_type i=0;i<EMBED;i++) {
            octave_idx_type hi=i*DELAY;
            for (octave_idx_type j=0;j<DIM;j++) {
              double dx=fabs(hs[count]-series[j][element+hi]);
              if (dx <= EPSMAX) {
                if (dx > max) {
                  max=dx;
                  if (max < EPSMIN) {
                    maxi=(HOWOFTEN-1);
                  }
                  else
                    {
                      double epsfactor = pow(EPSMAX1/EPSMIN,
                                             1.0/(double)(HOWOFTEN-1));
                      maxi=(log(EPSMAX1)-log(max))/log(epsfactor);
                    }
                }
                if (count > 0)
                  for (octave_idx_type k=imin;k<=maxi;k++)
                    found[count][k] += 1.0;
              }
              else {
                small=1;
                break;
              }
              count++;
            }
            if (small)
              break;
          }
        }
        element=list[element];
      }
    }
  }

}

void make_c2_1(const double **series, double **found, octave_idx_type *listc1,
               octave_idx_type *boxc1, octave_idx_type imin, double EPSMAX1,
               double EPSMAX, double EPSMIN, octave_idx_type MINDIST,
               octave_idx_type HOWOFTEN, octave_idx_type n1)
{

  double hs = series[0][n1];

  double epsinv     = 1.0 / EPSMAX;
  octave_idx_type x = (octave_idx_type)(hs*epsinv)&(NMAX-1);

  for (octave_idx_type i1=x-1;i1<=x+1;i1++) {
    octave_idx_type element=boxc1[i1&(NMAX-1)];
    while (element != -1)
      {
        if (abs(element-n1) > MINDIST)
          {
            double max_val=fabs(hs-series[0][element]);
            if (max_val <= EPSMAX)
              {
                octave_idx_type maxi;
                if (max_val < EPSMIN)
                  maxi=HOWOFTEN - 1;
                else
                  {
                    double epsfactor = pow (EPSMAX1/EPSMIN,
                                            1.0/(double)(HOWOFTEN-1));
                    maxi=(log(EPSMAX1)-log(max_val))/log(epsfactor);
                  }
                for (octave_idx_type i=imin;i<=maxi;i++)
                  found[0][i] += 1.0;
              }
          }
        element=listc1[element];
      }
  }
}

DEFUN_DLD (__d2__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if ((nargin != 12 && nargin != PAUSE_NARGIN) || nargout != 2)
  {
    print_usage ();
  }
  else
    {
      // Assign input
      Matrix input             = args(0).matrix_value ();
      octave_idx_type EMBED    = args(1).idx_type_value ();
      octave_idx_type DELAY    = args(2).idx_type_value ();
      octave_idx_type MINDIST  = args(3).idx_type_value ();
      double EPSMIN            = args(4).double_value ();
      bool eps_min_set         = args(5).bool_value ();
      double EPSMAX            = args(6).double_value ();
      bool eps_max_set         = args(7).bool_value ();
      octave_idx_type HOWOFTEN = args(8).idx_type_value ();
      octave_idx_type MAXFOUND = args(9).idx_type_value ();
      bool rescale_set         = args(10).bool_value ();
      octave_idx_type it_pause = args(11).idx_type_value ();

      octave_idx_type length = input.rows ();
      octave_idx_type DIM    = input.columns ();


      // Series is a pointer to data stored in input
      // so input(i,j) == series[j][i]
      // This is done for optimization purposes
      OCTAVE_LOCAL_BUFFER (const double *, series, DIM);
      for (octave_idx_type j = 0; j < DIM; j++)
        {
          const double *ptr = input.fortran_vec ();
          series[j] = ptr + length * j;
        }


      // Create or restore variables used when pausing calculation
      // '*_matrix' points to the same data as '*'
      // this is done for ease of pausing
      octave_idx_type counter;
      Matrix found_matrix (HOWOFTEN, DIM*EMBED);
      Matrix norm_matrix (HOWOFTEN, 1);
      Array <octave_idx_type> boxc1_matrix (dim_vector(NMAX, 1));
      Array <octave_idx_type> box_matrix (dim_vector(NMAX, NMAX));
      Array <octave_idx_type> list_matrix (dim_vector (length,1));
      Array <octave_idx_type> listc1_matrix (dim_vector (length,1));
      double EPSMAX1;// this is the original EPSMAX
      octave_idx_type imin;

      if (nargin == PAUSE_NARGIN) // restore from paused state
        {
          counter       = args(12).idx_type_value ();
          found_matrix  = args(13).matrix_value ();
          norm_matrix   = args(14).matrix_value ();
          boxc1_matrix  = args(15).octave_idx_type_vector_value ();
          box_matrix    = args(16).octave_idx_type_vector_value ();
          list_matrix   = args(17).octave_idx_type_vector_value ();
          listc1_matrix = args(18).octave_idx_type_vector_value ();
          imin          = args(19).idx_type_value ();
          EPSMAX1       = args(20).double_value ();
        }
      else // Prepare variables at beginning of calculation
        {
          counter = 0;

          found_matrix.fill (0.0);
          norm_matrix.fill (0.0);
          boxc1_matrix.fill (-1);
          box_matrix.fill (-1);

          double maxinterval;
          if (rescale_set) {
            double interval, min_val;
            for (octave_idx_type i=0;i<DIM;i++)
              rescale_data(input,i,length,&min_val,&interval);
            maxinterval=1.0;
          }
          else {
            maxinterval=0.0;
            for (octave_idx_type i=0;i<DIM;i++) {
              double interval = series[i][0];
              double min_val  = series[i][0];
              for (octave_idx_type j=1;j<length;j++) {
                if (min_val > series[i][j])
                  min_val=series[i][j];
                if (interval < series[i][j])
                  interval=series[i][j];
              }
              interval -= min_val;
              if (interval > maxinterval)
                maxinterval=interval;
            }
          }
          if (!eps_max_set)
            EPSMAX *= maxinterval;
          if (!eps_min_set)
            EPSMIN *= maxinterval;
          EPSMAX  = (fabs(EPSMAX)<maxinterval) ? fabs(EPSMAX) : maxinterval;
          EPSMIN  = (fabs(EPSMIN)<EPSMAX) ? fabs(EPSMIN) : EPSMAX/2.;
          //EPSMAX1 - original EPSMAX
          EPSMAX1 = EPSMAX;
          imin    = 0;
        }

      // Create pointers to octave Array variables
      OCTAVE_LOCAL_BUFFER (double *, found, DIM * EMBED);
      for (octave_idx_type i=0;i<EMBED*DIM;i++)
        found[i] = found_matrix.fortran_vec () + HOWOFTEN * i;

      double *norm           = norm_matrix.fortran_vec ();
      octave_idx_type *boxc1 = boxc1_matrix.fortran_vec ();

      OCTAVE_LOCAL_BUFFER (octave_idx_type *, box, NMAX);
      for (octave_idx_type i = 0; i< NMAX; i++)
        box[i] = box_matrix.fortran_vec () + NMAX * i;

      octave_idx_type *list   = list_matrix.fortran_vec ();
      octave_idx_type *listc1 = listc1_matrix.fortran_vec ();


      // Allocate memory
      OCTAVE_LOCAL_BUFFER (octave_idx_type, scr, length-(EMBED-1)*DELAY);
      OCTAVE_LOCAL_BUFFER (octave_idx_type, oscr, length-(EMBED-1)*DELAY);
      OCTAVE_LOCAL_BUFFER (double, epsm, HOWOFTEN);

      // epsfactor uses original EPSMAX which is EPSMAX1
      double epsfactor=pow(EPSMAX1/EPSMIN,1.0/(double)(HOWOFTEN-1));

      epsm[0]=EPSMAX1;
      for (octave_idx_type i=1;i<HOWOFTEN;i++) {
        epsm[i]=epsm[i-1]/epsfactor;
      }

      scramble(length, EMBED, DELAY, scr);
      for (octave_idx_type i=0;i<(length-(EMBED-1)*DELAY);i++)
        oscr[scr[i]]=i;

      octave_idx_type maxembed=DIM*EMBED-1;
      octave_idx_type nmax    = length-DELAY*(EMBED-1);

      time_t lasttime;
      time(&lasttime);

      if (! error_state)
        {

          bool imin_too_large = false;
          bool pause_calc     = false;
          // Calculate the outputs
          for (octave_idx_type n = 1 + counter; n < nmax && !imin_too_large
                                                && !pause_calc; n++)
            {
              counter           += 1;
              bool smaller       = 0;
              octave_idx_type sn = scr[n-1];
              double epsinv      = 1.0 / EPSMAX;
              octave_idx_type x,y;
              if (DIM > 1)
                {
                  x=(octave_idx_type)(series[0][sn]*epsinv)&(NMAX-1);
                  y=(octave_idx_type)(series[1][sn]*epsinv)&(NMAX-1);
                }
              else
                {
                  x=(octave_idx_type)(series[0][sn]*epsinv)&(NMAX-1);
                  y=(octave_idx_type)(series[0][sn+DELAY]*epsinv)&(NMAX-1);
                }
              list[sn]=box[x][y];
              box[x][y]=sn;
              listc1[sn]=boxc1[x];
              boxc1[x]=sn;

              octave_idx_type i=imin;
              while (found[maxembed][i] >= MAXFOUND)
                {
                  smaller=1;
                  if (++i > (HOWOFTEN-1))
                    break;
                }
              if (smaller)
                {
                  imin=i;
                  if (imin <= (HOWOFTEN-1))
                    {
                      EPSMAX        = epsm[imin];
                      double epsinv = 1.0/EPSMAX;
                      for (octave_idx_type i1=0;i1<NMAX;i1++)
                        {
                          boxc1[i1]= -1;
                          for (octave_idx_type j1=0;j1<NMAX;j1++)
                            box[i1][j1]= -1;
                        }
                      for (octave_idx_type i1=0;i1<n;i1++)
                        {
                          sn=scr[i1];
                          octave_idx_type x,y;
                          if (DIM > 1)
                            {
                              x=(octave_idx_type)(series[0][sn]*epsinv)
                                &(NMAX-1);
                              y=(octave_idx_type)(series[1][sn]*epsinv)
                                &(NMAX-1);
                            }
                          else
                            {
                              x=(octave_idx_type)(series[0][sn]*epsinv)
                                &(NMAX-1);
                              y=(octave_idx_type)(series[0][sn+DELAY]*epsinv)
                                &(NMAX-1);
                            }
                          list[sn]=box[x][y];
                          box[x][y]=sn;
                          listc1[sn]=boxc1[x];
                          boxc1[x]=sn;
                        }
                    }
                }

              if (imin <= (HOWOFTEN-1))
                {
                  octave_idx_type lnorm=n;
                  if (MINDIST > 0)
                    {
                      octave_idx_type sn=scr[n];
                      octave_idx_type n1=(sn-MINDIST>=0)?sn-MINDIST:0;
                      octave_idx_type n2=(sn+MINDIST<length-(EMBED-1)*DELAY)
                                         ?sn+MINDIST:length-(EMBED-1)*DELAY-1;
                      for (octave_idx_type i1=n1;i1<=n2;i1++)
                        if ((oscr[i1] < n))
                          lnorm--;
                    }

                  if (EMBED*DIM > 1)
                    make_c2_dim(series, found, list, box, DIM, imin, EPSMAX1,
                                EPSMAX, EPSMIN, EMBED, DELAY, MINDIST,
                                HOWOFTEN, scr[n]);
                  make_c2_1(series, found, listc1, boxc1, imin, EPSMAX1,
                            EPSMAX, EPSMIN, MINDIST, HOWOFTEN, scr[n]);
                  for (octave_idx_type i=imin;i<HOWOFTEN;i++)
                    norm[i] += (double)(lnorm);
                }


              // If any of the below occurs: pause or end.
              if (((time(NULL)-lasttime) > WHEN) || (n == (nmax-1)) || 
                  (imin > (HOWOFTEN-1)) || (counter % it_pause == 0))
                {
                  time(&lasttime);

                  if (imin > (HOWOFTEN-1))
                    {
                      // old exit(0);
                      imin_too_large = true;
                    }
                  pause_calc = true;
                }
            }

          // Create vars output
          octave_scalar_map vars;


          // Create vars output
          // old fprintf(fstat,"Center points treated so far= %ld\n",n);
          vars.setfield ("treated", counter);
          // old fprintf(fstat,"Maximum epsilon in the moment= %e\n",
          //             epsm[imin]);
          vars.setfield ("eps", epsm[imin]);

          if (counter < nmax - 1 && imin_too_large == false)
            {
              vars.setfield ("counter", counter);
              vars.setfield ("found", found_matrix);
              vars.setfield ("norm", norm_matrix);
              vars.setfield ("boxc1", boxc1_matrix);
              vars.setfield ("box", box_matrix);
              vars.setfield ("list", list_matrix);
              vars.setfield ("listc1", listc1_matrix);
              vars.setfield ("imin", imin);
              vars.setfield ("EPSMAX1",EPSMAX1);
              vars.setfield ("EPSMAX", EPSMAX);
              vars.setfield ("EPSMIN", EPSMIN);
            }

          // Create values output
          dim_vector dv (DIM * EMBED, 1);
          string_vector keys;
          keys.append (std::string("dim"));
          keys.append (std::string("c2"));
          keys.append (std::string("d2"));
          keys.append (std::string("h2"));
          octave_map values (dv, keys);

          for (octave_idx_type i=0;i<DIM*EMBED;i++)
            {

              octave_scalar_map tmp (keys);

              // old fprintf(fout,"#dim= %ld\n",i+1);
              tmp.setfield ("dim", i+1);

              // Allocate d2 output
              Matrix d2_out (HOWOFTEN - 1, 2);
              octave_idx_type d2_row = 0;

              // Allocate h2 output
              Matrix h2_out (HOWOFTEN, 2);
              octave_idx_type h2_row = 0;

              // Allocate c2 output
              Matrix c2_out (HOWOFTEN, 2);
              octave_idx_type c2_row = 0;

              double eps = EPSMAX1 * epsfactor;

              for (octave_idx_type j=0;j<HOWOFTEN;j++)
                {
                  eps /= epsfactor;

                  // Calculate d2 output
                  if ((j > 0) && (found[i][j] > 0.0)
                      && (found[i][j-1] > 0.0))
                    {
                      // old fprintf(fout,"%e %e\n",eps,
                      //      log(found[i][j-1]/found[i][j]/norm[j-1]
                      //          *norm[j])
                      //      /log(epsfactor));
                      d2_out(d2_row,0) = eps;
                      d2_out(d2_row,1) = log(found[i][j-1]/found[i][j]
                                             /norm[j-1]*norm[j])
                                         /log(epsfactor);
                      d2_row          += 1;
                    }

                  // Calculate h2 output
                  if (i < 1)
                    {
                      if (found[0][j] > 0.0)
                        {
                          // old fprintf(fout,"%e %e\n",eps,
                          //             -log(found[0][j]/norm[j]));
                          h2_out(h2_row,0) = eps;
                          h2_out(h2_row,1) = -log(found[0][j]/norm[j]);
                          h2_row          += 1;
                        }
                    }
                  else
                    {
                      if ((found[i-1][j] > 0.0) && (found[i][j] > 0.0))
                        {
                        // old fprintf(fout,"%e %e\n",eps,
                        //             log(found[i-1][j]/found[i][j]));
                          h2_out(h2_row,0) = eps;
                          h2_out(h2_row,1) = log(found[i-1][j]
                                                 /found[i][j]);
                          h2_row          += 1;
                        }
                    }

                  // Calculate c2 output
                  if (norm[j] > 0.0)
                    {
                      // old fprintf(fout,"%e %e\n",eps,
                      //             found[i][j]/norm[j]);
                      c2_out(c2_row,0) = eps;
                      c2_out(c2_row,1) = found[i][j]/norm[j];
                      c2_row          += 1;
                    }
                }
              // Prepare d2 output
              d2_out.resize (d2_row, 2);
              tmp.setfield ("d2", d2_out);
              // Prepare h2 output
              h2_out.resize (h2_row, 2);
              tmp.setfield ("h2", h2_out);
              // Prepare c2 output
              c2_out.resize (c2_row, 2);
              tmp.setfield ("c2", c2_out);

              values.assign (idx_vector(i), tmp);
            }


          // Assign outputs
          retval(0) = values;
          retval(1) = vars;
        }

    }
  return retval;
}
