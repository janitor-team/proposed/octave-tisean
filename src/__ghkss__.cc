/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on ghkss of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>

#include "routines_c/tsa.h"

#define BOX 1024

int eps_set=0,euclidean=0,dimset=0;

octave_idx_type dim,qdim=2,minn=50,iterations=1,embed=5,length;
octave_idx_type comp=1;
unsigned int delay=1;
unsigned int verbosity=0;
double mineps,epsfac;

char resize_eps;

double *d_min,*d_max,d_max_max;
double **delta,**corr;
double *metric,trace;
long **box,*list;
unsigned long *flist;
int emb_offset;
unsigned int ibox=BOX-1;
unsigned int *index_comp,*index_embed;

/*these are global to save time*/
int *sorted;
double *av,**mat,*matarray,*eig;


void sort(double *x,int *n)
{
  octave_idx_type i,j;
  long iswap;
  double dswap;
  
  for (i=0;i<dim;i++)
    n[i]=i;
  
  for (i=0;i<dim-1;i++)
    for (j=i+1;j<dim;j++)
      if (x[j] > x[i]) {
      dswap=x[i];
      x[i]=x[j];
      x[j]=dswap;
      iswap=n[i];
      n[i]=n[j];
      n[j]=iswap;
      }
}

void mmb(const Matrix &series,double eps)
{
  octave_idx_type i,x,y;
  double ieps=1.0/eps;

  for (x=0;x<BOX;x++)
    for (y=0;y<BOX;y++)
      box[x][y] = -1;
   
  for (i=emb_offset;i<length;i++) {
    x=(int)(series(i,0)*ieps)&ibox;
    y=(int)(series(i-emb_offset,comp-1)*ieps)&ibox;
    list[i]=box[x][y];
    box[x][y]=i;
  }
}

unsigned long fmn(const Matrix &series, long which,double eps)
{
  unsigned long nf=0;
  octave_idx_type i,i1,i2,j,j1,k,li;
  long k1;
  long element;
  double dx=0.0;
  
  i=(int)(series(which,0)/eps)&ibox;
  j=(int)(series(which-emb_offset,comp-1)/eps)&ibox;
  
  for (i1=i-1;i1<=i+1;i1++) {
    i2=i1&ibox;
    for (j1=j-1;j1<=j+1;j1++) {
      element=box[i2][j1&ibox];
      while (element != -1) {
      for (k=0;k<embed;k++) {
        k1= -k*(int)delay;
        for (li=0;li<comp;li++) {
          dx=fabs(series(which+k1,li)-series(element+k1,li));
          if (dx > eps)
            break;
        }
        if (dx > eps)
          break;
      }
      if (dx <= eps)
        flist[nf++]=element;
      element=list[element];
      }
    }
  }
  return nf;
}

void make_correction(const Matrix &series, unsigned long n,octave_idx_type nf)
{
  octave_idx_type i,i1,i2,j,j1,j2,k,k1,k2,hs;
  double help;
  
  for (i=0;i<dim;i++) {
    i1=index_comp[i];
    i2=index_embed[i];
    help=0.0;
    for (j=0;j<nf;j++)
      help += series(flist[j]-i2,i1);
    av[i]=help/nf;
  }

  for (i=0;i<dim;i++) {
    i1=index_comp[i];
    i2=index_embed[i];
    for (j=i;j<dim;j++) {
      help=0.0;
      j1=index_comp[j];
      j2=index_embed[j];
      for (k=0;k<nf;k++) {
      hs=flist[k];
      help += series(hs-i2,i1)*series(hs-j2,j1);
      }
      mat[i][j]=(help/nf-av[i]*av[j])*metric[i]*metric[j];
      mat[j][i]=mat[i][j];
    }
  }

  eigen(mat,dim,eig);
  sort(eig,sorted);

  for (i=0;i<dim;i++) {
    help=0.0;
    for (j=qdim;j<dim;j++) {
      hs=sorted[j];
      for (k=0;k<dim;k++) {
      k1=index_comp[k];
      k2=index_embed[k];
      help += (series(n-k2,k1)-av[k])*mat[k][hs]*mat[i][hs]*metric[k];
      }
    }
    corr[n][i]=help/metric[i];
  }
}

void handle_trend(unsigned long n,octave_idx_type nf)
{
  octave_idx_type i,i1,i2,j;
  double help;
  
  for (i=0;i<dim;i++) {
    help=0.0;
    for (j=0;j<nf;j++)
      help += corr[flist[j]][i];
    av[i]=help/nf;
  }

  for (i=0;i<dim;i++) {
    i1=index_comp[i];
    i2=index_embed[i];
    delta[i1][n-i2] += (corr[n][i]-av[i])/(trace*metric[i]);
  }
}

void set_correction(Matrix &series)
{
  octave_idx_type i,j;
  double help;

  OCTAVE_LOCAL_BUFFER (double, hav, comp);
  OCTAVE_LOCAL_BUFFER (double, hsigma, comp);
  if ( ! error_state)
    {
      for (j=0;j<comp;j++)
        hav[j]=hsigma[j]=0.0;

      for (i=0;i<length;i++)
        for (j=0;j<comp;j++) {
          hav[j] += (help=delta[j][i]);
          hsigma[j] += help*help;
        }

      for (j=0;j<comp;j++) {
        hav[j] /= length;
        hsigma[j]=sqrt(fabs(hsigma[j]/length-hav[j]*hav[j]));
      }
      if (verbosity) {
        for (i=0;i<comp;i++) {
          octave_stdout << "Average shift of component " << i+1 << " = "
                        << hav[i]*d_max[i] << "\n";
          octave_stdout << "Average rms correction of comp. " << i+1 << " = "
                        << hsigma[i]*d_max[i] << "\n\n";
        }
      }
      for (i=0;i<length;i++)
        for (j=0;j<comp;j++)
          series(i,j) -= delta[j][i];

      if (resize_eps) {
        mineps /= epsfac;
        if (verbosity)
          octave_stdout << "Reset minimal neighbourhood size to "
                        << mineps*d_max_max << "\n";
      }

      resize_eps=0;
    }
}

DEFUN_DLD (__ghkss__, args, , HELPTEXT)
{

  int epscount,*ok;
  octave_idx_type iter,nfound,n;
  octave_idx_type i,j;
  char all_done;
  unsigned long allfound;
  double epsilon;
  double **hser;

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 11)
  {
    print_usage ();
  }
  else
    {

      // Assign input
      Matrix input = args(0).matrix_value();
      embed        = args(1).int_value();
      comp         = args(2).int_value();
      delay        = args(3).int_value();
      qdim         = args(4).int_value();
      minn         = args(5).int_value();
      mineps       = args(6).double_value();
      eps_set      = args(7).int_value();
      iterations   = args(8).int_value();
      euclidean    = args(9).int_value();
      verbosity    = args(10).int_value();

      dim=comp*embed;
      emb_offset=(embed-1)*delay;
      length = input.rows();

      // Prepare for noise reduction and allocate memory
      check_alloc(d_min=(double*)malloc(sizeof(double)*comp));
      check_alloc(d_max=(double*)malloc(sizeof(double)*comp));
      d_max_max=0.0;
      for (i=0;i<comp;i++) {
        rescale_data(input,i,length,&d_min[i],&d_max[i]);
        if (d_max[i] > d_max_max)
          d_max_max=d_max[i];
      }

      if (!eps_set)
        mineps=1./1000.;
      else
        mineps /= d_max_max;
      epsfac=sqrt(2.0);

      check_alloc(box=(long**)malloc(sizeof(long*)*BOX));
      for (i=0;i<BOX;i++)
        check_alloc(box[i]=(long*)malloc(sizeof(long)*BOX));

      check_alloc(list=(long*)malloc(sizeof(long)*length));
      check_alloc(flist=(unsigned long*)malloc(sizeof(long)*length));

      check_alloc(metric=(double*)malloc(sizeof(double)*dim));
      trace=0.0;
      if (euclidean) {
        for (i=0;i<dim;i++) {
          metric[i]=1.0;
          trace += 1./metric[i];
        }
      }
      else {
        for (i=0;i<dim;i++) {
          if ((i >= comp) && (i < ((long)dim-(long)comp))) 
          metric[i]=1.0;
          else 
          metric[i]=1.0e3;
          trace += 1./metric[i];
        }
      }

      check_alloc(corr=(double**)malloc(sizeof(double*)*length));
      for (i=0;i<length;i++)
        check_alloc(corr[i]=(double*)malloc(sizeof(double)*dim));
      check_alloc(ok=(int*)malloc(sizeof(int)*length));
      check_alloc(delta=(double**)malloc(sizeof(double*)*comp));
      for (i=0;i<comp;i++)
        check_alloc(delta[i]=(double*)malloc(sizeof(double)*length));
      check_alloc(index_comp=(unsigned int*)malloc(sizeof(int)*dim));
      check_alloc(index_embed=(unsigned int*)malloc(sizeof(int)*dim));
      check_alloc(av=(double*)malloc(sizeof(double)*dim));
      check_alloc(sorted=(int*)malloc(sizeof(int)*dim));
      check_alloc(eig=(double*)malloc(sizeof(double)*dim));
      check_alloc(matarray=(double*)malloc(sizeof(double)*dim*dim));
      check_alloc(mat=(double**)malloc(sizeof(double*)*dim));
      for (i=0;i<dim;i++)
        mat[i]=(double*)(matarray+dim*i);
      check_alloc(hser=(double**)malloc(sizeof(double*)*comp));

      if ( ! error_state)
        {
          // Create output matrix
          Matrix output (length, comp);

          for (i=0;i<dim;i++) {
            index_comp[i]=i%comp;
            index_embed[i]=(i/comp)*delay;
          }

          // Calculate the noise reduction
          resize_eps=0;
          for (iter=1;iter<=iterations;iter++) 
            {
              for (i=0;i<length;i++) {
                ok[i]=0;
                for (j=0;j<dim;j++)
                corr[i][j]=0.0;
                for (j=0;j<comp;j++)
                delta[j][i]=0.0;
              }
              epsilon=mineps;
              all_done=0;
              epscount=1;
              allfound=0;
              if (verbosity)
                octave_stdout << "Starting iteration " << iter << "\n";
              while(!all_done) {
                mmb(input, epsilon);
                all_done=1;
                for (n=emb_offset;n<length;n++)
                if (!ok[n]) {
                  nfound=fmn(input,n,epsilon);
                  if (nfound >= minn) {
                    make_correction(input,n,nfound);
                    ok[n]=epscount;
                    if (epscount == 1)
                      resize_eps=1;
                    allfound++;
                  }
                  else
                    all_done=0;
                }
                if (verbosity)
                  octave_stdout << "Corrected " << allfound << " points with epsilon= "
                                << epsilon*d_max_max << "\n";
                if (std::isinf(epsilon*d_max_max))
                  {
                    error_with_id ("Octave:tisean", "cannot reduce noise on input data");
                    return retval;
                  }
                epsilon *= epsfac;
                epscount++;
              }
              if (verbosity)
                octave_stdout << "Start evaluating the trend\n";

              epsilon=mineps;
              allfound=0;
              for (i=1;i<epscount;i++) {
                mmb(input,epsilon);
                for (n=emb_offset;n<length;n++)
                if (ok[n] == i) {
                  nfound=fmn(input,n,epsilon);
                  handle_trend(n,nfound);
                  allfound++;
                }
                if (verbosity)
                octave_stdout << "Trend subtracted for " << allfound << " points with epsilon= " 
                              << epsilon*d_max_max << "\n";
                epsilon *= epsfac;
              }
              set_correction(input);

              if (iter == iterations)
                for (i=0;i<length;i++) 
                  {
                    for (j=0;j<comp;j++) 
                      {
                      // old  fprintf(file,"%e ",series[j][i]*d_max[j]+d_min[j]);
                        output(i,j) = input(i,j)*d_max[j]+d_min[j];
                      }
                  }
            }
          retval(0) = output;
        }
    // Deallocate of all the memory
      delete[] d_min;
      delete[] d_max;
      for (i=0;i<BOX;i++)
        delete[] box[i];
      delete[] box;
      delete[] list;
      delete[] flist;
      delete[] metric;
      for (i=0;i<length;i++)
        delete[] corr[i];
      delete[] corr;
      delete[] ok;
      for (i=0;i<comp;i++)
        delete[] delta[i];
      delete[] delta;
      delete[] index_comp;
      delete[] index_embed;
      delete[] av;
      delete[] sorted;
      delete[] eig;
      delete[] matarray;
      delete[] mat;
      delete[] hser;
    }
  return retval;
}
