/* -*- coding: utf-8 -*- */
/* Copyright (C) 1996-2015 Piotr Held <pjheld@gmail.com>
 *
 * This file is part of Octave.
 *
 * Octave is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any
 * later version.
 *
 * Octave is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public
 * License along with Octave; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 */
/* Author: Piotr Held <pjheld@gmail.com>.
 * This function is based on c1 of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */
/******************************************************************************/
/******************************************************************************/

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include <octave/f77-fcn.h>
#include <octave/oct-map.h>

#if defined (OCTAVE_HAVE_F77_INT_TYPE)
#  define TO_F77_INT(x) octave::to_f77_int (x)
#else
typedef octave_idx_type F77_INT;
#  define TO_F77_INT(x) (x)
#endif

extern "C"
{
  F77_RET_T
  F77_FUNC (d1, D1)
            (const F77_INT& nmax, const F77_INT& mmax,
             const F77_INT& nxx, const double *y,
             const F77_INT& delay, const F77_INT& m,
             const F77_INT& ncmin, const double& pr,
             double& pln, double& eln, 
             const F77_INT& nmin, const F77_INT& kmax,
             const F77_INT& iverb);

  F77_RET_T
  F77_FUNC (rand, RAND)
            (const double& R);
}


DEFUN_DLD (__c1__, args, nargout, HELPTEXT)
{
  octave_value_list retval;
  int nargin = args.length ();


  if (nargin != 10)
    {
      print_usage ();
    }
  else
    {
    // Assigning inputs
      Matrix input           = args(0).matrix_value ();
      F77_INT mindim         = TO_F77_INT (args(1).idx_type_value ());
      F77_INT maxdim         = TO_F77_INT (args(2).idx_type_value ());
      F77_INT delay          = TO_F77_INT (args(3).idx_type_value ());
      F77_INT tmin           = TO_F77_INT (args(4).idx_type_value ());
      F77_INT cmin           = TO_F77_INT (args(5).idx_type_value ());
      double resolution      = args(6).double_value ();
      double seed            = args(7).double_value ();
      F77_INT kmax           = TO_F77_INT (args(8).idx_type_value ());
      bool verbose           = args(9).bool_value ();
      F77_INT iverb          = TO_F77_INT (verbose);


      if (! error_state)
        {

          F77_INT lines_read   = TO_F77_INT (input.rows ()); //nmax in d1()
          F77_INT columns_read = TO_F77_INT (input.columns ());


          dim_vector dv (maxdim - mindim + 1, 1);
          string_vector keys;
          keys.append (std::string("dim"));
          keys.append (std::string("c1"));
          octave_map output (dv, keys);

          // Seed the rand() function for d1()
          F77_XFCN (rand, RAND, (sqrt(seed)));

          for (F77_INT m = mindim; m <= maxdim; m++)
            {
              octave_scalar_map tmp (keys);
              tmp.setfield ("dim", m);

              // Creat c1 output
              Matrix c1_out ((octave_idx_type) ((0 - log (1./(lines_read 
                                                         -(m-1) * delay)) +
                                                (log (2.) /resolution)) 
                                                     / (log (2.) /resolution))
                             , 2);

              double pr = 0.0;
              octave_idx_type current_row = 0;
              for (double pl = log (1./(lines_read - (m-1)*delay));
                   pl <= 0.0; pl += log (2.) / resolution)
                {
                  double pln = pl;
                  double rln;

                  F77_XFCN (d1, D1,
                            (lines_read, columns_read, lines_read,
                             input.fortran_vec (), delay, m, cmin,
                             pr, pln, rln, tmin, kmax, iverb));

                  if (pln != pr)
                    {
                      pr = pln;
                      c1_out(current_row,0) = exp (rln);
                      c1_out(current_row,1) = exp (pln);
                      current_row += 1;
                    }

                }
              // Resize output
              c1_out.resize (current_row, 2);
              tmp.setfield ("c1", c1_out);

              output.assign (idx_vector(m-mindim), tmp);
            }

          retval(0) = output;
        }
    }
  return retval;
}
