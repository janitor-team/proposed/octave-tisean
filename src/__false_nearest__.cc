/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on false_nearest of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include <octave/oct.h>
#include "routines_c/tsa.h"


unsigned long length=-1,exclude=0,theiler=0;
unsigned int delay=1,maxdim=6,minemb=1;
unsigned int comp=1,maxemb=5;

double rt=2.0;
double eps0=1.0e-5;
double **series;
double aveps,vareps;
double varianz;

#define BOX 1024
int ibox=BOX-1;
long **box,*list;
unsigned int *vcomp,*vemb;
unsigned long toolarge;


void mmb(const Matrix &series, unsigned int hdim,unsigned int hemb,double eps)
{
  unsigned long i;
  long x,y;

  for (x=0;x<BOX;x++)
    for (y=0;y<BOX;y++)
      box[x][y] = -1;

  for (i=0;i<length-(maxemb+1)*delay;i++) {
    x=(long)(series(i,0)/eps)&ibox;
    y=(long)(series(i+hemb, hdim)/eps)&ibox;
    list[i]=box[x][y];
    box[x][y]=i;
  }
}

char find_nearest(const Matrix &series, long n,unsigned int dim,double eps)
{
  long x,y,x1,x2,y1,i,i1,ic,ie;
  long element,which= -1;
  double dx,maxdx,mindx=1.1,hfactor,factor;

  ic=vcomp[dim];
  ie=vemb[dim];
  x=(long)(series(n,0)/eps)&ibox;
  y=(long)(series(n+ie,ic)/eps)&ibox;
  
  for (x1=x-1;x1<=x+1;x1++) {
    x2=x1&ibox;
    for (y1=y-1;y1<=y+1;y1++) {
      element=box[x2][y1&ibox];
      while (element != -1) {
      if (labs(element-n) > theiler) {
        maxdx=fabs(series(n,0)-series(element,0));
        for (i=1;i<=dim;i++) {
          ic=vcomp[i];
          i1=vemb[i];
          dx=fabs(series(n+i1,ic)-series(element+i1,ic));
          if (dx > maxdx)
            maxdx=dx;
        }
        if ((maxdx < mindx) && (maxdx > 0.0)) {
          which=element;
          mindx=maxdx;
        }
      }
      element=list[element];
      }
    }
  }

  if ((which != -1) && (mindx <= eps) && (mindx <= varianz/rt)) {
    aveps += mindx;
    vareps += mindx*mindx;
    factor=0.0;
    for (i=1;i<=comp;i++) {
      ic=vcomp[dim+i];
      ie=vemb[dim+i];
      hfactor=fabs(series(n+ie,ic)-series(which+ie,ic))/mindx;
      if (hfactor > factor) 
      factor=hfactor;
    }
    if (factor > rt)
      toolarge++;
    return 1;
  }
  return 0;
}

DEFUN_DLD (__false_nearest__, args, , HELPTEXT)
{
  double min,inter=0.0,ind_inter,epsilon,av,ind_var;
  int dimset=0;
  char *nearest,alldone;
  unsigned long i;
  int verbosity=0;
  unsigned int dim,emb;
  unsigned long donesofar;

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 9)
  {
    print_usage ();
  }
  else
    {

      // Load input data
      Matrix input = args(0).matrix_value();
      minemb       = args(1).int_value();
      comp         = args(2).int_value();
      maxemb       = args(3).int_value();
      dimset       = args(4).int_value();
      delay        = args(5).int_value();
      theiler      = args(6).int_value();
      rt           = args(7).double_value();
      verbosity    = args(8).int_value();

      if (dimset)
        maxdim=comp*(maxemb+1);

      length = input.rows();


      // Analise input data
      for (i=0;i<comp;i++) {
        rescale_data(input,i,length,&min,&ind_inter);
        variance(input.column(i),length,&av,&ind_var);
        if (i == 0) {
          varianz=ind_var;
          inter=ind_inter;
        }
        else {
          varianz=(varianz>ind_var)?ind_var:varianz;
          inter=(inter<ind_inter)?ind_inter:inter;
        }
      }

      // Allocate memory
      check_alloc(list=(long*)malloc(sizeof(long)*length));
      check_alloc(nearest=(char*)malloc(length));
      check_alloc(box=(long**)malloc(sizeof(long*)*BOX));
      for (i=0;i<BOX;i++)
        check_alloc(box[i]=(long*)malloc(sizeof(long)*BOX));

      check_alloc(vcomp=(unsigned int*)malloc(sizeof(int)*(maxdim)));
      check_alloc(vemb=(unsigned int*)malloc(sizeof(int)*(maxdim)));

      if ( ! error_state)
        {
          for (i=0;i<maxdim;i++) {
            if (comp == 1) {
              vcomp[i]=0;
              vemb[i]=i*delay;
            }
            else {
              vcomp[i]=i%comp;
              vemb[i]=(i/comp)*delay;
            }
          }

          // Create output matrix
          Matrix output (maxemb-minemb+1, 4);

          // Compute output
          for (emb=minemb;emb<=maxemb;emb++) 
            {
              dim=emb*comp-1;
              epsilon=eps0;
              toolarge=0;
              alldone=0;
              donesofar=0;
              aveps=0.0;
              vareps=0.0;
              for (i=0;i<length;i++)
                nearest[i]=0;
              if (verbosity)
                octave_stdout << "Start for dimension=" << dim+1 << "\n";
              while (!alldone && (epsilon < 2.*varianz/rt)) {
                alldone=1;
                mmb(input, vcomp[dim],vemb[dim],epsilon);
                for (i=0;i<length-maxemb*delay;i++)
                if (!nearest[i]) {
                  nearest[i]=find_nearest(input, i,dim,epsilon);
                  alldone &= nearest[i];
                  donesofar += (unsigned long)nearest[i];
                }
                if (verbosity)
                octave_stdout << "Found " << donesofar << " up to epsilon=" << \
                              epsilon*inter << "\n";
                epsilon*=sqrt(2.0);
                if (!donesofar)
                eps0=epsilon;
              }
              if (donesofar == 0) {
                error_with_id ("Octave:tisean", "Not enough points found");
              }
              aveps *= (1./(double)donesofar);
              vareps *= (1./(double)donesofar);


          //  old fprintf(file,"%u %e %e %e\n",dim+1,(double)toolarge/(double)donesofar,
          //              aveps*inter,sqrt(vareps)*inter);
              int id = emb-minemb;
              output(id,0) = dim + 1;
              output(id,1) = (double)toolarge/(double)donesofar;
              output(id,2) = aveps*inter;
              output(id,3) = sqrt(vareps)*inter;
            }

          delete[] series;
          delete[] list;
          delete[] nearest;
          for (i=0;i<BOX;i++)
            delete[] box[i];
          delete[] box;

          for (i = 0; i < 4; i++)
            retval(i) = output.column(i);
        }
    }
  return retval;
}
