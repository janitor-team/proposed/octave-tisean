/*
 *   This file is part of TISEAN
 *
 *   Copyright (c) 1998-2007 Rainer Hegger, Holger Kantz, Thomas Schreiber
 *                           Piotr Held
 *   TISEAN is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   TISEAN is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with TISEAN; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/* Author: Rainer Hegger 
 * Modified: Piotr Held <pjheld@gmail.com> (2015).
 * This function is based on rbf of TISEAN 3.0.1 
 * https://github.com/heggus/Tisean"
 */

#define HELPTEXT "Part of tisean package\n\
No argument checking\n\
FOR INTERNAL USE ONLY"

#include "routines_c/tsa.h"
#include <octave/oct.h>

double avdistance(octave_idx_type CENTER, octave_idx_type DIM,
                  double **center)
{

  double dist=0.0;

  for (octave_idx_type i=0;i<CENTER;i++)
    for (octave_idx_type j=0;j<CENTER;j++)
      if (i != j)
        for (octave_idx_type k=0;k<DIM;k++)
          dist += sqr(center[i][k]-center[j][k]);

  return sqrt(dist/(CENTER-1)/CENTER/DIM);
}

double rbf(octave_idx_type DELAY, octave_idx_type DIM, double varianz,
           double *act,double *cen)
{
  double denum=2.0 * varianz * varianz;
  double r=0;

  for (octave_idx_type i=0;i<DIM;i++)
    r += sqr(*(act-i*DELAY)-cen[i]);
  
  return exp(-r/denum);
}

void drift(octave_idx_type CENTER, octave_idx_type DIM, double **center) 
{

  const double step = 1e-2;

  OCTAVE_LOCAL_BUFFER (double, force, DIM);
  for (octave_idx_type l=0;l<20;l++) {
    for (octave_idx_type i=0;i<CENTER;i++) {
      for (octave_idx_type j=0;j<DIM;j++) {
        force[j]=0.0;
        for (octave_idx_type k=0;k<CENTER;k++) {
          if (k != i) {
            double h=center[i][j]-center[k][j];
            force[j] += h/sqr(h)/fabs(h);
          }
        }
      }
      double h=0.0;
      for (octave_idx_type j=0;j<DIM;j++) 
        h += sqr(force[j]);
      double step1=step/sqrt(h);
      for (octave_idx_type j=0;j<DIM;j++) {
        double h1 = step1*force[j];
        if (((center[i][j]+h1) > -0.1) && ((center[i][j]+h1) < 1.1))
          center[i][j] += h1;
      }
    }
  }
}

double forecast_error(double *series, double **center, double *coefs,
                      octave_idx_type CENTER, octave_idx_type DELAY,
                      octave_idx_type DIM, octave_idx_type STEP,
                      double varianz, octave_idx_type i0, octave_idx_type i1)
{

  double error_val=0.0;

  for (octave_idx_type n=i0+(DIM-1)*DELAY;n<i1-STEP;n++) {
    double h=coefs[0];
    for (octave_idx_type i=1;i<=CENTER;i++)
      h += coefs[i]*rbf(DELAY, DIM, varianz, &series[n],center[i-1]);
    error_val += (series[n+STEP]-h)*(series[n+STEP]-h);
  }
  
  return sqrt(error_val/(i1-i0-STEP-(DIM-1)*DELAY));
}

DEFUN_DLD (__rbf__, args, nargout, HELPTEXT)
{

  int nargin = args.length ();
  octave_value_list retval;

  if (nargin != 9 || nargout > 5)
  {
    print_usage ();
  }
  else
    {
      // Assign input
      NDArray input            = args(0).array_value ();
      octave_idx_type DIM      = args(1).idx_type_value ();
      octave_idx_type DELAY    = args(2).idx_type_value ();
      octave_idx_type CENTER   = args(3).idx_type_value ();
      octave_idx_type STEP     = args(4).idx_type_value ();
      octave_idx_type INSAMPLE = args(5).idx_type_value ();
      octave_idx_type CLENGTH  = args(6).idx_type_value ();
      bool MAKECAST            = args(7).bool_value ();
      bool setdrift            = args(8).bool_value ();

      octave_idx_type LENGTH = input.numel ();
      double *series = input.fortran_vec();

      // Analyze input
      double varianz, interval, min_val, av;

      rescale_data(input,LENGTH,&min_val,&interval);
      variance(input,LENGTH,&av,&varianz);

      // Allocate memory
      OCTAVE_LOCAL_BUFFER (double *, center, CENTER);
      OCTAVE_LOCAL_BUFFER (double, center_data, CENTER * DIM);
      for (octave_idx_type i=0;i<CENTER;i++)
        center[i] = center_data + DIM * i;

      Matrix coefs (CENTER + 1, 1);
      double *coefs_arr = coefs.fortran_vec ();

      // Assign initial values
      octave_idx_type cstep=LENGTH-1-(DIM-1)*DELAY;
      for (octave_idx_type i=0;i<CENTER;i++)
        for (octave_idx_type j=0;j<DIM;j++)
          center[i][j]=series[(DIM-1)*DELAY-j*DELAY+(i*cstep)/(CENTER-1)];

      if (! error_state)
        {

          // Promote warnings connected with singular matrixes to errors
          set_warning_state ("Octave:nearly-singular-matrix","error");
          set_warning_state ("Octave:singular-matrix","error");

          // Calculate coefficients
          if (setdrift)
            drift(CENTER, DIM, center);
          varianz=avdistance(CENTER, DIM, center);

          // old make_fit();
          Matrix mat (CENTER + 1, CENTER + 1);
          OCTAVE_LOCAL_BUFFER (double *, mat_arr, CENTER + 1);
          for (octave_idx_type i=0;i <CENTER + 1;i++)
            mat_arr[i]=mat.fortran_vec () + (CENTER + 1) * i;

          for (octave_idx_type i=0;i<=CENTER;i++) {
            coefs_arr[i]=0.0;
            for (octave_idx_type j=0;j<=CENTER;j++)
              mat_arr[i][j]=0.0;
          }

          OCTAVE_LOCAL_BUFFER (double, hcen, CENTER);

          for (octave_idx_type n=(DIM-1)*DELAY;n<INSAMPLE-STEP;n++) {
            octave_idx_type nst=n+STEP;
            for (octave_idx_type i=0;i<CENTER;i++)
              hcen[i]=rbf(DELAY, DIM, varianz, &series[n],center[i]);
            coefs_arr[0] += series[nst];
            mat_arr[0][0] += 1.0;
            for (octave_idx_type i=1;i<=CENTER;i++)
              mat_arr[i][0] += hcen[i-1];
            for (octave_idx_type i=1;i<=CENTER;i++) {
              double h = hcen[i-1];
              coefs_arr[i] += series[nst] * h;
              for (octave_idx_type j=1;j<=i;j++)
                mat_arr[i][j] += h*hcen[j-1];
            }
          }
          
          double h=(double)(INSAMPLE-STEP-(DIM-1)*DELAY);
          for (octave_idx_type i=0;i<=CENTER;i++) {
            coefs_arr[i] /= h;
            for (octave_idx_type j=0;j<=i;j++) {
              mat_arr[i][j] /= h;
              mat_arr[j][i]=mat_arr[i][j];
            }
          }

          // old solvele(mat_arr,coefs_arr, CENTER+1);
          coefs = mat.solve(coefs);
          coefs_arr = coefs.fortran_vec ();// coefs takes up new memory space

          // If solving the matrix generated errors do not continue
          if (error_state)
            return retval;

          // end make_fit()


          // Create outputs

          // Create centers
          Matrix centers (CENTER, DIM); 
          for (octave_idx_type i=0;i<CENTER;i++) 
              for (octave_idx_type j=0;j<DIM;j++)
                {
                  // old fprintf(stdout," %e",center[i][j]*interval+min_val);
                  centers(i,j) = center[i][j]*interval+min_val;
                }

          // Create variance
          // old fprintf(stdout,"#variance= %e\n",varianz*interval);
          double variance_val = varianz*interval;

          // Create coefficients
          NDArray coeff (dim_vector(CENTER +1, 1));
          //  old fprintf(stdout,"#%e\n",coefs[0]*interval+min_val);
          coeff(0) = coefs_arr[0]*interval+min_val;
          for (octave_idx_type i=1;i<=CENTER;i++)
            {
            // old fprintf(stdout,"#%e\n",coefs[i]*interval);
              coeff(i) = coefs_arr[i]*interval;
            }

          // Calculate insample error
          double sigma = 0.0;
          av = 0.0;
          for (octave_idx_type i=0;i<INSAMPLE;i++) {
            av += series[i];
            sigma += series[i]*series[i];
          }
          av /= INSAMPLE;
          sigma=sqrt(fabs(sigma/INSAMPLE-av*av));

          // Create sample error
          // 1st element of sample_error is the insample error
          // 2nd element of sample_error is the out of sample error (if exists)
          NDArray sample_error (dim_vector(1,1));
          // old oldfprintf(stdout,"#insample error= %e\n",
          //                forecast_error(0LU,INSAMPLE)/sigma);
          sample_error(0) = forecast_error(series, center, coefs_arr, CENTER, 
                                           DELAY, DIM, STEP, varianz,
                                           0,INSAMPLE)
                            / sigma;

          if (INSAMPLE < LENGTH)
            {
              // Calculate out of sample error
              av=sigma=0.0;
              for (octave_idx_type i=INSAMPLE;i<LENGTH;i++) {
                av += series[i];
                sigma += series[i]*series[i];
              }
              av /= (LENGTH-INSAMPLE);
              sigma=sqrt(fabs(sigma/(LENGTH-INSAMPLE)-av*av));

              // old fprintf(stdout,"#out of sample error= %e\n",
              //             forecast_error(INSAMPLE,LENGTH)/sigma);
              sample_error.resize (dim_vector(2,1));
              sample_error(1) = forecast_error(series, center, coefs_arr,
                                               CENTER, DELAY, DIM, STEP,
                                               varianz, INSAMPLE, LENGTH)
                                / sigma;
            }

          // Create forecast if MAKECAST == true
          NDArray forecast (dim_vector (0,0));
          if (MAKECAST)
            {
            // old make_cast ();

              forecast.resize(dim_vector(CLENGTH,1));

              octave_idx_type dim=(DIM-1)*DELAY;

              OCTAVE_LOCAL_BUFFER (double, cast, dim + 1);
              for (octave_idx_type i=0;i<=dim;i++)
                cast[i]=series[LENGTH-1-dim+i];

              for (octave_idx_type n=0;n<CLENGTH;n++)
                {
                  double new_el=coefs_arr[0];
                  for (octave_idx_type i=1;i<=CENTER;i++)
                    new_el += coefs_arr[i]*rbf(DELAY,DIM,varianz,&cast[dim],
                                           center[i-1]);
                //  old  fprintf(out,"%e\n",new_el*interval+min_val);
                  forecast(n) = new_el*interval+min_val;
                  for (octave_idx_type i=0;i<dim;i++)
                    cast[i]=cast[i+1];
                  cast[dim]=new_el;
                }
            }

          // Create output
          retval(0) = centers; 
          retval(1) = variance_val; // variance;
          retval(2) = coeff; 
          retval(3) = sample_error; // sample error [in sample; out of sample];
          retval(4) = forecast; // forecast values;
        }
    }
  return retval;
}

